//
//  VipTypeCell.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "VipTypeCell.h"

@implementation VipTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.cornerView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH-32, 100)];
}

- (void)setModel:(VipTypeModel *)model
{
    _model = model;
    self.typeLabel.text = model.title;
    self.priceLabel.text = [NSString stringWithFormat:@"/￥%@",model.price];
    self.introduceLabel.text = model.remarks;
    [self.bgImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.picture)] placeholderImage:[UIImage imageNamed:@"组 45838"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
