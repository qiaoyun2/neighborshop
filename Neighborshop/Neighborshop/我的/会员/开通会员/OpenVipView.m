//
//  OpenVipView.m
//  PPK
//
//  Created by null on 2022/3/12.
//

#import "OpenVipView.h"
#import "VipTypeCell.h"
#import "WXApiManager.h"
#import <AlipaySDK/AlipaySDK.h>
#import "YQInAppPurchaseTool.h"
#import "RWReserveResultController.h"

@interface OpenVipView ()<UITableViewDelegate,UITableViewDataSource,WXApiManagerDelegate,YQInAppPurchaseToolDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *aliPayButton;
@property (weak, nonatomic) IBOutlet UIButton *wxPayButton;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) VipTypeModel *lastModel;

@end

@implementation OpenVipView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.hidden = YES;
    self.dataArray = [NSMutableArray array];
    self.contentViewHeight.constant = 607+bottomBarH;
    self.contentViewBottom.constant = - self.contentViewHeight.constant;
    [self.contentView addRoundedCorners:UIRectCornerTopLeft|UIRectCornerTopRight withRadii:CGSizeMake(12, 12) viewRect:CGRectMake(0, 0, SCREEN_WIDTH, self.contentViewHeight.constant)];
    
    self.aliPayButton.selected = YES;
    self.aliPayButton.borderColor = MainColor;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    YQInAppPurchaseTool *IAPTool = [YQInAppPurchaseTool defaultTool];
    //设置代理
    IAPTool.delegate = self;
    //购买后，向苹果服务器验证一下购买结果。默认为YES。不建议关闭
    IAPTool.CheckAfterPay = NO;
    
    [self requestVipType];
}

#pragma mark - Network
- (void)requestVipType
{
    [NetworkingTool getWithUrl:kMasterVipTypeURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                VipTypeModel *model = [[VipTypeModel alloc] initWithDictionary:obj];
                [self.dataArray addObject:model];
            }
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

//
- (void)requestForBuyVip:(NSInteger)type
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"paymentType"] = @(type);
    params[@"vipTypeId"] = self.lastModel.ID;
    [NetworkingTool postWithUrl:kMasterBuyVipURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dataDic = responseObject[@"data"];
            if (type==1) {
                NSDictionary *payDic = dataDic[@"wechatMsg"];
                [self wxPayWithMessage:payDic];
            }else {
                NSString *signStr = dataDic[@"aliMsg"];
                [self aliPayWithMessage:signStr];
            }
            [self dismiss];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (void)requetForServiceNotify:(NSDictionary *)infoDic{
    
    // appStoreReceiptURL iOS7.0增加的，购买交易完成后，会将凭据存放在该地址
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    // 从沙盒中获取到购买凭据
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    NSString *encodeStr = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"vipTypeId"] = self.lastModel.ID;
    params[@"transactionID"] = infoDic[@"transactionIdentifier"];
    params[@"receipt"] = encodeStr;
    params[@"transactionDate"] = infoDic[@"transactionDate"];
    
    [NetworkingTool postWithUrl:kInAppPurchaseURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showOKHud:@"购买成功" delay:1];
            //比对字典中以下信息基本上可以保证数据安全
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self requestForUserInfo];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PaySuccess" object:nil userInfo:nil];
                [self dismiss];
                RWReserveResultController *vc = [[RWReserveResultController alloc] init];
                vc.tipLabel.text = @"开通成功";
                vc.subTipLabel.text = @"恭喜您 成为尊贵的月度会员";
                [vc.backButton setTitle:@"接单赚钱" forState:UIControlStateNormal];
                [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
                
            });
        }else {
            [LJTools showNOHud:@"校验失败，请联系管理员" delay:2];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
    
    
    //    NSURL *url = [NSURL URLWithString:self.notify_url];
    //    NSLog(@"%@",url);
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0f];
    //    request.HTTPMethod = @"POST";
    //    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[[NSBundle mainBundle] appStoreReceiptURL]];
    //    NSData *transData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error:nil];
    //
    //    NSString *boxStr = @"";
    //#if DEBUG
    //    boxStr = @"0";//沙盒
    //#else
    //    boxStr = @"1";//正式
    //#endif
    //    NSString *encodeStr = [transData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    //    NSString *payload = [NSString stringWithFormat:@"{\"receipt-data\" : \"%@\",\"sandbox\" : \"%@\",\"out_trade_no\" : \"%@\",\"transactionIOS\" : \"%@\"}", encodeStr,boxStr,self.order_sn,transactionIdentifier];
    //    //把bodyString转换为NSData数据
    //    NSData *bodyData = [payload dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];//把bodyString转换为NSData数据
    //    [request setHTTPBody:bodyData];
    //    // 提交验证请求，并获得官方的验证JSON结果
    //    NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    //    if (result == nil) {
    //        [LJTools showNOHud:@"充值失败，请联系管理员" delay:2];
    //    } else {
    //        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingAllowFragments error:nil];
    //        NSLog(@"dict--%@",dict);
    //        if(dict==nil)
    //        {
    //            //             [MBProgressHUD showError:@"请查看网站是否开启了调试模式"];
    //            return;
    //        }
    //        if ([dict[@"status"] integerValue] ==1) {
    //
    //        } else {
    //            [LJTools showNOHud:@"校验失败，请联系管理员" delay:2];
    //        }
    //    }
}

- (void)requestForUserInfo
{
    [NetworkingTool getWithUrl:kGetUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"]];
            [User saveUser:user];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VipTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VipTypeCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"VipTypeCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    VipTypeModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.coverView.hidden = model != self.lastModel;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VipTypeModel *model = self.dataArray[indexPath.row];
    self.lastModel = model;
    [tableView reloadData];
}

#pragma mark - WXApiManagerDelegate
- (void)managerDidRecvPaymentResponse:(PayResp *)response{
    if (response.errCode==0) {
        //[LJTools showText:@"购买成功" delay:1.5];
        [LJTools showOKHud:@"购买成功" delay:1];
        //比对字典中以下信息基本上可以保证数据安全
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self requestForUserInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PaySuccess" object:nil userInfo:nil];
            [self dismiss];
            RWReserveResultController *vc = [[RWReserveResultController alloc] init];
            vc.tipLabel.text = @"开通成功";
            vc.subTipLabel.text = @"恭喜您 成为尊贵的月度会员";
            [vc.backButton setTitle:@"接单赚钱" forState:UIControlStateNormal];
            [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
            
        });
    }
}

#pragma mark - InAppPurchasesDelegate
//IAP工具已获得可购买的商品
- (void)IAPToolGotProducts:(NSMutableArray *)products {
    NSLog(@"%@",products);
    if (products.count == 0) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [LJTools showText:@"未查询到可以购买的商品" delay:1.5];
        });
    }
}
//支付失败/取消
- (void)IAPToolCanceldWithProductID:(NSString *)productID {
    NSLog(@"canceld:%@",productID);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [LJTools showText:@"购买失败" delay:2];
    });
}
//商品被重复验证了
- (void)IAPToolCheckRedundantWithProductID:(NSString *)productID {
    NSLog(@"CheckRedundant:%@",productID);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [LJTools showText:@"重复验证了" delay:2];
    });
}
//内购校验
- (void)IAPToolBoughtProductSuccessedWithProductID:(NSString *)productID
                                           andInfo:(NSDictionary *)infoDic {
    NSLog(@"BoughtSuccessed:%@",productID);
    NSLog(@"successedInfo:%@",infoDic);
    [self requetForServiceNotify:infoDic];
}
//内购系统错误了
- (void)IAPToolSysWrong {
    NSLog(@"SysWrong");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [LJTools showText:@"内购系统出错" delay:2];
    });
}

#pragma mark - Function
- (void)show
{
    self.hidden = NO;
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contentViewBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [self layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contentViewBottom.constant = - self.contentViewHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

- (void)wxPayWithMessage:(NSDictionary *)dic
{
    PayReq *request = [[PayReq alloc] init];
    request.openID = dic[@"appid"];//公众账号ID
    request.partnerId = dic[@"partnerid"];//微信支付分配的商户号
    request.prepayId = dic[@"prepayid"];//预支付交易会话ID
    request.nonceStr = dic[@"noncestr"];//随机字符串
    request.timeStamp = [dic[@"timestamp"] intValue];//时间戳
    request.package = dic[@"package"];//扩展字段(默认 Sign=WXPay)
    request.sign = dic[@"sign"];//签名
    [WXApiManager sharedManager].delegate=self;
    ///发起支付
    [WXApi sendReq:request completion:^(BOOL success) {
        if (success) {
            
        } else{
            [LJTools showNOHud:@"发起支付失败" delay:1.0];
        }
    }];
}

- (void)aliPayWithMessage:(NSString *)msg
{
    [[AlipaySDK defaultService] payOrder:msg fromScheme:AlipayScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"-------%@", resultDic);
        //        NSString *Memo = [resultDic objectForKey:@"memo"];
        NSString *resultStatus = [resultDic objectForKey:@"resultStatus"];
        if ([resultStatus isEqualToString:@"9000"])
        {
            //            [LJTools showText:@"充值成功" delay:1.5];
        }else{
            [LJTools showText:@"支付失败" delay:1.5];
        }
    }];
}

#pragma mark - XibFunction
- (IBAction)openButtonAction:(UIButton *)sender {
    //    [self dismiss];
    if (!self.lastModel) {
        [LJTools showText:@"请选择想要购买的类型" delay:1.5];
        return;
    }
    NSInteger type = self.aliPayButton.selected ? 2 : 1;
    [self requestForBuyVip:type];
    //    [LJTools showTextHud:@"正在购买"];
    //    [[YQInAppPurchaseTool defaultTool] requestProductsWithProductArray:@[self.lastModel.productId]];
}

- (IBAction)aliPayButtonAction:(UIButton *)sender {
    sender.selected = YES;
    sender.borderColor = MainColor;
    
    self.wxPayButton.selected = NO;
    self.wxPayButton.borderColor = UIColorFromRGB(0xD9D9D9);
}

- (IBAction)wxPayButtonAction:(UIButton *)sender {
    sender.selected = YES;
    sender.borderColor = MainColor;
    
    self.aliPayButton.selected = NO;
    self.aliPayButton.borderColor = UIColorFromRGB(0xD9D9D9);
}

- (IBAction)closeButtonAction:(UIButton *)sender {
    [self dismiss];
}

@end
