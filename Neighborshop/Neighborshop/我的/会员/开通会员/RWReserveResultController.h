//
//  RWReserveResultController.h
//  PPK
//
//  Created by null on 2022/4/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWReserveResultController : BaseViewController

@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTipLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;


@end

NS_ASSUME_NONNULL_END
