//
//  TakeoutShopInfoModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/12/10.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TakeoutShopInfoModel : BaseModel
@property (nonatomic, strong) NSString *address; //
@property (nonatomic, strong) NSString *averagePrice; //人均价
@property (nonatomic, strong) NSString *backgroundPicture; //
@property (nonatomic, strong) NSString *businessFlag;///是否可取消订单
@property (nonatomic, strong) NSString *businessHours;//营业时间，格式：12:00-16:00
@property (nonatomic, strong) NSString *businessStatus;//营业状态: 0-停止;1-营业
@property (nonatomic, strong) NSString *collect;
@property (nonatomic, strong) NSArray *foodCategoryList;
@property (nonatomic, strong) NSString *introduction;//商家简介
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *licenseImg;
@property (nonatomic, strong) NSString *logoImg;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *monthSales;
@property (nonatomic, strong) NSString *notice;//公告
@property (nonatomic, strong) NSString *score;
@property (nonatomic, strong) NSString *sendCost;//配送费用
@property (nonatomic, strong) NSString *shopCategoryName;
@property (nonatomic, strong) NSString *shopId;//商铺id
@property (nonatomic, strong) NSString *shopName;
@property (nonatomic, strong) NSString *shopPictures;
@property (nonatomic, strong) NSString *startDelivery;//起送费用

@end

NS_ASSUME_NONNULL_END
