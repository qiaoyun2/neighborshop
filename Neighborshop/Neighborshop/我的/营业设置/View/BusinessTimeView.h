//
//  BusinessTimeView.h
//  AdvorsysBusiness
//
//  Created by pengfei li on 2022/1/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BusinessTimeView : UIView
+(BusinessTimeView *)showWith:(void(^)(NSString *start,NSString *end))block;
@end

NS_ASSUME_NONNULL_END
