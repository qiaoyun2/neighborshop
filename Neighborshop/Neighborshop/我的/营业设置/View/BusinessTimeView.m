//
//  BusinessTimeView.m
//  AdvorsysBusiness
//
//  Created by pengfei li on 2022/1/27.
//

#import "BusinessTimeView.h"
@interface BusinessTimeView ()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *contView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contBottom;
@property (weak, nonatomic) IBOutlet UIPickerView *leftPickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *rightPickerView;
@property (nonatomic, copy) void(^Block)(NSString *start,NSString *end);
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, copy) NSString *start;
@property (nonatomic, copy) NSString *end;
@end
@implementation BusinessTimeView
static BusinessTimeView *manager = nil;
+(instancetype)shareManager
{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
        manager = [[[NSBundle mainBundle] loadNibNamed:@"BusinessTimeView" owner:self options:nil] lastObject];
        [manager setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        manager.contHeight.constant = 227+bottomBarH;
        manager.contBottom.constant = -manager.contHeight.constant;
        manager.dataArray = @[@"06:00",@"06:30",@"07:00",@"07:30",@"08:00",@"08:30",@"09:00",@"09:30",@"10:00",@"10:30",@"11:00",@"11:30",@"12:00",@"12:30",@"13:00",@"13:30",@"14:00",@"14:30",@"15:00",@"15:30",@"16:00",@"16:30",@"17:00",@"17:30",@"16:00",@"16:30",@"17:00",@"17:30",@"18:00",@"18:30",@"19:00",@"19:30",@"20:00",@"20:30",@"21:00",@"21:30",@"22:00",@"22:30",@"23:00"];
        manager.start = @"06:00";
        manager.end = @"06:00";
//    });
    return manager;
}
+(BusinessTimeView *)showWith:(void(^)(NSString *start,NSString *end))block
{
    BusinessTimeView *helper = [BusinessTimeView shareManager];
    [helper show];
    helper.Block = block;
    [helper layoutIfNeeded];
    [[LJTools getAppWindow] addSubview:helper];
    return helper;
}
- (void)show
{
    [self.leftPickerView setDelegate:self];
    [self.leftPickerView setDataSource:self];
    [self.leftPickerView reloadAllComponents];
    [self.rightPickerView setDelegate:self];
    [self.rightPickerView setDataSource:self];
    [self.rightPickerView reloadAllComponents];
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0.4);
        self.contBottom.constant = 0;
        [self layoutIfNeeded];
    }];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.2 animations:^{
        self.backgroundColor = RGBA(0, 0, 0, 0);
        self.contBottom.constant = -manager.contHeight.constant;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.dataArray.count;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 50;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (view == nil) {
        UILabel *lab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 50)];
        lab.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
        lab.textColor = [UIColor blackColor];
        lab.textAlignment = NSTextAlignmentCenter;
        view = lab;
    }
    UILabel *lab = (UILabel *)view;
    lab.text = self.dataArray[row];
    return lab;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //xxxxx核心滑动逻辑判断
    NSLog(@"======%@",self.dataArray[row]);
    if (pickerView == self.leftPickerView) {
        self.start = self.dataArray[row];
    }else
    {
        self.end = self.dataArray[row];
    }
}
- (IBAction)btn:(UIButton *)sender {
    switch (sender.tag) {
        case 1://取消
        {
            [self dismiss];
        }
            break;
        case 2://确定
        {
            if ([self.start compare:self.end] == NSOrderedDescending) {
                [LJTools showNOHud:LocalizedString(@"开始时间不能大于结束时间") delay:1.0];
                return;
            }
            !self.Block?:self.Block(self.start, self.end);
            [self dismiss];
        }
            break;
    }
    
}

@end
