//
//  TradingProfitViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/20.
//

#import "TradingProfitViewController.h"
#import "TakeoutShopInfoModel.h"
#import "LJImagePicker.h"
#import "UploadManager.h"
#import "BusinessTimeView.h"
#import "HXPhotoView.h"

@interface TradingProfitViewController ()<HXPhotoViewDelegate>

///休息中 正在营业中
@property (weak, nonatomic) IBOutlet UILabel *businessLabel;

@property (weak, nonatomic) IBOutlet UIImageView *businessImageView;

///营业中
@property (weak, nonatomic) IBOutlet UIButton *businessBtn;
///人均消费
@property (weak, nonatomic) IBOutlet UITextField *minimumText;

///起配送
@property (weak, nonatomic) IBOutlet UITextField *freeText;

///配送费
@property (weak, nonatomic) IBOutlet UITextField *distributionText;
///营业时间
@property (weak, nonatomic) IBOutlet UITextField *timeText;

///公告
@property (weak, nonatomic) IBOutlet UITextView *noticeText;
///简介
@property (weak, nonatomic) IBOutlet UITextView *interfaceText;
///log
@property (weak, nonatomic) IBOutlet HXPhotoView *shopImageView;
@property (nonatomic, strong) TakeoutShopInfoModel *model;

// 图片上传
@property (nonatomic, strong) HXPhotoManager *manager;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray<UIImage *> *detailPhotos;
@property (nonatomic, strong) NSMutableArray *imgArray;
///图片路径
@property (nonatomic, strong) NSMutableArray *imagePathArray;

@property (nonatomic, assign) NSInteger index; // 上传下标
@end

@implementation TradingProfitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"营业设置";
    
    self.shopImageView.spacing = 10.f;
    self.shopImageView.delegate = self;
    self.shopImageView.deleteCellShowAlert = YES;
    self.shopImageView.outerCamera = YES;
    self.shopImageView.previewShowDeleteButton = YES;
    self.shopImageView.addImageName = @"组 51102";
    self.shopImageView.lineCount = 3;
    self.shopImageView.manager = self.manager;
    
    [self getAllData];
}
#pragma mark -网络请求
-(void)getAllData{
    //
    WeakSelf
    [NetworkingTool getWithUrl:ktakeoutShopShopInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *dict = responseObject[@"data"];
            TakeoutShopInfoModel *model = [[TakeoutShopInfoModel alloc] initWithDictionary:dict];
            weakSelf.model = model;
            [weakSelf updateUI];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark -更新数据
-(void)updateUI{
    self.businessLabel.text  = self.model.businessStatus.intValue == 1?@"正在营业中":@"休息中";
    self.businessBtn.selected =  self.model.businessStatus.intValue == 1;
    self.businessImageView.image = [UIImage imageNamed:self.model.businessStatus.intValue == 1?@"组 9902":@"组 51549"];
    self.minimumText.text = self.model.averagePrice;
    self.freeText.text = self.model.startDelivery;
    self.distributionText.text = self.model.sendCost;
    self.timeText.text = self.model.businessHours;
    
    self.noticeText.text = self.model.notice;
    self.interfaceText.text = self.model.introduction;
    if(self.model.shopPictures.length > 0){
        self.imagePathArray = [NSMutableArray arrayWithArray:[self.model.shopPictures componentsSeparatedByString:@","]];
        //imageUrlArray是已知图片的网络地址数组
        NSMutableArray *assets = @[].mutableCopy;
        for(NSString *url in self.imagePathArray)  {
            //selected设为YES 表示图片处于已选中状态
            HXCustomAssetModel *asset = [HXCustomAssetModel assetWithNetworkImageURL:[NSURL URLWithString:kImageUrl(url)] selected:YES];
            [assets addObject:asset];
        }
        [self.manager addCustomAssetModel:assets];
        [self.shopImageView refreshView];
        
    }
    
}
#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    self.photos = [NSMutableArray arrayWithArray:allList];
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // 判断是否已经存在小数点
    if ([string isEqualToString:@"."] && [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        textField.text = @"";
        return NO;
    } else if ([string isEqualToString:@"."] && [textField.text containsString:@"."]) {
        return NO;
    }
    NSMutableString * futureString = [NSMutableString stringWithString:textField.text];
    [futureString  insertString:string atIndex:range.location];
    NSInteger flag=0;
    const NSInteger limited = 2;  // 小数点  限制输入两位
    for (NSInteger i = futureString.length-1; i>=0; i--) {
        if ([futureString characterAtIndex:i] == '.') {
            if (flag > limited)   return NO;
            break;
        }
        flag++;
    }
    return YES;
}

#pragma mark -logo
- (IBAction)logoImageClick:(id)sender {
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                weakSelf.model.logoImg = imageUrl;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
            }];
        }
    }];
}

#pragma mark -是否营业
- (IBAction)businessBtnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    sender.selected = !sender.selected;
    if(sender.selected) {
        self.businessImageView.image = [UIImage imageNamed:@"组 9902"];
        self.businessLabel.text = @"正在营业中";
        self.model.businessStatus = @"1";
        
    }else{
        self.businessImageView.image = [UIImage imageNamed:@"组 51549"];
        self.businessLabel.text = @"休息中";
        self.model.businessStatus = @"0";
    }
}
- (IBAction)timeBtnClick:(id)sender {
    WeakSelf
    [self.view endEditing:YES];
    [BusinessTimeView showWith:^(NSString * _Nonnull start, NSString * _Nonnull end) {
        weakSelf.timeText.text = [NSString stringWithFormat:@"%@-%@",start,end];
        weakSelf.model.businessHours = [NSString stringWithFormat:@"%@-%@",start,end];
    }];
}
#pragma mark - 上传图片
- (void)requestForUploadImages
{
    // 开始上传图片
    [LJTools showHud];
    dispatch_group_t getImageGroup = dispatch_group_create();
    // 处理图片数据
    dispatch_group_enter(getImageGroup);
    self.imgArray = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i < self.photos.count; i++) {
        HXPhotoModel *model = self.photos[i];
        if (model.networkPhotoUrl) {
            [self.imgArray addObject:model.networkPhotoUrl];
            if (self.imgArray.count == self.photos.count) {
                // 离开线程
                dispatch_group_leave(getImageGroup);
            }
        }else{
            [model requestPreviewImageWithSize:model.imageSize startRequestICloud:^(PHImageRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
            } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                
            } success:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                [self.imgArray addObject:image];
                if (self.imgArray.count == self.photos.count) {
                    // 离开线程
                    dispatch_group_leave(getImageGroup);
                }
            } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                NSLog(@"获取图片失败");
            }];
        }
    }
    // 运行完了 回主线程开始上传
    dispatch_group_notify(getImageGroup, dispatch_get_main_queue(), ^{
        WeakSelf
        if ([self isContainNetImg]) {
            // 开始循环上传
            weakSelf.index = 0;
            weakSelf.imagePathArray = [NSMutableArray arrayWithCapacity:0];
            [weakSelf uploadIageOrAddUrl];
        } else {
            // 整体上传
            [UploadManager uploadImageArray:self.imgArray block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                NSArray *array = [imageUrl componentsSeparatedByString:@","];
                weakSelf.imagePathArray = [NSMutableArray arrayWithArray:array];
                [weakSelf requestForSubmit];
            }];
        }
        
    });
}
#pragma mark -  判断是否包含网络图片
- (BOOL)isContainNetImg{
    BOOL isContain = NO;
    for (HXPhotoModel *model in self.photos) {
        if (model.networkPhotoUrl) {
            isContain = YES;
            break;
        }
    }
    return isContain;
}

- (void)uploadIageOrAddUrl{
    id obj = self.imgArray[self.index];
    if ([obj isKindOfClass:[UIImage class]]) {
        UIImage *image = (UIImage *)obj;
        WeakSelf
        [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
            [weakSelf.imagePathArray addObject:imageUrl];
            if (weakSelf.imagePathArray.count == weakSelf.imgArray.count) {
                // 提交商品
                [weakSelf requestForSubmit];
            }else{
                weakSelf.index++;
                [weakSelf uploadIageOrAddUrl];
            }
        }];
    } else {
        NSString *imgUrl = (NSString *)obj;
        [self.imagePathArray addObject:imgUrl];
        if (self.imagePathArray.count == self.imgArray.count) {
            // 提交商品
            [self requestForSubmit];
        }else{
            self.index++;
            [self uploadIageOrAddUrl];
        }
    }
}
#pragma mark - 保存
- (IBAction)saveBtnClick:(id)sender {
    if(self.minimumText.text.length == 0){
        [LJTools showText:self.minimumText.placeholder delay:1.0f];
        return;
    }
    if(self.freeText.text.length == 0){
        [LJTools showText:self.freeText.placeholder delay:1.0f];
        return;
    }
    if(self.distributionText.text.length == 0){
        [LJTools showText:self.distributionText.placeholder delay:1.0f];
        return;
    }
    if(self.noticeText.text.length == 0){
        [LJTools showText:@"请输入公告" delay:1.0f];
        return;
    }
    if(self.timeText.text.length == 0){
        [LJTools showText:self.timeText.placeholder delay:1.0f];
        return;
    }
    [self requestForUploadImages];
}
-(void)requestForSubmit{
    //
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"shopId"] = self.model.shopId;
    dict[@"businessStatus"] = self.model.businessStatus;
    dict[@"businessHours"] = self.timeText.text;
    dict[@"startDelivery"] = self.freeText.text;
    dict[@"sendCost"] = self.distributionText.text;
    dict[@"notice"] = self.noticeText.text;
    dict[@"averagePrice"] = self.minimumText.text;
    dict[@"introduction"] = self.interfaceText.text;
    dict[@"shopPictures"] = [self.imagePathArray componentsJoinedByString:@","];
    
    [NetworkingTool getWithUrl:ktakeoutShopShopEditURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:responseObject[@"msg"] delay:1.0];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - Lazy Loads
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum = 0;
        _manager.configuration.photoMaxNum = 9;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
        _manager.configuration.selectPhotoLimitSize = YES; // 限制图片大小
        /// 限制照片的大小 单位：b 字节
        /// 默认 0字节 不限制
        /// 网络图片不限制
        _manager.configuration.limitPhotoSize = 10 * 1024 * 1024;
    }
    return _manager;
}

#pragma mark - 懒加载
-(NSMutableArray *)photos{
    if (!_photos) {
        _photos = [NSMutableArray array];
    }
    return _photos;
}

@end
