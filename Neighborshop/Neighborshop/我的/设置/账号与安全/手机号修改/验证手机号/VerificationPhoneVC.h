//
//  VerificationPhoneVC.h
//  ZZR
//
//  Created by null on 2020/9/11.
//  Copyright © 2020 null. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface VerificationPhoneVC : BaseViewController

@property(nonatomic,assign)NSInteger type;//1 更改手机号; 2. 修改支付密码//3.修改密码

@property(nonatomic,assign)BOOL isNew;

@end

NS_ASSUME_NONNULL_END
