//
//  ChangePhoneVC.m
//  ZZR
//
//  Created by null on 2021/3/8.
//

#import "ChangePhoneVC.h"

#import "SettingController.h" // 设置

@interface ChangePhoneVC ()
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;

@end

@implementation ChangePhoneVC
-  (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"绑定新手机";
//    [_codeBtn setCountdown:60 WithStartString:@"" WithEndString:@"获取验证码"];
    [self initUI];
}
#pragma mark – UI
- (void)initUI{
    self.phoneTF.jk_maxLength = 11;
    self.codeTF.jk_maxLength = 4;
}
#pragma mark – Network
#pragma mark – Delegate
#pragma mark - Function

#pragma mark – XibFunction
- (IBAction)btnClick:(UIButton*)sender {
    [self.view endEditing:YES];
    if ([self.phoneTF.text isBlankString]) {
        [LJTools showText:LocalizedString(@"请输入手机号") delay:1];
        return;
    }
    
    if (![self.phoneTF.text isTelephone]) {
        [LJTools showText:LocalizedString(@"请输入正确的手机号") delay:1];
        return;
    }
    
    if ([self.codeTF.text isBlankString]) {
        [LJTools showText:LocalizedString(@"请输入手机验证码") delay:1];
        return;
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithCapacity:4];
    [dic setValue:self.phoneTF.text forKey:@"mobile"];
    [dic setValue:self.codeTF.text forKey:@"captcha"];
    [dic setValue:@"changePhone" forKey:@"event"];
    [NetworkingTool postWithUrl:kChangeMobileSecondURL params:dic success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            [LJTools showText:@"修改成功" delay:2];
            User * user = [User getUser];
            user.mobile = self.phoneTF.text;
            [User saveUser:user];
            [self.navigationController jk_popToViewControllerWithClassName:@"SettingController" animated:YES];
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
- (IBAction)sendCode:(id)sender {
    [self.view endEditing:YES];
    if ([self.phoneTF.text isBlankString]) {
        [LJTools showText:LocalizedString(@"请输入手机号") delay:1];
        return;
    }
    if (![self.phoneTF.text isTelephone]) {
        [LJTools showText:LocalizedString(@"请输入正确的手机号") delay:1];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = [User getUser].mobile;
    params[@"event"] = @"changePhone";
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            // 获取验证码
            [sender setCountdown:60 WithStartString:@"" WithEndString:LocalizedString(@"获取验证码")];
            [LJTools showText:@"发送成功" delay:1.5];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark – lazy load

@end
