//
//  LogoutViewController.m
//  ZZR
//
//  Created by ZZR on 2021/10/7.
//

#import "LogoutViewController.h"
#import "LogoutViewController1.h"

@interface LogoutViewController ()
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@end

@implementation LogoutViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    self.navigationItem.title = LocalizedString(@"注销账号");
    [self requestForConfig];
}

- (void)requestForConfig
{
    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"delUserHelp"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            self.contentLabel.text = responseObject[@"data"];
        }else {
            
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

- (IBAction)buttonClick:(id)sender {
    LogoutViewController1 *vc = [[LogoutViewController1 alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
