//
//  ChangeMyPassViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/12/15.
//

#import "ChangeMyPassViewController.h"

@interface ChangeMyPassViewController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;

@end

@implementation ChangeMyPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"修改密码";
    
    User * user = [User getUser];
    _phoneText.text = [NSString stringWithFormat:@"%@",user.mobile];
    
    [_phoneText addTarget:self action:@selector(textFieldDidChange:)
       forControlEvents:UIControlEventEditingChanged];
    [_codeText addTarget:self action:@selector(textFieldDidChange:)
       forControlEvents:UIControlEventEditingChanged];
    [_passwordText addTarget:self action:@selector(textFieldDidChange:)
       forControlEvents:UIControlEventEditingChanged];
    
}
#pragma mark - 💓💓 Function ------
/** 输入框观察者事件 */
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == _phoneText && _phoneText.text.length > 11) {
        _phoneText.text = [_phoneText.text substringToIndex:11];
    }
    if (textField == _codeText && _codeText.text.length > 4) {
        _codeText.text = [_codeText.text substringToIndex:4];
    }
    if (textField == _passwordText && _passwordText.text.length > 12) {
        _passwordText.text = [_passwordText.text substringToIndex:12];
    }
}
#pragma mark - 获取验证码
- (IBAction)codeBtnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    if (_phoneText.text.length == 0) {
        [LJTools showText:@"请输入手机号" delay:1];
        return;
    }
    if (![_phoneText.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.phoneText.text;
    params[@"event"] = @"verifyPhone";
    
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [sender setCountdown:60 WithStartString:@"" WithEndString:@"获取验证码"];
        }
        [LJTools showText:responseObject[@"msg"] delay:1];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showText:RequestServerError delay:1.0];
    } IsNeedHub:YES];
    
}
#pragma mark - 提交
- (IBAction)sureBtnClick:(id)sender {
    [self.view endEditing:YES];
    if (self.phoneText.text.length == 0) {
        [LJTools showText:@"请输入手机号" delay:1];
        return;
    }
    if (![self.phoneText.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1];
        return;
    }
    if ([self.codeText.text isBlankString]) {
        [LJTools showText:@"请输入验证码" delay:1];
        return;
    }
    if ([self.passwordText.text isBlankString]) {
        [LJTools showText:@"请输入密码" delay:1];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setValue:self.passwordText.text forKey:@"password"];
    [params setValue:@"verifyPhone" forKey:@"event"];
    [params setValue:self.codeText.text forKey:@"captcha"];
    [NetworkingTool postWithUrl:kChangePwdByCodeSecondURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"]intValue]==SUCCESS) {
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
            [self.navigationController jk_popToViewControllerWithClassName:@"SettingController" animated:YES];
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools hideHud];
    } IsNeedHub:YES];
}


@end
