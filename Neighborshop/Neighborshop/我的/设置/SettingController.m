//
//  SettingController.m
//  ZZR
//
//  Created by null on 2020/10/7.
//  Copyright © 2020 null. All rights reserved.
//

#import "SettingController.h" // 设置

#import "FeedbackVC.h"// 意见反馈

#import "SelectChangeTypeVC.h"
#import "NavigationController.h"
#import <StoreKit/StoreKit.h>
#import "WKWebViewController.h"
#import <SDImageCache.h>
#import "LoginViewController.h" //登录页面
#import "ChangeMyPassViewController.h"
#import "LoginViewController.h"
#import "TabBarController.h"
#import "AboutUsVC.h"


@interface SettingController ()<SKStoreProductViewControllerDelegate>{
    NSInteger _is_disturb;
}

@property (weak, nonatomic) IBOutlet UILabel *cache;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginOutButton;

@end

@implementation SettingController

#pragma mark - 💓💓 Life Cycle Methods ------
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = LocalizedString(@"设置");
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    // 获取版本号
    NSDictionary *appInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *currentVersion = [appInfo objectForKey:@"CFBundleShortVersionString"];
    self.versionLabel.text = [NSString stringWithFormat:@"V%@",currentVersion];
    self.cache.text = [NSString stringWithFormat:@"%.2fM",[self getCache]];
}

#pragma mark - 💓💓 UI ------
#pragma mark - 💓💓 Network ------
#pragma mark - 💓💓 Delegate ------
#pragma mark - 💓💓 Function ------
///** 取消按钮监听 */
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -- 版本更新b
- (void)promptUpdate {
    
    NSString *appCurVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSLog(@"当前应用软件版本:%@",appCurVersion);
    
    [NetworkingTool postWithUrl:[NSString stringWithFormat:@"https://itunes.apple.com/lookup?id=%@",AppStoreAppID] params:[NSDictionary dictionary] success:^(NSURLSessionDataTask *task, id responseObject) {
        
        NSArray *array = responseObject[@"results"];
        NSDictionary *dict = [array lastObject];
        NSLog(@"当前版本为：%@", dict[@"version"]);
        if ([appCurVersion floatValue]<[dict[@"version"] floatValue]) {
            [self creatAlertViewControllerWithMessage:[NSString stringWithFormat:LocalizedString(@"有新版本:%@"),dict[@"version"]]];
        }else{
            [LJTools showText:LocalizedString(@"已是最新版本～") delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

- (void)creatAlertViewControllerWithMessage:(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:LocalizedString(@"提示") message:message preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *trueA = [UIAlertAction actionWithTitle:LocalizedString(@"更新") style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        [self openAppSore];
    }];
    UIAlertAction *falseA = [UIAlertAction actionWithTitle:LocalizedString(@"取消") style:(UIAlertActionStyleCancel) handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:trueA];
    [alert addAction:falseA];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)openAppSore {
    
    SKStoreProductViewController *storeProductViewContorller = [[SKStoreProductViewController alloc] init];
    //设置代理请求为当前控制器本身
    storeProductViewContorller.delegate = self;
    //加载一个新的视图展示
    [storeProductViewContorller loadProductWithParameters:
     //appId唯一的
     @{SKStoreProductParameterITunesItemIdentifier : AppStoreAppID} completionBlock:^(BOOL result, NSError *error) {
        //block回调
        if (error) {
            NSLog(@"error %@ with userInfo %@",error,[error userInfo]);
        } else {
            //模态弹出appstore
            [self presentViewController:storeProductViewContorller animated:YES completion:^{
                
            }
             ];
        }
    }];
}

//获取sdwebimage的缓存
- (CGFloat)getCache {
    NSUInteger bytesCache = [SDImageCache sharedImageCache].totalDiskSize;
    //换算成 MB (注意iOS中的字节之间的换算是1000不是1024)
    float MBCache = bytesCache/1000.0/1000.0;
    return MBCache;
}

#pragma mark - 💓💓 XibFunction ------
- (IBAction)buttonsClick:(UIButton *)sender {
    // 1绑定手机号 1意见反馈 2关于我们 3协议 4政策 5清除缓存 6更新
    switch (sender.tag) {
        case 90:{
            ChangeMyPassViewController *vc = [ChangeMyPassViewController  new];
//            SelectChangeTypeVC *vc = [[SelectChangeTypeVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;

        case 1:{
            FeedbackVC *vc = [[FeedbackVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 2:{
            AboutUsVC *vc = [[AboutUsVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 3:{
            // 协议
            [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"registerAgreement"} success:^(NSURLSessionDataTask *task, id responseObject) {
                [LJTools hideHud];
                if ([responseObject[@"code"] intValue] == SUCCESS) {
                    NSString *agreement = (NSString *)responseObject[@"data"];
                    WKWebViewController *vc = [WKWebViewController new];
                    vc.titleStr = @"用户注册协议";
                    vc.contentStr = agreement;
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
                }
            } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
                [LJTools showNOHud:RequestServerError delay:1.0];
            } IsNeedHub:YES];
        }
            break;
            
        case 4:{
            // 隐私政策
            [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"privacyPolicy"} success:^(NSURLSessionDataTask *task, id responseObject) {
                [LJTools hideHud];
                if ([responseObject[@"code"] intValue] == SUCCESS) {
                    NSString *privacy = (NSString *)responseObject[@"data"];
                    WKWebViewController *vc = [WKWebViewController new];
                    vc.titleStr = @"隐私政策";
                    vc.contentStr = privacy;
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
                }
            } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
                [LJTools showNOHud:RequestServerError delay:1.0];
            } IsNeedHub:YES];
            
        }
            break;
        case 5:{
            /** 异步清除图片缓存 （磁盘中的） */
            
            NSUInteger bytesCache = [SDImageCache sharedImageCache].totalDiskSize;
            float MBCache = bytesCache/1024.0/1024.0;
            
            if (MBCache < 0.01) {
                [LJTools showText:LocalizedString(@"暂无缓存无需清理") delay:1];
                
                return;
            }
            MJWeakSelf;
            [UIAlertController alertViewNormalWithTitle:@"确定清除本地缓存?" message:nil titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
                // 确定
                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
                        NSString *hint = [NSString stringWithFormat:LocalizedString(@"已清理%@空间"),self.cache.text];
                        [MBProgressHUD showAutoMessage:hint];
                        weakSelf.cache.text = [NSString stringWithFormat:@"%.2fM",[self getCache]];
                    }];
                });
            } okColor:MainColor cancleColor:[UIColor lightGrayColor] isHaveCancel:YES];
           
        }
            break;
        case 6:{
            // 版本更新
            [self promptUpdate];
        }
            break;
            
        case 10:{
            // 注意事项
            [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"masterReceiveRemind"} success:^(NSURLSessionDataTask *task, id responseObject) {
                [LJTools hideHud];
                if ([responseObject[@"code"] intValue] == SUCCESS) {
                    NSString *privacy = (NSString *)responseObject[@"data"];
                    WKWebViewController *vc = [WKWebViewController new];
                    vc.titleStr = @"注意事项";
                    vc.contentStr = privacy;
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
                }
            } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
                [LJTools showNOHud:RequestServerError delay:1.0];
            } IsNeedHub:YES];
        }
            break;
    }
}

- (IBAction)loginOutButtonAction:(id)sender {
    [UIAlertController alertViewNormalWithTitle:@"确定退出当前账号?" message:nil titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
        [User deleUser];
        // 同时推出IM
        [[V2TIMManager sharedInstance] logout:^{
            
        } fail:^(int code, NSString *desc) {
            
        }];
        // 确定
        LoginViewController *loginVC = [LoginViewController new];
        [LJTools.topViewController.navigationController pushViewController:loginVC animated:YES];
    } okColor:MainColor cancleColor:[UIColor lightGrayColor] isHaveCancel:YES];
    
}
#pragma mark - 💓💓 Lazy Loads ------
- (IBAction)loginButtonAction:(id)sender {
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"温馨提示" message:@"确定要注销账号吗？" titlesArry:@[@"确定"] indexBlock:^(NSInteger index, id obj) {
        [weakSelf requestForDeleteAccount];
    } okColor:[UIColor redColor] cancleColor:[UIColor grayColor] isHaveCancel:YES];

}
#pragma mark – 注销
- (void)requestForDeleteAccount {
    NSDictionary *dict = @{
        @"token_d":[User getUserToken]
    };
    [NetworkingTool postWithUrl:kDelUserURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [User deleUser];
            [UIApplication sharedApplication].keyWindow.rootViewController = [[NavigationController alloc] initWithRootViewController:[[LoginViewController alloc] init]];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
@end
