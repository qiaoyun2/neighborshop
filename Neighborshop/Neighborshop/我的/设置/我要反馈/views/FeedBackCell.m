//
//  FeedBackCell.m
//  ZZR
//
//  Created by null on 2021/7/7.
//

#import "FeedBackCell.h"
#import "YBImageBrowser.h"
//#import <YBImageBrowser/YBImageBrowser.h>

@implementation FeedBackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setModel:(FeedBackModel *)model{
    _model = model;
    [_imgsView removeAllSubviews];
    User * user = [User getUser];
    [_headImg sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.avatar)] placeholderImage:DefaultImgHeader];
    _contentLabel.text = model.content;
    if (model.contact.length>0) {
        _contactLabel.hidden = NO;
        _contactLabel.text = [NSString stringWithFormat:@"联系方式：%@",model.contact];
    }else{
        _contactLabel.hidden = YES;
    }
    
//    _typeLabel.text = [NSString stringWithFormat:@"反馈类型：%@",@""];
    if (model.typeContents.count>0) {
        _typeLabel.hidden = NO;
        _typeLabel.text = [NSString stringWithFormat:@"反馈类型：%@",model.typeContents[0]];
    }else{
        _typeLabel.hidden = YES;
    }
    _timeLabel.text = [NSString stringWithFormat:@"反馈时间：%@",model.createTime];
    if (!model.replyContent.length) {
        _replyView.hidden = YES;
    }else{
        _replyView.hidden = NO;
        NSString * totalStr = [NSString stringWithFormat:@"平台回复：%@",model.replyContent];
        NSString * str = @"平台回复：";
        NSAttributedString * attr = [self attrFromStr:totalStr toStr:str withFont:[UIFont systemFontOfSize:14] withColor:MainColor];
        _replyContentLabel.attributedText = attr;
    }
    
    if (model.imgArray.length) {
        NSArray *imgArray = [model.imgArray componentsSeparatedByString:@","];
        _imgsViewHeight.constant = 102;
        [self addImgsWithImgs:imgArray];
    }else {
        _imgsViewHeight.constant = 0;
    }

}
- (NSAttributedString*)attrFromStr:(NSString *)totalStr toStr:(NSString *)toStr withFont:(UIFont *)font withColor:(UIColor *)color{
    
    NSMutableAttributedString * attr = [[NSMutableAttributedString alloc]initWithString:totalStr];
    NSRange range = [totalStr rangeOfString:toStr];
    [attr setAttributes:@{NSFontAttributeName:font,NSForegroundColorAttributeName:color} range:range];
    
    return attr;
}
-(void)addImgsWithImgs:(NSArray*)imgs{
    int maxCount = (int)imgs.count;
    if (maxCount>3) {
        maxCount = 3;
    }
    CGFloat space = (SCREEN_WIDTH-60-16-3*90)/2;
    for (int i = 0; i<maxCount; i++) {
        NSString * imgUrl = imgs[i];
        UIImageView * imgV = [[UIImageView alloc]initWithFrame:CGRectMake((space+90)*i,12, 90, 90)];
        imgV.contentMode = UIViewContentModeScaleAspectFill;

        imgV.layer.cornerRadius = 8;
        imgV.layer.masksToBounds = YES;
        imgV.tag = 10+i;
        [imgV sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imgUrl)] placeholderImage:DefaultImgWidth];
        [_imgsView addSubview:imgV];
        imgV.userInteractionEnabled = YES;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [imgV addGestureRecognizer:tap];
    }
}
-(void)tap:(UITapGestureRecognizer*)tap{
    UIImageView * imgV = (UIImageView*)tap.view;
    NSArray *imgs = [_model.imgArray componentsSeparatedByString:@","];
    NSMutableArray * mutArray = [NSMutableArray new];
    for (int i = 0; i<imgs.count; i++) {
        YBIBImageData *data = [YBIBImageData new];
        data.imageURL = [NSURL URLWithString:kImageUrl(imgs[i])];
        data.projectiveView = [_imgsView viewWithTag:10+i];
        [mutArray addObject:data];
    }
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = mutArray;
    browser.currentPage = imgV.tag - 10;
    [browser show];
}
@end
