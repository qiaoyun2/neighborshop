//
//  MyBanksViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/20.
//

#import "MyBanksViewController.h"
#import "BindBankViewController.h"

@interface MyBanksViewController ()

@end

@implementation MyBanksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title  = @"我的银行卡";
}

- (IBAction)bindBtnClick:(id)sender {
    BindBankViewController *vc = [BindBankViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
