//
//  BindBankViewController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/20.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface BindBankViewController : BaseViewController
/// 是否来自绑定页
@property (nonatomic, assign) BOOL isFromSetting;
@end

NS_ASSUME_NONNULL_END
