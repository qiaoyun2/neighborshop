//
//  BindBankViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/20.
//

#import "BindBankViewController.h"
#import "QueryListByCodeModel.h"
#import "OrderCancelView.h"
#import "MyBindingVC.h"

@interface BindBankViewController ()
@property (weak, nonatomic) IBOutlet UIView *typeView;
@property (weak, nonatomic) IBOutlet UILabel *bankTypeLabel;
@property (weak, nonatomic) IBOutlet UITextField *numberText;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *phoneText;
@property (weak, nonatomic) IBOutlet UIView *bankTypeView;
@property (nonatomic, strong) OrderCancelView *cancelView;//取消view
@property (nonatomic , strong) NSString *bankType;

@property (nonatomic , strong) NSString *idStr;

@end

@implementation BindBankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"绑定银行卡";
    
    [self.nameText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.phoneText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    WeakSelf
    [self.bankTypeView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        [weakSelf.cancelView show];
        weakSelf.cancelView.reasonArr = weakSelf.dataArray;
    }];
    
    [self getQueryListByCode];
    
    if(self.isFromSetting){
        [self getBindingAccount];
    }
  
}
#pragma mark - 银行类型
- (void) getQueryListByCode {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"code"] = @"bankList";
    
    WeakSelf
    [NetworkingTool getWithUrl:kQueryListByCodeURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [weakSelf.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *dataDic = responseObject[@"data"];
            for (NSDictionary *dict in dataDic) {
                QueryListByCodeModel *model = [[QueryListByCodeModel alloc] initWithDictionary:dict];
                [weakSelf.dataArray addObject:model];
            }
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 获取绑定账号
- (void)getBindingAccount {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"type"] = @"3";
    
    WeakSelf
    [NetworkingTool getWithUrl:kTakeoutCashGetByTypeURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if([responseObject[@"data"] safeDictionary]){
                NSDictionary *dataDic = responseObject[@"data"];
                weakSelf.nameText.text = (NSString *)dataDic[@"realName"];
                weakSelf.phoneText.text = (NSString *)dataDic[@"phoneNumber"];
                weakSelf.bankTypeLabel.text = (NSString *)dataDic[@"bankTypeName"];
                weakSelf.numberText.text = (NSString *)dataDic[@"bankCardNumber"];
                weakSelf.idStr = (NSString *)dataDic[@"id"];
            }
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - Function
//输入框观察者事件
- (void)textFieldDidChange:(UITextField *)textField {
    
    if (textField == self.nameText) {
        if (textField.text.length > 10) {
            textField.text = [textField.text substringToIndex:10];
        }
    }
    if (textField == self.phoneText) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}
#pragma mark - 提交
- (IBAction)submitBtnClick:(id)sender {
    if([self.bankTypeLabel.text isEqualToString:@"请选择"] ){
        [LJTools showNOHud:@"请选择银行类型" delay:1.0];
        return;
    }
    if([self.numberText.text length] == 0){
        [LJTools showNOHud:@"请输入请输入银行卡号" delay:1.0];
        return;
    }
    if([self.nameText.text length] == 0){
        [LJTools showNOHud:@"请输入持卡人姓名" delay:1.0];
        return;
    }
    if([self.phoneText.text length] == 0){
        [LJTools showNOHud:@"请输入银行预留手机号" delay:1.0];
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"realName"] = self.nameText.text;
    parameters[@"bankType"] = self.bankType;
    parameters[@"type"] = @"3";
    parameters[@"bankCardNumber"] = self.numberText.text;
    parameters[@"phoneNumber"] = self.phoneText.text;
    
    NSString *url = @"";
    if(self.isFromSetting){
        parameters[@"id"] = self.idStr;
        url = kTakeoutCashAccountEditURL;
    }else{
        url = kTakeoutCashAccountAddURL;
    }
    
    [NetworkingTool postWithUrl:url params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"绑定成功" delay:1.0];
            for (UIViewController *vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[MyBindingVC class]]) {
                    [self.navigationController popToViewController:vc animated:YES];
                }
            }
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
    
}
#pragma mark - 懒加载
- (OrderCancelView *)cancelView{
    if (!_cancelView) {
        _cancelView = [[NSBundle mainBundle] loadNibNamed:@"OrderCancelView" owner:nil options:nil].lastObject;
        [_cancelView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        [[UIApplication sharedApplication].keyWindow addSubview:_cancelView];
        WeakSelf
        _cancelView.selectResonBlock = ^(QueryListByCodeModel * _Nonnull selectDic) {
            weakSelf.bankTypeLabel.text = selectDic.title;
            weakSelf.bankType = selectDic.value;
        };
    }
    return _cancelView;
}
@end
