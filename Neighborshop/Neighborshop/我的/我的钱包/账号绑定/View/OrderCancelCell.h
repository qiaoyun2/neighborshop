//
//  OrderCancelCell.h
//  ChaoPin
//
//  Created by ayoue on 2019/10/22.
//  Copyright © 2019 BenBenKeJi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderCancelCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *titleButton;
@end

NS_ASSUME_NONNULL_END
