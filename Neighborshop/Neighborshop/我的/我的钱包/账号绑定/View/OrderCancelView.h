//
//  OrderCancelView.h
//  ChaoPin
//
//  Created by ayoue on 2019/10/21.
//  Copyright © 2019 BenBenKeJi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QueryListByCodeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderCancelView : UIView<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottom;

@property (nonatomic, strong)NSMutableArray *reasonArr;
@property (nonatomic, assign)NSInteger index;

@property (nonatomic, copy)void (^selectResonBlock)(QueryListByCodeModel *model);

- (void)show;
- (void)hidden;

@end

NS_ASSUME_NONNULL_END
