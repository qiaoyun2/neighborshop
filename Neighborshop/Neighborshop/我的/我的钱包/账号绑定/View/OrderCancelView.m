//
//  OrderCancelView.m
//  ZhiNengShiBie
//
//  Created by ayoue on 2019/10/17.
//  Copyright © 2019 benbenkeji. All rights reserved.
//

#import "OrderCancelView.h"
#import "OrderCancelCell.h"

@implementation OrderCancelView

#pragma mark - UI
- (void)awakeFromNib{
    [super awakeFromNib];
    
    [self setUpCollectView];
    [self.backHeight setConstant:340+bottomBarH];
    [self.bottom setConstant:-340-bottomBarH];
    self.hidden = YES;
    [self layoutIfNeeded];
}

#pragma mark - Network

#pragma mark - Delegate
//collectView
- (void)setUpCollectView{
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    //注册cell
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderCancelCell" bundle:nil] forCellReuseIdentifier:@"OrderCancelCell"];
    
}
#pragma mark - Network

#pragma mark - Delegate
#pragma mark - tableviewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _reasonArr.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderCancelCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderCancelCell" forIndexPath:indexPath];
    QueryListByCodeModel *model = _reasonArr[indexPath.row];
    [cell.titleButton setTitle:[NSString stringWithFormat:@"  %@",model.title] forState:UIControlStateNormal];
    if (indexPath.row == _index) {
        cell.titleButton.selected = YES;
    }
    else
    {
        cell.titleButton.selected = NO;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _index = indexPath.row;
    [tableView reloadData];
}

#pragma mark - Function

- (void)setReasonArr:(NSMutableArray *)reasonArr{
    _reasonArr = reasonArr;
    _index = 0;
    [self.tableView reloadData];
}

- (void)show{
    self.hidden = NO;
    [self.bottom setConstant:0];
    [UIView animateWithDuration:.2 animations:^{
        [self setBackgroundColor:RGBA(0, 0, 0, 0.5)];
        [self layoutSubviews];
    }];
}

- (void)hidden{
    [self.bottom setConstant:-340-bottomBarH];
    [UIView animateWithDuration:.2 animations:^{
        [self setBackgroundColor:RGBA(0, 0, 0, 0)];
        [self layoutSubviews];
    } completion:^(BOOL finished) {
        if (finished) {
            [self setHidden:YES];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    CGPoint point = [[touches anyObject] locationInView:self];
    if (point.y < SCREEN_HEIGHT-340-bottomBarH) {
        [self hidden];
    }
}

#pragma mark - XibFunction

- (IBAction)cancelAction:(id)sender {
    [self hidden];
}

- (IBAction)submitAction:(UIButton *)sender {
     [self hidden];
    if (self.selectResonBlock) {
        self.selectResonBlock(_reasonArr[_index]);
    }
}
#pragma mark - 懒加载

@end
