//
//  QueryListByCodeModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/28.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QueryListByCodeModel : BaseModel
///显示名称
@property (nonatomic, copy) NSString *text;
///
@property (nonatomic, copy) NSString *title;
///
@property (nonatomic, copy) NSString *value;
@end

NS_ASSUME_NONNULL_END
