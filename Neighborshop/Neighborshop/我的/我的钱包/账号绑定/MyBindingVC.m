//
//  MyBindingVC.m
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import "MyBindingVC.h"
#import "MyBindingCell.h"
#import "MyBingdingInfoVC.h"
#import "BindingStatusModel.h"
#import "BindBankViewController.h"
#import  "MyBanksViewController.h"
//

@interface MyBindingVC ()
@property (nonatomic, strong) BindingStatusModel *statusModel;
@end

@implementation MyBindingVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self getBindingStatus];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"账号绑定";
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    
    
}
/// 获取绑定账号状态
- (void)getBindingStatus {
    [NetworkingTool getWithUrl:kTakeoutCashAccountURL params:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        [self.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *array = responseObject[@"data"];
            for (NSDictionary *dict in array) {
                BindingStatusModel *model = [[BindingStatusModel alloc] initWithDictionary:dict];
                [self.dataArray addObject:model];
            }
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
        [self.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *titleA = @[@"银行卡", @"微信", @"支付宝", ];
    //
    //    NSArray *imgA = @[@"路径 19587", @"路径 19218",];
    static NSString *identifier = @"MyBindingCell";
    MyBindingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell= [[[NSBundle mainBundle] loadNibNamed:@"MyBindingCell" owner:self options:nil] lastObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    cell.titleL.text = titleA[indexPath.row];
    cell.addView.hidden  =  NO;
    cell.bankView.hidden  = cell.wechatAlipayView.hidden = YES;
    if(indexPath.row ==  0){
        for (BindingStatusModel *model in self.dataArray) {
            if(model.type.intValue == 3){
                cell.bankNameLabel.text = [NSString stringWithFormat:@"银行类型：%@",model.bankTypeName];
                cell.bankNumLabel.text = [NSString stringWithFormat:@"银行账号：%@",model.bankCardNumber];
                cell.bankNumLabel.text = [NSString stringWithFormat:@"手机号：%@",model.phoneNumber];
                cell.nameLabel.text = [NSString stringWithFormat:@"持卡人：%@",model.realName];
                
                cell.addBtn.hidden = model.bankCardNumber.length > 0;
                cell.modifyBtn.hidden = model.bankCardNumber.length  == 0;
                cell.addView.hidden  = model.bankCardNumber > 0;
                cell.bankView.hidden  = model.bankCardNumber.length == 0;
            }
        }
    }else if (indexPath.row ==  1){
        for (BindingStatusModel *model in self.dataArray) {
            if(model.type.intValue == 1){
                [cell.qrCodeImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.qrCode)] placeholderImage:DefaultImgWidth];
                cell.addBtn.hidden = model.qrCode.length > 0;
                cell.modifyBtn.hidden = model.qrCode.length == 0;
                cell.addView.hidden  = model.qrCode.length > 0;
                cell.wechatAlipayView.hidden = model.qrCode.length == 0;
            }
        }
    }else{
        for (BindingStatusModel *model in self.dataArray) {
            if(model.type.intValue == 2){
                [cell.qrCodeImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.qrCode)] placeholderImage:DefaultImgWidth];
                cell.addBtn.hidden = model.qrCode.length > 0;
                cell.modifyBtn.hidden = model.qrCode.length == 0;
                cell.addView.hidden  = model.qrCode.length > 0;
                cell.wechatAlipayView.hidden = model.qrCode.length == 0;
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row  == 0){
        BindingStatusModel *list = [BindingStatusModel new];
        for (BindingStatusModel *model in self.dataArray) {
            if(model.type.intValue == 3){
                list = model;
                break;
            }
        }
        if(list.bankCardNumber.length > 0){
            BindBankViewController *vc = [BindBankViewController new];
            vc.isFromSetting = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            MyBanksViewController *vc = [MyBanksViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else{
        MyBingdingInfoVC *info = [[MyBingdingInfoVC alloc] init];
        info.state = indexPath.row;
        info.isFromSetting = YES;
        [self.navigationController pushViewController:info animated:YES];
    }
    
}

@end
