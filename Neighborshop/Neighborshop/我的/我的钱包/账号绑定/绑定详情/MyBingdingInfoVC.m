//
//  MyBingdingInfoVC.m
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import "MyBingdingInfoVC.h"
#import "MyBindingInfoHeaderView.h"
#import "MyBingdingInfoCell1.h"
#import "MyBingdingInfoCell2.h"
#import "MyBingdingInfoCell3.h"
#import "UploadManager.h"

@interface MyBingdingInfoVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSString *codeS;
@property (nonatomic, copy) NSString *nameS;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;
@property (nonatomic, strong) UIImage *codeImage;

@property (nonatomic, copy) NSString *qrcode;

@end

@implementation MyBingdingInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = _state == 1 ? @"绑定微信" : @"绑定支付宝";
    self.view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    [self.tableView registerNib:[UINib nibWithNibName:@"MyBindingInfoHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"MyBindingInfoHeaderView"];
    [self getBindingAccount];
}


/// 提交
- (void)submitInfo:(NSString *)codeImageUrl {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"realName"] = _nameS;
    parameters[@"qrCode"] = codeImageUrl;
    parameters[@"type"] = _state == 1 ? @"1" : @"2";
    parameters[@"captcha"] = _codeS;
//    parameters[@"event"] = @"verifyPhone";
    [NetworkingTool postWithUrl:kTakeoutCashAccountAddURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [super backItemClicked];
            [LJTools showOKHud:@"绑定成功" delay:1.0];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

/// 获取绑定账号
- (void)getBindingAccount {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"type"] = _state == 1 ? @"1" : @"2";
    [NetworkingTool getWithUrl:kTakeoutCashGetByTypeURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *dataDic = responseObject[@"data"];
            self.qrcode = (NSString *)dataDic[@"qrCode"];
            self.nameS = (NSString *)dataDic[@"realName"];
            [self.tableView reloadData];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

- (void)requestForSendCode:(UIButton *)sender
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = [User getUser].mobile;
    params[@"event"] = @"verifyPhone";
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [sender setCountdown:60 WithStartString:@"" WithEndString:@"获取验证码"];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {

    } IsNeedHub:YES];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 93;
    } else if (indexPath.section == 1) {
        return 144;
    }
    return 52;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    MyBindingInfoHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"MyBindingInfoHeaderView"];
    headerView.backgroundColor = [UIColor yellowColor];
    NSArray *titleA = @[@"第一步：先获取手机验证码", @"第二步：上传支付宝收款二维码", @"第三步：填写支付宝真实姓名"];
    if (_state == 1) {
        titleA = @[@"第一步：先获取手机验证码", @"第二步：上传微信收款二维码", @"第三步：填写微信真实姓名"];
    }
    [headerView.titleLabel setText:titleA[section]];
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *identifier = @"MyBingdingInfoCell1";
        MyBingdingInfoCell1 *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell= [[[NSBundle mainBundle] loadNibNamed:@"MyBingdingInfoCell1" owner:self options:nil] lastObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        User * user = [User getUser];
        [cell.contentL setText:[@"绑定的用户手机" stringByAppendingString:[NSString telephoneEncrypt:user.mobile]]];
        [cell.sendBtn addTarget:self action:@selector(sendBtnDidClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.textField.tag = indexPath.section + 99;
        [cell.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        if (![_codeS isBlankString]) {
            [cell.textField setText:_codeS];
        }
        
        return cell;
    } else if (indexPath.section == 1) {
        static NSString *identifier = @"MyBingdingInfoCell2";
        MyBingdingInfoCell2 *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"MyBingdingInfoCell2" owner:self options:nil] lastObject];
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.addImg.userInteractionEnabled = YES;
        if (_codeImage) {
            [cell.addImg setImage:_codeImage];
        } else if (_qrcode) {
            [cell.addImg sd_setImageWithURL:[NSURL URLWithString:kImageUrl(_qrcode)] placeholderImage:[UIImage imageNamed:@"组 35510"]];
        }
        
        @weakify(self);
        [cell setDidChooseImage:^(UIImage * _Nonnull image) {
            @strongify(self);
            self.codeImage = image;
        }];
        
        return cell;
    }
    static NSString *identifier = @"MyBingdingInfoCell3";
    MyBingdingInfoCell3 *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyBingdingInfoCell3" owner:self options:nil] lastObject];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.textField.tag = indexPath.section + 99;
    [cell.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    if (![_nameS isBlankString]) {
        [cell.textField setText:_nameS];
    }
    
    return cell;
}

#pragma mark - Function
//输入框观察者事件
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField.tag == 99) {
        if (textField.text.length > 6) {
            textField.text = [textField.text substringToIndex:6];
        }
        _codeS = textField.text;
    }
    if (textField.tag == 101) {
        if (textField.text.length > 10) {
            textField.text = [textField.text substringToIndex:10];
        }
        _nameS = textField.text;
    }
}

/// 发送验证码
/// @param sender <#sender description#>
- (void)sendBtnDidClick:(UIButton *)sender {
    [self requestForSendCode:sender];
}

#pragma mark - XibFunction
- (IBAction)bottomBtnDidClick:(UIButton *)sender {
    if (!_codeS || [_codeS isBlankString]) {
        [LJTools showNOHud:@"请填写验证码" delay:1.0];
        return;
    }
    
    if (!_codeImage && !_qrcode) {
        [LJTools showNOHud:_state == 1 ? @"请上传微信收款二维码" : @"请上传支付宝收款二维码" delay:1.0];
        return;
    }
    
    if (!_nameS || [_nameS isBlankString]) {
        [LJTools showNOHud:_state == 1 ? @"请填写微信真实姓名" : @"请填写支付宝真实姓名" delay:1.0];
        return;
    }
    
    if (_codeImage) {
        @weakify(self);
        [UploadManager uploadImageArray:@[_codeImage] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
            @strongify(self);
            [self submitInfo:imageUrl];
        }];
    } else {
        [self submitInfo:_qrcode];
    }
}

@end
