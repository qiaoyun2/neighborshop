//
//  MyBingdingInfoCell1.m
//  ZZR
//
//  Created by null on 2021/3/16.
//  Copyright © 2021 null. All rights reserved.
//

#import "MyBingdingInfoCell1.h"

@implementation MyBingdingInfoCell1

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //修改占位符文字颜色
    [self.textField setValue:UIColorFromRGB(0x999999) forKeyPath:@"placeholderLabel.textColor"];

    self.textField.tintColor = [UIColor whiteColor];      //设置光标颜色
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
