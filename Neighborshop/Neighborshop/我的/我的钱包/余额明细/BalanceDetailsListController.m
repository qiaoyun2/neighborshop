//
//  BalanceDetailsListController.m
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import "BalanceDetailsListController.h"
#import "BalanceDetailsListCell.h"

@interface BalanceDetailsListController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, copy) NSString *start_data;
@property (nonatomic, copy) NSString *end_data;

@end

@implementation BalanceDetailsListController

#pragma mark - Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"零钱明细";
    _start_data = @"";
    _end_data = @"";
    [self initTableView];
}

#pragma mark – UI
- (void)initTableView {
    [self.tableView registerNib:[UINib nibWithNibName:@"BalanceDetailsListCell" bundle:nil] forCellReuseIdentifier:@"BalanceDetailsListCell"];
    @weakify(self);
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        @strongify(self);
        [self refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        [self loadMore];
    }];
    [self refresh];
}

#pragma mark - Network

- (void)refresh {
    self.pageNum = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}

- (void)loadMore {
    self.pageNum++;
    [self getDataArrayFromServerIsRefresh:NO];
}

/** 5cc45422e5c87    余额明细，我的钱包首页的30天记录也走这个，其中时间格式start_data: 2022-04-01 end_data: 2022-04-22
 
 **/
- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"pageNo"] = @(self.pageNum);
    params[@"pageSize"] = @(10);
    
WeakSelf
    [NetworkingTool getWithUrl:kFlowRecordURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSArray *tempList = [BalanceDetailsModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"records"]];
            [weakSelf.dataArray addObjectsFromArray:tempList];
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf addBlankOnView:weakSelf.tableView];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        [weakSelf.tableView.mj_header endRefreshing];
        if(weakSelf.dataArray.count < 10){
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [weakSelf.tableView.mj_footer endRefreshing];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - Delegate
#pragma mark UITableViewDelgate
#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BalanceDetailsListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BalanceDetailsListCell" forIndexPath:indexPath];
    if (indexPath.row < self.dataArray.count) {
        cell.detailsModel = self.dataArray[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark – Function
#pragma mark – XibFunction
#pragma mark - Lazy Loads

- (void)setTimeString:(NSString *)timeString {
    _timeString = timeString;
    _start_data = [[timeString componentsSeparatedByString:@"至"] firstObject];
    _end_data = [[timeString componentsSeparatedByString:@"至"] lastObject];
    [self refresh];
}

@end
