//
//  BalanceDetailsListController.h
//  ZZR
//
//  Created by null on 2021/9/14.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/** BalanceDetailsListController 余额明细列表 */
@interface BalanceDetailsListController : BaseViewController

/// 0:全部 1:收入记录 2:支出记录
@property (nonatomic, assign) NSInteger fromType;
/// 时间
@property (nonatomic, copy) NSString *timeString;

@end

NS_ASSUME_NONNULL_END
