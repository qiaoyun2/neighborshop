//
//  CoinBillFlowCell.h
//  ZZR
//
//  Created by null on 2021/9/11.
//

#import <UIKit/UIKit.h>
#import "BalanceDetailsModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CoinBillFlowCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (nonatomic, strong) BalanceDetailsModel *detailsModel;

- (void)defaultValue;

@end

NS_ASSUME_NONNULL_END
