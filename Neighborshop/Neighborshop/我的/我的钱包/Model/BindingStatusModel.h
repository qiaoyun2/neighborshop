//
//  BindingStatusModel.h
//  ZZR
//
//  Created by null on 2021/12/22.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BindingStatusModel : BaseModel

@property (nonatomic, assign) BOOL zfbIsBind;
@property (nonatomic, assign) BOOL is_bank;
@property (nonatomic, assign) BOOL wxIsBind;
///银行卡号
@property (nonatomic, copy) NSString *bankCardNumber;;
///银行类型
@property (nonatomic, copy) NSString *bankType;
///银行类型名字
@property (nonatomic, copy) NSString *bankTypeName;
///ID
@property (nonatomic, copy) NSString *idStr;
///手机号
@property (nonatomic, copy) NSString *phoneNumber;
///二维码
@property (nonatomic, copy) NSString *qrCode;
///姓名
@property (nonatomic, copy) NSString *realName;
///账号类型:1-微信;2-支付宝;3-银行卡
@property (nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
