//
//  MyWalletModel.h
//  ZZR
//
//  Created by null on 2021/12/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyWalletModel : NSObject

@property (nonatomic, copy) NSString *todayIncome; // 今日收入
@property (nonatomic, copy) NSString *totalIncome; // 总收入
@property (nonatomic, copy) NSString *withdrawMoney; // 可提现金额


///// 冻结余额
//@property (nonatomic, copy) NSString *freeze_money;
///// 冻结规则
//@property (nonatomic, copy) NSString *freeze_money_rule;
///// 最小提现金额
//@property (nonatomic, copy) NSString *min_withdraw_money;
///// 本月收入
//@property (nonatomic, copy) NSString *month_money;
//@property (nonatomic, copy) NSString *pay_password;
//@property (nonatomic, copy) NSString *total_consumption_money;
//@property (nonatomic, copy) NSString *total_consumption_virtual_money;
///// 总收入
//@property (nonatomic, copy) NSString *total_money;
//@property (nonatomic, copy) NSString *total_order_commission_money;
///// 用户总金额=用户余额+冻结金额
//@property (nonatomic, copy) NSString *total_revenue_money;
//@property (nonatomic, copy) NSString *total_revenue_virtual_money;
///// 用户余额
//@property (nonatomic, copy) NSString *user_money;
//@property (nonatomic, copy) NSString *user_virtual_money;
//@property (nonatomic, copy) NSString *withdraw_handling_fee;
//@property (nonatomic, copy) NSString *withdraw_handling_type;
//@property (nonatomic, copy) NSString *money;
//@property (nonatomic, strong) NSNumber *share_money;
//@property (nonatomic, strong) NSNumber *discounts_money;
//@property (nonatomic, strong) NSNumber *score;


@end


NS_ASSUME_NONNULL_END
