//
//  WithdrawViewController.m
//  ZZR
//
//  Created by null on 2021/9/13.
//

#import "WithdrawViewController.h"
#import "WithdrawStatusViewController.h"
#import "MyBingdingInfoVC.h"
#import "WithdrawDetailsController.h"
#import "MyWalletModel.h"
#import "BindingStatusModel.h"
#import "MyBanksViewController.h"


@interface WithdrawViewController ()

/// 账户余额
@property (weak, nonatomic) IBOutlet UITextField *amountTextField;
/// 比例
@property (weak, nonatomic) IBOutlet UILabel *scaleLabel;
@property (weak, nonatomic) IBOutlet UIView *alipayView;
@property (weak, nonatomic) IBOutlet UIView *wechatView;
@property (weak, nonatomic) IBOutlet UIView *bankView;
///银行卡
@property (weak, nonatomic) IBOutlet UIButton *bankBtn;
///微信
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
///支付宝
@property (weak, nonatomic) IBOutlet UIButton *alipayBtn;

/// 提现方式
@property (weak, nonatomic) IBOutlet UIButton *alipayItem;
@property (weak, nonatomic) IBOutlet UIButton *wechatItem;
@property (weak, nonatomic) IBOutlet UIButton *cardItem;

@property (weak, nonatomic) IBOutlet UIButton *withdrawButton;

@property (nonatomic, strong) UIButton *selectedButton;
//@property (nonatomic, strong) MyWalletModel *model;
@property (nonatomic, strong) NSString *money;
@property (nonatomic, strong) NSString *minMoney;

@property (nonatomic, assign) int wxIsBind;
@property (nonatomic, assign) int zfbIsBind;
@property (nonatomic, assign) int yhkIsBind;

@end

@implementation WithdrawViewController

#pragma mark - Life Cycle Methods
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self getBindingStatus];
    [self getBindingStatusData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"提现";
    self.view.backgroundColor = UIColorNamed(@"F6F7F9");
    
    [_amountTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self getData];
    [self getConfigData];
    
}
#pragma mark – 获取绑定账号状态
- (void)getBindingStatus {
    [NetworkingTool getWithUrl:kGetBindInfoURL params:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            
            NSDictionary *dict = responseObject[@"data"];
            self.wxIsBind = [dict[@"wxIsBind"] intValue];
            self.zfbIsBind = [dict[@"zfbIsBind"] intValue];
            self.yhkIsBind = [dict[@"yhkIsBind"] intValue];
            if(self.zfbIsBind ==0 && self.wxIsBind == 0){
                if( self.yhkIsBind == 0){
                    self.alipayBtn.selected =  self.wechatBtn.selected = self.bankBtn.selected = NO;
                }else{
                    self.bankBtn.selected = YES;
                    self.selectedButton = self.bankBtn;
                }
            }else{
                if(self.zfbIsBind == 0){
                    self.wechatBtn.selected = YES;
                    self.selectedButton = self.wechatBtn;
                }else{
                    self.alipayBtn.selected = YES;
                    self.selectedButton = self.alipayBtn;
                }
            }
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark – UI
-(void)getData{
    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"takeoutWithdrawalAmount"} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            self.minMoney = responseObject[@"data"];
            self.scaleLabel.text = [NSString stringWithFormat:@"提现金额不能小于%@元",responseObject[@"data"]] ;
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}
#pragma mark -  获取绑定账号状态
- (void)getBindingStatusData {
    [NetworkingTool getWithUrl:kTakeoutCashAccountURL params:@{} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        [self.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *array = responseObject[@"data"];
            for (NSDictionary *dict in array) {
                BindingStatusModel *model = [[BindingStatusModel alloc] initWithDictionary:dict];
                [self.dataArray addObject:model];
            }
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
        
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - Network
- (void)getConfigData {
    
    [NetworkingTool getWithUrl:kMyWalletURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSString *money = [NSString stringWithFormat:@"%@",responseObject[@"data"]];
            self.money = money;
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - Delegate
#pragma mark - UITableViewDelgate
#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.length >= 1) {
        /// 删除数据, 都允许删除
        return YES;
    }
    NSString *money = [textField.text stringByAppendingString:string];
    if (![money validateMoney]) {
        if (textField.text.length > 0 && [string isEqualToString:@"."] && ![textField.text containsString:@"."]) {
            return YES;
        }
        return NO;
    }
    return YES;
    //    return money.floatValue <= _myWallerModel.user_money.floatValue;
}

#pragma mark - Function

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender {
    WithdrawDetailsController *withdrawDetailsVC = [WithdrawDetailsController new];
    [self.navigationController pushViewController:withdrawDetailsVC animated:YES];
}

/// 输入框观察者事件
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == _amountTextField && _amountTextField.text.length > 12) {
        _amountTextField.text = [_amountTextField.text substringToIndex:12];
    }
}

#pragma mark – XibFunction
- (IBAction)operationAction:(UIButton *)sender {
    switch (sender.tag) {
        case 801:
        {
            /// 全部
            _amountTextField.text = [NSString stringWithFormat:@"%.2f",[self.money floatValue]];
        }
            break;
        case 802:
        {
            /// 疑问
        }
            break;
        case 803:
        {
            [self.view endEditing:YES];
            /// 提现
            if ([_amountTextField.text integerValue] <= 0) {
                [LJTools showNOHud:@"请输入提现金额" delay:1];
                return;
            }
            if ([self.money doubleValue] < self.minMoney.doubleValue) {
                [LJTools showNOHud:@"您的余额不足" delay:1];
                return;
            }
            
            if ([_amountTextField.text doubleValue] < self.minMoney.doubleValue) {
                [LJTools showNOHud:[NSString stringWithFormat:@"提现金额不能小于%@元",self.minMoney] delay:1];
                return;
            }
            
            //提现方式 1：微信 2：支付宝 3银行
            NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
            parameters[@"amount"] = _amountTextField.text;
            if (self.selectedButton.tag == 900) {
                parameters[@"withdrawType"] = @"2";
            }if (self.selectedButton.tag == 901) {
                parameters[@"withdrawType"] = @"1";
            } else  {
                parameters[@"withdrawType"] = @"3";
            }
            
            [NetworkingTool postWithUrl:kWithdrawApplyURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
                [LJTools hideHud];
                if ([responseObject[@"code"] intValue] == SUCCESS) {
                    WithdrawStatusViewController *statusVC = [WithdrawStatusViewController new];
                    statusVC.withdrawType = [parameters[@"withdrawType"] intValue];
                    statusVC.money = self.amountTextField.text;
                    [self.navigationController pushViewController:statusVC animated:YES];
                } else {
                    [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
                }
            } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
                [LJTools showNOHud:RequestServerError delay:1.0];
            } IsNeedHub:YES];
        }
            break;
            
        default:
            break;
    }
}

/// 选择提现方式
/// @param sender <#sender description#>
- (IBAction)chooseWayAction:(UIButton *)sender {
    if (sender.isSelected) {
        return;
    }
    [self.view endEditing:YES];
    if (sender.tag == 900 && !self.zfbIsBind) {
        MyBingdingInfoVC *info = [[MyBingdingInfoVC alloc] init];
        info.state = 0;
        [self.navigationController pushViewController:info animated:YES];
        return;
    }
    if (sender.tag == 901 && !self.wxIsBind) {
        MyBingdingInfoVC *info = [[MyBingdingInfoVC alloc] init];
        info.state = 1;
        [self.navigationController pushViewController:info animated:YES];
        return;
    }
    if (sender.tag == 902 && !self.yhkIsBind) {
        MyBanksViewController *vc = [MyBanksViewController new];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    sender.selected = YES;
    _selectedButton.selected = NO;
    _selectedButton = sender;
    _alipayItem.selected = sender.tag == 900;
    _wechatItem.selected = sender.tag == 901;
    _cardItem.selected = sender.tag == 902;
}

#pragma mark - Lazy Loads

@end
