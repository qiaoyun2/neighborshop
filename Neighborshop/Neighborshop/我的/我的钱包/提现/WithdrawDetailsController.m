//
//  WithdrawDetailsController.m
//  ZZR
//
//  Created by null on 2021/11/23.
//

#import "WithdrawDetailsController.h"
#import "BalanceDetailsListCell.h"
#import "SignInView.h"
#import "WithdrawListModel.h"

@interface WithdrawDetailsController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, copy) NSString *start_data;
@property (nonatomic, copy) NSString *end_data;

@end

@implementation WithdrawDetailsController

#pragma mark - Life Cycle Methods

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [SignInView reinitialize];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.title = @"提现明细";
    self.view.backgroundColor = UIColorNamed(@"F6F7F9");
    _start_data = @"";
    _end_data = @"";
    [self setNavigationRightBarButtonWithImage:@"路径 19589"];
    [self initTableView];
}

#pragma mark – UI

- (void)initTableView {
    [self.tableView registerNib:[UINib nibWithNibName:@"BalanceDetailsListCell" bundle:nil] forCellReuseIdentifier:@"BalanceDetailsListCell"];

    MJWeakSelf;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {

        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{

        [weakSelf loadMore];
    }];
    
    [self.tableView.mj_header beginRefreshing];

}

#pragma mark - Network

- (void)refresh {
    self.pageNum = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}

- (void)loadMore {
    self.pageNum++;
    [self getDataArrayFromServerIsRefresh:NO];
}

/** 5ff5675a0bb6e    获取提现记录，其中时间格式start_data: 2022-04-01 end_data: 2022-04-22 **/
- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    parameters[@"change_type"] = @"4";
    parameters[@"startTime"] = _start_data;
    parameters[@"endTime"] = _end_data;
    parameters[@"pageNo"] = @(self.pageNum);
    parameters[@"pageSize"] = @(10);
    
    WeakSelf
    [NetworkingTool getWithUrl:kWithdrawRecordURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if (isRefresh) {
            [weakSelf.dataArray removeAllObjects];
        }
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *tempList = [WithdrawListModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"][@"records"]];
            [weakSelf.dataArray addObjectsFromArray:tempList];

            [weakSelf.tableView.mj_header endRefreshing];
            if ([responseObject[@"data"][@"current"] integerValue] <= weakSelf.pageNum) {
                [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
            } else {
                [weakSelf.tableView.mj_footer endRefreshing];
            }
        } else {
            [weakSelf.tableView.mj_header endRefreshing];
            [weakSelf.tableView.mj_footer endRefreshing];
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [weakSelf.tableView reloadData];
    } IsNeedHub:NO];
}

#pragma mark - Delegate
#pragma mark UITableViewDelgate
#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 10;
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BalanceDetailsListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BalanceDetailsListCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    if (indexPath.row < self.dataArray.count) {
        cell.isWithdraw = YES;
        cell.wModel = self.dataArray[indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
    view.backgroundColor = UIColorFromRGB(0xF6F7F9);
    return view;
}

#pragma mark – Function

- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender {
    MJWeakSelf;
    [SignInView title:@"选择时间" show:^(NSString * _Nonnull start, NSString * _Nonnull end) {
        weakSelf.start_data = start;
        weakSelf.end_data = end;
        [weakSelf refresh];
    }];
}

#pragma mark – XibFunction
#pragma mark - Lazy Loads

@end
