//
//  WithdrawListModel.h
//  StarCard
//
//  Created by ZZR on 2022/4/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawListModel : NSObject
//auditOpinion = "";
//createTime = "2022-10-27 15:39";
//money = 10;
//realMoney = "9.9";
//status = 0;
//withdrawType = 2;

///0-审核中 1-审核未通过 2-审核已通过
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *auditOpinion;
///创建时间
@property (nonatomic, copy) NSString *createTime;
///1：微信 2：支付宝
@property (nonatomic, copy) NSString *withdrawType;
///实际提现金额（显示这个吧，扣除手续费的）
@property (nonatomic, copy) NSString *realMoney;
///提现金额
@property (nonatomic, copy) NSString *money;

@end

NS_ASSUME_NONNULL_END
