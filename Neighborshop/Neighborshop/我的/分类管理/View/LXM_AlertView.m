//
//  LXM_AlertView.m
//  Luoxiaomeng
//
//  Created by 王巧云 on 2021/7/7.
//  Copyright © 2021 BenBenKeJi. All rights reserved.
//

#import "LXM_AlertView.h"
@interface LXM_AlertView ()
@property (weak, nonatomic) IBOutlet UITextField *titleText;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *tipsLabel;


@property (nonatomic, copy) void(^Block)(NSString *name);
@end
@implementation LXM_AlertView

-(void)awakeFromNib{
    [super awakeFromNib];
    [self.titleText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}
static LXM_AlertView *manager = nil;
+(instancetype)shareManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager= [[[NSBundle mainBundle] loadNibNamed:@"LXM_AlertView" owner:self options:nil] lastObject];
    });
    return manager;
}
//展示
-(void)show
{
    self.hidden = NO;
    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.contentView.alpha = 0;
    self.frame=CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        self.contentView.transform = transform;
        self.contentView.alpha = 1;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3f];
        
    } completion:^(BOOL finished) {
    }];
}
//隐藏
-(void)dismiss
{
    [UIView animateWithDuration:0.3 animations:^{
        self.contentView.transform=CGAffineTransformMakeScale(0.02, 0.02);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
//输入框观察者事件
- (void)textFieldDidChange:(UITextField *)textField {
    
    if (textField == self.titleText) {
        if (textField.text.length > 6) {
            textField.text = [textField.text substringToIndex:6];
        }
    }
}
+(LXM_AlertView *)title:(NSString *)title show:(void(^)(NSString *name))block{
    LXM_AlertView *view = [LXM_AlertView shareManager];
    view.Block = block;
    if(title.length > 0){
        view.titleText.text = title;
    }
    
    [view show];
    return view;
}

- (IBAction)cancelBtnClick:(id)sender {
    [self dismiss];
}

- (IBAction)sureBtnClick:(id)sender {
    if([self.titleText.text length] == 0){
        [LJTools showNOHud:@"请输入分类名称" delay:1.0];
        return;
    }
    [self dismiss];
    if (self.Block) {
        self.Block(self.titleText.text);
    }
}

@end
