//
//  LXM_AlertView.h
//  Luoxiaomeng
//
//  Created by 王巧云 on 2021/7/7.
//  Copyright © 2021 BenBenKeJi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LXM_AlertView : UIView

+(LXM_AlertView *)title:(NSString *)title show:(void(^)(NSString *name))block;
@end

NS_ASSUME_NONNULL_END
