//
//  MyClassViewController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/20.
//

#import "BaseViewController.h"
#import "TakeoutFoodCategoryModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MyClassViewController : BaseViewController
@property (nonatomic, assign)  BOOL isAdd;
@property (nonatomic, copy) void(^sureClickBlock)(TakeoutFoodCategoryModel *model);
@end

NS_ASSUME_NONNULL_END
