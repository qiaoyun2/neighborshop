//
//  MyClassViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/20.
//

#import "MyClassViewController.h"
#import "MyClassTableViewCell.h"
#import "LXM_AlertView.h"


@interface MyClassViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MyClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title  = @"分类管理";
    self.tableView.rowHeight = 55;
    [self loadBasicData];
}
#pragma mark - 网络请求
-(void)loadBasicData{
WeakSelf
    [NetworkingTool getWithUrl:kTakeoutFoodCategoryListURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                TakeoutFoodCategoryModel *model = [[TakeoutFoodCategoryModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }  else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [self addBlankOnView:self.tableView];
        self.noDataView.hidden = self.dataArray.count != 0;
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyClassTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyClassTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyClassTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    TakeoutFoodCategoryModel *model = self.dataArray[indexPath.row];
    cell.titleLabel.text = model.name;
    WeakSelf
    [cell.deleteBtn jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除该分类吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
            [weakSelf requestForDeleteOrder:model];
        } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
    }];
    [cell.titleLabel jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        if(weakSelf.isAdd){
            if(weakSelf.sureClickBlock){
                weakSelf.sureClickBlock(model);
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else{
            [weakSelf requestForEditOrder:model indexPath:indexPath];
        }
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
}
-(void)requestForEditOrder:(TakeoutFoodCategoryModel *)model indexPath:(NSIndexPath *)indexPath{
    WeakSelf
    [LXM_AlertView title:model.name show:^(NSString * _Nonnull name) {
        NSDictionary *dict = @{
            @"categoryId":model.categoryId,
            @"name":name,
            @"sort":model.sort
        };
        [NetworkingTool postWithUrl:kTakeoutFoodCategoryEditURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"] integerValue]==1) {
                [LJTools showText:@"修改成功" delay:1.5];
                model.name = name;
                [weakSelf.dataArray replaceObjectAtIndex:indexPath.row withObject:model];
                [weakSelf.tableView reloadData];
            }else {
                [LJTools showText:responseObject[@"msg"] delay:1.5];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];
        } IsNeedHub:YES];
    }];
}
#pragma mark - 删除
-(void)requestForDeleteOrder:(TakeoutFoodCategoryModel *)model{
    NSString *url = kTakeoutFoodCategoryDeleteURL;
    WeakSelf
    [NetworkingTool postWithUrl:url params:@{@"categoryId":model.categoryId} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [weakSelf.dataArray removeObject:model];
            
            [weakSelf addBlankOnView:self.tableView];
            weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
            
            [weakSelf.tableView reloadData];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark -  添加
- (IBAction)addBtnClick:(id)sender {
    WeakSelf
    [LXM_AlertView title:@"" show:^(NSString * _Nonnull name) {
        
        [NetworkingTool postWithUrl:kTakeoutFoodCategoryAddURL params:@{@"name":name} success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"] integerValue]==1) {
                [LJTools showText:@"添加成功" delay:1.5];
                [weakSelf loadBasicData];
            }else {
                [LJTools showText:responseObject[@"msg"] delay:1.5];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];
        } IsNeedHub:YES];
    }];
}

@end
