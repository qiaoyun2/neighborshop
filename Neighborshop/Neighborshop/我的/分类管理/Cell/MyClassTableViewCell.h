//
//  MyClassTableViewCell.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyClassTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;


@end

NS_ASSUME_NONNULL_END
