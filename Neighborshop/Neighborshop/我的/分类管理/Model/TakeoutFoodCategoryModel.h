//
//  TakeoutFoodCategoryModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/28.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TakeoutFoodCategoryModel : BaseModel
///分类名称前的小图标
@property (nonatomic, strong) NSString *categoryId;
///分类名称前的小图标
@property (nonatomic, strong) NSString *name;
///分类名称前的小图标
@property (nonatomic, strong) NSString *picture;
///排序
@property (nonatomic, strong) NSString *sort;
@end

NS_ASSUME_NONNULL_END
