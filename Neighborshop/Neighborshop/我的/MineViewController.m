//
//  MineViewController.m
//  PPKMaster
//
//  Created by null on 2022/4/16.
//

#import "MineViewController.h"
#import "VipCenterController.h"
//#import "PersonCertificationController.h"
#import "MyWalletViewController.h"
#import "MyCommentController.h"
#import "SettingController.h"
#import "PersonDataController.h"
#import "AboutUsVC.h"
//#import "CertificationFinishController.h"
#import "BindingStatusModel.h"
#import "MyWalletModel.h"
#import "MyBindingVC.h"
#import "WithdrawViewController.h"
#import "BalanceDetailsListController.h"
#import "MyClassViewController.h"
#import "TradingProfitViewController.h"
#import "AnswerQuestionsViewController.h"

@interface MineViewController ()
@property (weak, nonatomic) IBOutlet UIView *userInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UIView *vipView;
@property (weak, nonatomic) IBOutlet UIView *walletView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (nonatomic, strong) BindingStatusModel *statusModel;
@property (nonatomic, strong) MyWalletModel *myWallerModel;

@end

@implementation MineViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if ([LJTools islogin]) {
        [self requestForUserInfo];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    self.topViewHeight.constant = NavAndStatusHight;
    
    //    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:AppIsOnline];
    //    self.vipView.hidden = YES;
    //    self.walletView.hidden = YES;
    //    if ([str isEqualToString:@"1"]) {
    //        self.vipView.hidden = NO;
    //        self.walletView.hidden = NO;
    //        [self.view layoutIfNeeded];
    //    }
    [self getMyWalletInfo];

    WeakSelf
    [self.userInfoView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        PersonDataController *vc = [[PersonDataController alloc] init];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
}

/// 获取我的钱包信息
- (void)getMyWalletInfo {
    [NetworkingTool getWithUrl:kMyWalletURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSString *money = [NSString stringWithFormat:@"%@",responseObject[@"data"]];
//            self.myWallerModel = [MyWalletModel mj_objectWithKeyValues:responseObject[@"data"]];
            self.priceLabel.attributedText = [LJTools attributString:@"¥" twoStr:[NSString stringWithFormat:@"%.2f",[money floatValue]] color:UIColorFromRGB(0xFA2033) oneHeight:14 andTColor:UIColorFromRGB(0xFA2033) twoHeight:20];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - Network
- (void)requestForUserInfo
{
    [NetworkingTool getWithUrl:kGetUserInfoURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"]];
            [User saveUser:user];
            [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(user.avatar)] placeholderImage:DefaultImgHeader];
            self.nameLabel.text = user.user_nickname;
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:NO];
}

#pragma mark - XibFunction
- (IBAction)buttonsAction:(UIButton *)sender {
    switch (sender.tag) {
        case 0:
        {
            //            if (!_statusModel.wxIsBind && !_statusModel.zfbIsBind ) {
            //                MyBindingVC *bindingVC = [MyBindingVC new];
            //                [self.navigationController pushViewController:bindingVC animated:YES];
            //                return;
            //            }
            WithdrawViewController *withdrawVC = [WithdrawViewController new];
            withdrawVC.myWallerModel = _myWallerModel;
//            withdrawVC.statusModel = _statusModel;
            [self.navigationController pushViewController:withdrawVC animated:YES];
        }
            break;
            
        case 1://提现账号管理
        {
            MyBindingVC *bindingVC = [MyBindingVC new];
            [self.navigationController pushViewController:bindingVC animated:YES];
        }
            break;
            
        case 2:
        {
            // 账户明细
            BalanceDetailsListController *vc = [[BalanceDetailsListController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 3:
        {
            // 分类管理
            MyClassViewController *vc = [[MyClassViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 4:
        {
            // 营业设置
            TradingProfitViewController *vc = [[TradingProfitViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 5:
        {
            // 商家答疑
            AnswerQuestionsViewController *vc = [AnswerQuestionsViewController new];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
            
        case 6:
        {
            // 设置
            SettingController *vc = [[SettingController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
            break;
    }
}


@end
