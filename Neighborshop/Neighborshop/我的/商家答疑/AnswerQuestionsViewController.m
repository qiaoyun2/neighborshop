//
//  AnswerQuestionsViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/20.
//

#import "AnswerQuestionsViewController.h"
#import "AnswerQueTableViewCell.h"
#import "WKWebViewController.h"

@interface AnswerQuestionsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@end

@implementation AnswerQuestionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商家答疑";
    self.headerView.frame = CGRectMake(0, 0, ScreenWidth, W(148)+68);
    self.tableView.tableHeaderView = self.headerView;
    [self loadBasscData];
}
#pragma mark - 网络请求
-(void)loadBasscData{
    //
    WeakSelf
    [NetworkingTool getWithUrl:kMerchantAnswerQuestionURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [weakSelf.dataArray removeAllObjects];
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *dict = responseObject[@"data"];
            weakSelf.phoneLabel.text = dict[@"contactMobile"];
            NSArray *array = dict[@"questionVOList"];
            if(array.count > 0 && [array isKindOfClass:[NSArray class]]){
                [weakSelf.dataArray addObjectsFromArray:array];
            }
            [weakSelf.tableView reloadData];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AnswerQueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AnswerQueTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AnswerQueTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dict = self.dataArray[indexPath.row];
    cell.titleLabel.text = dict[@"title"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.dataArray[indexPath.row];
    WKWebViewController *vc = [WKWebViewController new];
    vc.titleStr = dict[@"title"];
    vc.contentStr = dict[@"content"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 打电话
- (IBAction)callBtnClick:(id)sender {
    NSString *phone =  self.phoneLabel.text;
    [LJTools call:phone];
}



@end
