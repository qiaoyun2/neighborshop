//
//  FoundListImage9TypographyView.m
//  LePin
//
//  Created by  ebz on 2020/7/30.
//  Copyright © 2020 ebz. All rights reserved.
//

#import "FoundListImage9TypographyView.h"
#import "FoundListImage9TypographyDetailManager.h"
#import "YBImageBrowser.h"
#import "YBIBVideoData.h"


static NSInteger const imgTag = 200;

@interface FoundListImage9TypographyView ()

@property (nonatomic, strong) UIImageView *imgV1;
@property (nonatomic, strong) UIImageView *imgV2;
@property (nonatomic, strong) UIImageView *imgV3;
@property (nonatomic, strong) UIImageView *imgV4;
@property (nonatomic, strong) UIImageView *imgV5;
@property (nonatomic, strong) UIImageView *imgV6;
@property (nonatomic, strong) UIImageView *imgV7;
@property (nonatomic, strong) UIImageView *imgV8;
@property (nonatomic, strong) UIImageView *imgV9;

@property (nonatomic, strong) NSMutableArray *imgVArray; // 存放imageView
@end

@implementation FoundListImage9TypographyView


#pragma mark - Life Cycle Methods
#pragma mark – UI
#pragma mark - Network
#pragma mark - Private Methods
//- (void)setVideo:(NSString *)videoPath coverImage:(NSString *)imagePath
//{
//    self.videoPath
//    [self setImageArray:@[imagePath]];
//}

- (void)setVideoPath:(NSString *)videoPath
{
    _videoPath = videoPath;
}



- (void)setImageArray:(NSArray *)imageArray {
    _imageArray = imageArray;
    // 图片显示几张
    if (imageArray.count == 0) {
        [self isDisplayImageViewImgCount:1];
    } else {
        [self isDisplayImageViewImgCount:imageArray.count];
    }
    
    switch (imageArray.count) {
        case 0:
        {
            [FoundListImage9TypographyDetailManager zoreImageTypographyImgV1:self.imgV1 currentView:self];
        }
            break;
        case 1:
            [FoundListImage9TypographyDetailManager oneImageTypographyImgV1:self.imgV1 currentView:self];
            break;
        case 2:
            [FoundListImage9TypographyDetailManager twoImageTypography:self.imgVArray imgV1:self.imgV1 imgV2:self.imgV2 currentView:self];
            break;
        case 3:
            [FoundListImage9TypographyDetailManager threeImageTypography:self.imgVArray imgV1:self.imgV1];
            break;
        case 4:
            [FoundListImage9TypographyDetailManager fourImageTypography:self.imgVArray imgV1:self.imgV1 imgV2:self.imgV2 imgV3:self.imgV3 imgV4:self.imgV4 currentView:self];
            break;
        case 5:
            [FoundListImage9TypographyDetailManager fiveImageTypography:self.imgVArray imgV1:self.imgV1 imgV2:self.imgV2];
            break;
        case 6:
            [FoundListImage9TypographyDetailManager sixImageTypography:self.imgVArray imgV1:self.imgV1];
            break;
        case 7:
        case 8:
        case 9:
        default:
            [FoundListImage9TypographyDetailManager sevenImageTypography:self.imgVArray imgV1:self.imgV1 imgV2:self.imgV2 imgV3:self.imgV3 imgV4:self.imgV4];
            break;
    }
    // 赋值
    if (imageArray.count > 0) {
        [FoundListImage9TypographyDetailManager setValeForImageView:self.imgVArray imageArray:_imageArray];
    }
}

#pragma mark 不显示的图片是否要创建
- (void)isDisplayImageViewImgCount:(NSInteger)imgCount {
    //    if (imgCount >= 1) [self addSubview:self.imgV1];
    //    if (imgCount >= 2) [self addSubview:self.imgV2];
    //    if (imgCount >= 3) [self addSubview:self.imgV3];
    //    if (imgCount >= 4) [self addSubview:self.imgV4];
    //    if (imgCount >= 5) [self addSubview:self.imgV5];
    //    if (imgCount >= 6) [self addSubview:self.imgV6];
    //    if (imgCount >= 7) [self addSubview:self.imgV7];
    //    if (imgCount >= 8) [self addSubview:self.imgV8];
    //    if (imgCount >= 9) [self addSubview:self.imgV9];
    
    // 判断imgV是否要添加到页面上
    self.imgVArray = [NSMutableArray array];
    if (imgCount >= 1) {
        [self addSubview:self.imgV1];
        [self.imgVArray addObject:self.imgV1];
    }
    if (imgCount >= 2) {
        [self addSubview:self.imgV2];
        [self.imgVArray addObject:self.imgV2];
    }
    if (imgCount >= 3) {
        [self addSubview:self.imgV3];
        [self.imgVArray addObject:self.imgV3];
    }
    if (imgCount >= 4) {
        [self addSubview:self.imgV4];
        [self.imgVArray addObject:self.imgV4];
    }
    if (imgCount >= 5) {
        [self addSubview:self.imgV5];
        [self.imgVArray addObject:self.imgV5];
    }
    if (imgCount >= 6) {
        [self addSubview:self.imgV6];
        [self.imgVArray addObject:self.imgV6];
    }
    if (imgCount >= 7) {
        [self addSubview:self.imgV7];
        [self.imgVArray addObject:self.imgV7];
    }
    if (imgCount >= 8) {
        [self addSubview:self.imgV8];
        [self.imgVArray addObject:self.imgV8];
    }
    if (imgCount >= 9) {
        [self addSubview:self.imgV9];
        [self.imgVArray addObject:self.imgV9];
    }
    
    // 如果已经添加过了，需要判断图片是否要显示
    if (_imgV1)_imgV1.hidden = !(imgCount >= 1) ? YES : NO;
    if (_imgV2)_imgV2.hidden = !(imgCount >= 2) ? YES : NO;
    if (_imgV3)_imgV3.hidden = !(imgCount >= 3) ? YES : NO;
    if (_imgV4)_imgV4.hidden = !(imgCount >= 4) ? YES : NO;
    if (_imgV5)_imgV5.hidden = !(imgCount >= 5) ? YES : NO;
    if (_imgV6)_imgV6.hidden = !(imgCount >= 6) ? YES : NO;
    if (_imgV7)_imgV7.hidden = !(imgCount >= 7) ? YES : NO;
    if (_imgV8)_imgV8.hidden = !(imgCount >= 8) ? YES : NO;
    if (_imgV9)_imgV9.hidden = !(imgCount >= 9) ? YES : NO;
    
}

#pragma mark - DataSourceAndDelegate

#pragma mark - Lazy Loads
#pragma mark 9张图片
- (UIImageView *)imgV1 {
    if (_imgV1 == nil) {
        _imgV1 = [[UIImageView alloc] init];
        _imgV1.tag = imgTag;
        
        if (self.videoPath.length>0) {
            UIView *coverView = [[UIView alloc] init];
            coverView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
            coverView.userInteractionEnabled = NO;
            [_imgV1 addSubview:coverView];
            [coverView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(_imgV1);
            }];
            
            UIImageView *playImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"路径 86592"]];
            [_imgV1 addSubview:playImageView];
            [playImageView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.centerY.equalTo(_imgV1);
                make.width.height.mas_equalTo(18);
            }];
        }
        [self setImageViewAttribute:_imgV1];
    }
    return _imgV1;
}
- (UIImageView *)imgV2 {
    if (_imgV2 == nil) {
        _imgV2 = [[UIImageView alloc] init];
        _imgV2.tag = imgTag+1;
        [self setImageViewAttribute:_imgV2];
    }
    return _imgV2;
}
- (UIImageView *)imgV3 {
    if (_imgV3 == nil) {
        _imgV3 = [[UIImageView alloc] init];
        _imgV3.tag = imgTag+2;
        [self setImageViewAttribute:_imgV3];
    }
    return _imgV3;
}
- (UIImageView *)imgV4 {
    if (_imgV4 == nil) {
        _imgV4 = [[UIImageView alloc] init];
        _imgV4.tag = imgTag+3;
        [self setImageViewAttribute:_imgV4];
    }
    return _imgV4;
}
- (UIImageView *)imgV5 {
    if (_imgV5 == nil) {
        _imgV5 = [[UIImageView alloc] init];
        _imgV5.tag = imgTag+4;
        [self setImageViewAttribute:_imgV5];
    }
    return _imgV5;
}
- (UIImageView *)imgV6 {
    if (_imgV6 == nil) {
        _imgV6 = [[UIImageView alloc] init];
        _imgV6.tag = imgTag+5;
        [self setImageViewAttribute:_imgV6];
    }
    return _imgV6;
}
- (UIImageView *)imgV7 {
    if (_imgV7 == nil) {
        _imgV7 = [[UIImageView alloc] init];
        _imgV7.tag = imgTag+6;
        [self setImageViewAttribute:_imgV7];
    }
    return _imgV7;
}
- (UIImageView *)imgV8 {
    if (_imgV8 == nil) {
        _imgV8 = [[UIImageView alloc] init];
        _imgV8.tag = imgTag+7;
        [self setImageViewAttribute:_imgV8];
    }
    return _imgV8;
}
- (UIImageView *)imgV9 {
    if (_imgV9 == nil) {
        _imgV9 = [[UIImageView alloc] init];
        _imgV9.tag = imgTag+8;
        [self setImageViewAttribute:_imgV9];
    }
    return _imgV9;
}

- (NSMutableArray *)imgVArray {
    if (_imgVArray == nil) {
        _imgVArray = [NSMutableArray array];
    }
    return _imgVArray;
}

#pragma mark – Function
- (void)setImageViewAttribute:(UIImageView *)imgV {
    imgV.contentMode = UIViewContentModeScaleAspectFill;
    imgV.clipsToBounds = YES;
    imgV.userInteractionEnabled = YES;
    imgV.layer.cornerRadius = 8;
    //    [self addSubview:imgV];
    WeakSelf;
//    [imgV mdf_whenSingleTapped:^(UIGestureRecognizer *gestureRecognizer) {
//        if (weakSelf.clickImageVCallBack) {
//            weakSelf.clickImageVCallBack(imgV.tag-imgTag);
//        }
//    }];
    UITapGestureRecognizer *tapImg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [imgV addGestureRecognizer:tapImg];
}
- (void)tapAction:(UITapGestureRecognizer *)sender {
    UIImageView *imgV = (UIImageView *)sender.view;
    if (self.is_tap) {
        [self showBrowserWithIndex:imgV.tag - imgTag];
        return;
    }
    if (self.clickImageVCallBack) {
        self.clickImageVCallBack(imgV.tag-imgTag);
    }
}

- (UIImageView *)getImageV:(NSInteger)index {
    
    UIImageView *imgV = (UIImageView *)[self viewWithTag:imgTag+index];
    return imgV ? : nil;
}

#pragma mark 点击放大图片
- (void)showBrowserWithIndex:(NSInteger)index
{
    WeakSelf;
    NSMutableArray *datas = [NSMutableArray array];
    if (self.videoPath.length>0) {
        YBIBVideoData *data = [YBIBVideoData new];
        data.videoURL = [NSURL URLWithString:kImageUrl(self.videoPath)];
        data.projectiveView = [self getImageV:index];
        [datas addObject:data];
    }
    else {
        [self.imageArray enumerateObjectsUsingBlock:^(NSString *_Nonnull imageStr, NSUInteger idx, BOOL * _Nonnull stop) {
            // 网络图片
            YBIBImageData *data = [YBIBImageData new];
            data.imageURL = [NSURL URLWithString:kImageUrl(imageStr)];
            data.projectiveView = [weakSelf getImageV:idx];
            [datas addObject:data];
        }];
    }

    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}

#pragma mark – XibFunction

@end
