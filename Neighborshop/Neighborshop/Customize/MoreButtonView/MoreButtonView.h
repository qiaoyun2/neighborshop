//
//  MoreButtonView.h
//  DZBase
//
//  Created by ayoue on 2020/8/5.
//  Copyright © 2020 BenBenKeJi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MoreButtonView : UIView

- (instancetype)initWithFrame:(CGRect)frame titleArr:(NSArray *)titleArr;

//左边的间距
@property (nonatomic, assign) CGFloat leftSpace;
/** 列间距 */
@property (nonatomic, assign) CGFloat rowSpace;
/** 行间距 */
@property (nonatomic, assign) CGFloat sectionSpace;
// 
@property (nonatomic, strong) UIColor *normalColor;
// 选择的颜色
@property (nonatomic, strong) UIColor *selectColor;
//文字字号
@property (nonatomic, assign) CGFloat normalFontSize;
// 是否多选
@property (nonatomic, assign) BOOL isMoreSelect;
// 是否圆角  默认是yes
@property (nonatomic, assign) BOOL isCornerRadius;
@property (nonatomic, copy) void(^Block)(NSInteger index);
//属性设置后 刷新下view
- (void)reloadSubviews;
//只有在属性刷新后才能获取到准确的高度
- (CGFloat)getViewHeight;
//下标 逗号隔开 
- (NSString *)getSelectIndex;
-(void)resetIndex:(NSInteger)index;
@end

NS_ASSUME_NONNULL_END
