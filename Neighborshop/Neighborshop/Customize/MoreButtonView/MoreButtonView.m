//
//  MoreButtonView.m
//  DZBase
//
//  Created by ayoue on 2020/8/5.
//  Copyright © 2020 BenBenKeJi. All rights reserved.
//

#import "MoreButtonView.h"

@interface MoreButtonView ()

@property (nonatomic, strong) NSArray *titleArr;

@property (nonatomic, strong) NSMutableArray *buttonArr;

@property (nonatomic, assign) CGFloat viewHeight;

@end

@implementation MoreButtonView

- (instancetype)initWithFrame:(CGRect)frame titleArr:(NSArray *)titleArr{
    if (self == [super initWithFrame:frame]) {
        _titleArr = titleArr;
        self.rowSpace = 15.0f;
        self.sectionSpace = 12.0f;
        self.leftSpace = 12.0f;
        self.normalColor = [UIColor whiteColor];
        self.selectColor = UIColorFromRGB(0xF6F7F9);
        self.normalFontSize = 12.0f;
        self.isCornerRadius = YES;
        self.isMoreSelect = NO;
        self.buttonArr = [NSMutableArray array];
        [self setUpSignLabelViewWithTitleArr:titleArr];
    }
    return self;
}

- (void)setUpSignLabelViewWithTitleArr:(NSArray *)titleArr{
    
    [self.subviews respondsToSelector:@selector(removeFromSuperview)];
    
    CGFloat x = self.leftSpace;
    CGFloat y = 0;
    CGFloat lastHeight = 24;
    CGFloat normalHeight = self.normalFontSize + 13;
    for (NSInteger i = 0 ; i < titleArr.count ; i++) {
        
        CGFloat width  = 0.0;
        CGFloat height = 0.0;
        
        CGSize size = [titleArr[i] boundingRectWithSize:CGSizeMake(self.bounds.size.width-self.leftSpace*2-30, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:self.normalFontSize],} context:nil].size;
        
        width = size.width+30;
        if (x+size.width+self.rowSpace+self.leftSpace*2 > self.bounds.size.width) {
            x = self.leftSpace;
            y += lastHeight+self.sectionSpace;
        }
        height = size.height+13>normalHeight?size.height+13:normalHeight;
        
        NSLog(@"width  %f  height %f",size.width+30,size.height+13);
        UIButton * btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
        [btn setTitle:titleArr[i] forState:(UIControlStateNormal)];
        [btn setBackgroundImage:[UIImage imageWithColor:self.normalColor] forState:(UIControlStateNormal)];
        [btn setBackgroundImage:[UIImage imageWithColor:self.selectColor] forState:(UIControlStateSelected)];
        [btn setFrame:CGRectMake(x, y, width, height)];
        
        [btn setClipsToBounds:YES];
        [btn setTag:i];
        if (self.isCornerRadius) {
            [btn.layer setCornerRadius:4.0];
            [btn.layer setMasksToBounds:YES];
        }
        [btn.titleLabel setFont:[UIFont systemFontOfSize:self.normalFontSize]];
        [btn.titleLabel setNumberOfLines:0];
        [btn setTitleColor:UIColorFromRGB(0x333333) forState:(UIControlStateNormal)];
        [btn setTitleColor:MainColor forState:(UIControlStateSelected)];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
        [btn addTarget:self action:@selector(selectSignAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [self addSubview:btn];
        if (i==0) {
            btn.selected = YES;
            [btn.titleLabel setFont:[UIFont boldSystemFontOfSize:12.0]];
        }
        [self.buttonArr addObject:btn];
        x += width+self.rowSpace;
        
        lastHeight = height;
    }
    self.viewHeight = y + lastHeight;
}

#pragma mark - setter
- (void)setLeftSpace:(CGFloat)leftSpace{
    _leftSpace = leftSpace;
}

- (void)setRowSpace:(CGFloat)rowSpace{
    _rowSpace = rowSpace;
}

- (void)setSectionSpace:(CGFloat)sectionSpace{
    _sectionSpace = sectionSpace;
}

- (void)reloadSubviews{
    [self setUpSignLabelViewWithTitleArr:self.titleArr];
}

- (void)setNormalColor:(UIColor *)normalColor{
    _normalColor = normalColor;
    for (UIButton *sender in self.buttonArr) {
        [sender setBackgroundImage:[UIImage imageWithColor:normalColor] forState:(UIControlStateNormal)];
    }
}

- (void)setSelectColor:(UIColor *)selectColor{
    _selectColor = selectColor;
    for (UIButton *sender in self.buttonArr) {
        [sender setBackgroundImage:[UIImage imageWithColor:selectColor] forState:(UIControlStateSelected)];
    }
}

- (void)setIsCornerRadius:(BOOL)isCornerRadius{
    _isCornerRadius = isCornerRadius;
    for (UIButton *sender in self.buttonArr) {
        if (isCornerRadius) {
            [sender.layer setCornerRadius:(self.normalFontSize + 12)/2];
            [sender.layer setMasksToBounds:YES];
        }else{
            [sender.layer setCornerRadius:0];
        }
    }
}

#pragma mark - getter
- (CGFloat)getViewHeight{
    return self.viewHeight;
}
-(void)resetIndex:(NSInteger)index
{
    [NSArray enumerateObjectsWithArray:self.buttonArr usingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        obj.layer.borderWidth = 0.5;
        obj.cornerRadius = obj.bounds.size.height/2;
        if (index==idx) {
            obj.selected = YES;
            [obj.titleLabel setFont:obj.selected?[UIFont boldSystemFontOfSize:12.0]:[UIFont systemFontOfSize:12.0]];
            [obj setTitleColor:UIColorFromRGB(0x00A2EA) forState:UIControlStateSelected];
            obj.layer.borderColor = UIColorFromRGB(0x00A2EA).CGColor;
        }else
        {
            obj.selected = NO;
            [obj setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateSelected];
            [obj.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
            obj.layer.borderColor = UIColorFromRGB(0xF5F6F9).CGColor;
        }
    }];
}
- (void)selectSignAction:(UIButton *)sender{
    if (!self.isMoreSelect) {
        for (UIButton *btn in self.buttonArr) {
            btn.selected = NO;
            [btn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateSelected];
            [btn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
            btn.layer.borderColor = UIColorFromRGB(0xF5F6F9).CGColor;
        }
    }
    sender.selected = !sender.selected;
    [sender.titleLabel setFont:sender.selected?[UIFont boldSystemFontOfSize:12.0]:[UIFont systemFontOfSize:12.0]];
    
    [sender setTitleColor:UIColorFromRGB(0x00A2EA) forState:UIControlStateSelected];
    sender.layer.borderColor = UIColorFromRGB(0x00A2EA).CGColor;
    
    !self.Block?:self.Block(sender.tag);
}

- (NSString *)getSelectIndex{
    
    NSMutableArray *tmpArr = [NSMutableArray array];
    
    [self.buttonArr enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.selected) {
            [tmpArr addObject:[NSString stringWithFormat:@"%ld",idx]];
        }
    }];
    
    return [tmpArr componentsJoinedByString:@","];
}


@end
