//
//  LJTools.h
//  ZZR
//
//  Created by null on 2018/12/13.
//  Copyright © 2018 null. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "MBProgressHUD+NH.h"

#define DEFAULT_HIDE_DELAY 1.0

NS_ASSUME_NONNULL_BEGIN

@interface LJTools : NSObject
#pragma mark -- 设置textfield占位颜色
+ (void)changePlaceholderColor:(NSString *)placeholder textField:(UITextField *)textField color:(UIColor *)color;
+ (MBProgressHUD *)sharedManager;
//是否登录
+ (BOOL)islogin;
//没有登录去登录
+ (BOOL)panduanLoginWithViewContorller:(UIViewController *)viewController isHidden:(BOOL)ishidden;
//清除空格
+ (NSString*)removeWhiteSpaceWithString:(NSString *)string;
//提示窗口
+ (void)MsgBox:(NSString *)msg title:(NSString *)title;

+ (NSString *) getIOSDeviceInfo;
//获取当前app版本号
+ (NSString *) getAppVersion;
//获取当前window
+ (UIWindow *)getAppWindow;
//获取入口类
+ (AppDelegate *)getAppDelegate;
//获取当前的ViewController
+ (UIViewController *)topViewController;

#pragma mark -- 获取当前显示的控制器 viewController
+ (UIViewController *)getCurrentActivityViewController;
//获得当前时间
+(NSString *)getCurrentTime;
//获得一条线
+(UIView *)getAlineView:(CGRect)frame;
+(NSString *)flattenHTML:(NSString *)html trimWhiteSpace:(BOOL)trim;
#pragma mark -
#pragma mark 切割图片
+ (UIImage *)originImage:(UIImage *)image scaleToSize:(CGSize)size;

#pragma mark -- 计算图片大小
+ (double)calulateImageFileSize:(UIImage *)image;
#pragma mark -
#pragma mark 获得时间差
+(NSString *)getUTCFormateDate:(NSString *)newsDate;
#pragma mark -  MBProgressHUD 方法
//显示菊花
+ (void)showHud;
//显示文字不消失
+ (void)showText:(NSString *)text;
//显示文字 自定义消失时间
+ (void)showText:(NSString *)text delay:(NSTimeInterval)dalay;
//显示文字+菊花
+ (void)showTextHud:(NSString *)text;
//成功 自定义消失时间
+ (void)showOKHud:(NSString *)text delay:(NSTimeInterval)dalay;
//失败 自定义消失时间
+ (void)showNOHud:(NSString *)text delay:(NSTimeInterval)dalay;
//自定义图片和文字
+ (void)showAnimated:(NSString *)imageName Text:(NSString *)text delay:(NSTimeInterval)dalay;
//移除
+ (void)hideHud;

/**
 计算结算时间
 
 @param date 开始时间
 */
+ (void)calculateEndTime:(NSDate*)date;

/**
 初始化结束时间
 */
+ (void)deleteEndTime;
//获取结束时间
+ (NSDate*)getEndTime;
+ (NSInteger)getSecond;

//计算时间差
+ (NSString *) compareCurrentTime:(NSString *)str;
/**NSDictionary变为json字符串**/
+ (NSData *)toJSONData:(id)theData;

/**NSArray变为json字符串**/
+ (id)toArrayOrNSDictionary:(NSData *)jsonData;
//计算文字高度
+ (CGSize)sizeForString:(NSString *)string withSize:(CGSize)fitsize withFontSize:(NSInteger)fontSize;
//检查是否包含域名
+ (NSString *)chackUrl:(NSString *)string;
+ (NSInteger)dateTimeDifferenceWithStartTime:(NSString *)startTime endTime:(NSString *)endTime;
//解析error
+ (void)jiexierrrorwitherror:(NSError *)error;
//补全html
+ (NSString *)htmlAddHeader:(NSString *)htmlStr;
// 格式化html
+ (NSString *)formatHtml:(NSString *)html;

// 时间戳 -> NSDate
+ (NSString *)shijianchuoChangeToTimeStr:(int)time Format:(NSString*)format;
//计算距离
+ (double)distanceBetweenOrderBy:(double) lat1 :(double) lat2 :(double) lng1 :(double) lng2;

+ (NSString *)getCFBundleDisplayName;

+ (void)call:(NSString *)mobile;

+ (void)showVipView;

+ (NSString *)getRWStatusFormat:(NSInteger)status;
///设置状态
+ (NSString *)getMTStatusFormat:(NSInteger)status;

#pragma mark -- 轻量级初始化
+ (void)setUserDefaults:(id)value key:(NSString*)key;
+ (id)getUserDefaults:(NSString*)key;
+ (void)removeOneDefaults:(NSString *)key;

+ (NSString *)getTOOoderStatusFormat:(NSInteger)status type:(NSInteger)type isDetail:(BOOL)isDetail;
#pragma mark -- 调整label的前后2部分颜色，大小不同
+ (NSMutableAttributedString *)attributString:(NSString *)string twoStr:(NSString *)twoString color:(UIColor *) oneColor oneHeight:(CGFloat) height  andTColor:(UIColor *) twoColor twoHeight:(CGFloat) twoHeight;
#pragma mark -- 价格颜色变化：¥4699.00
+ (NSMutableAttributedString *)attributedString:(NSString *)string color:(UIColor *) oneColor oneHeight:(CGFloat) height  andTColor:(UIColor *) twoColor twoHeight:(CGFloat) twoHeight andThreeTColor:(UIColor *) threeColor threeHeight:(CGFloat) threeHeight;

@end

NS_ASSUME_NONNULL_END
