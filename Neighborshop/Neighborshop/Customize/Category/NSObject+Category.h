//
//  NSObject+Category.h
//  daycareParent
//
//  Created by Git on 2017/11/27.
//  Copyright © 2017年 XueHui. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface NSObject (Category)
-(id)safeString;
-(id)safeStringZero;
-(NSDictionary *)safeDictionary;
-(NSArray *)safeArray;
/**
 设置头像
 */
-(void)setHeaderWithUrl:(NSString *)headerUrl;
/**
 设置其它图片
 */
-(void)setImageWithUrl:(NSString *)imageUrl;
/**
设置性别图片
*/
-(void)setSexImgView:(NSInteger)sex;
#pragma mark 计算文本的宽度
- (CGFloat)getWidthWithTitle:(NSString *)title fontOfSize:(CGFloat)fontOfSize;
#pragma mark 计算文本的高度
- (CGFloat)getHeightWithTitle:(NSString *)title width:(CGFloat)width fontOfSize:(CGFloat)fontOfSize;
/**
 getRows 获得行数
 @param width 宽度
 @param title 内容
 @param fontOfSize 字号
 @param spacing 字体间隔
 @return Rows 行数
 */
-(CGFloat)getRowsWithTitle:(NSString *)title width:(CGFloat)width fontOfSize:(CGFloat)fontOfSize spacing:(CGFloat)spacing;
#pragma mark - 设置小数点前后字体大小
-(void)setAttributesPointFrontFont:(UIFont *)frontFont behindFont:(UIFont *)behindFont text:(NSString *)text;
#pragma mark- 设置字体大小和颜色
-(void)setAttributesFrontTextColor:(UIColor *)frontTextColor frontFontOfSize:(UIFont *)frontFont fromeIndex:(NSInteger)fromeIndex toIndex:(NSInteger)toIndex behindTextColor:(UIColor *)behindTextColor behindFontOfSize:(UIFont *)behindFont text:(NSString *)text;
#pragma mark- 设置字体大小
-(void)setAttributesFrontFontOfSize:(UIFont *)frontFont fromeIndex:(NSInteger)fromeIndex toIndex:(NSInteger)toIndex behindFontOfSize:(UIFont *)behindFont text:(NSString *)text;
#pragma mark- 设置字体颜色
-(void)setAttributesFrontTextColor:(UIColor *)frontTextColor fromeIndex:(NSInteger)fromeIndex toIndex:(NSInteger)toIndex behindTextColor:(UIColor *)behindTextColor text:(NSString *)text;
#pragma mark- 设置￥和字体大小
-(void)setAttributedStringFrontOfSize:(CGFloat)fontOfSize  behindFontOfSize:(CGFloat)behindFont text:(NSString *)text;
/**
  设置行间距和字间距
 @param fontOfSize 字号
 @param lineSpacing 行间距
 @param attributeNameFont 字间距
 @param text text
 @return 
 */
#pragma mark--设置行间距和字间距
-(void)setAttributedStringSpaceWithFontOfSize:(CGFloat)fontOfSize withlineSpacing:(CGFloat)lineSpacing withAttributeNameFont:(CGFloat)attributeNameFont text:(NSString *)text;
#pragma mark--计算高度(带有行间距的情况)
-(CGFloat)getAttributedStringSpaceLabelHeight:(NSString*)str withFontOfSize:(CGFloat)fontOfSize withWidth:(CGFloat)width withlineSpacing:(CGFloat)lineSpacing withAttributeNameFont:(CGFloat)attributeNameFont;

#pragma mark- 判断是否为整数
-(BOOL)isPureInt:(NSString *)string;
#pragma mark- 判断是否为纯数字
- (BOOL)validateNumber:(NSString*)number;
@end
