//
//  HLCircleCodeView.h
//  VCloud
//
//  Created by null on 2019/12/17.
//  Copyright © 2019 锤子科技. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HLCircleCodeView : UIView

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, assign) BOOL isClear;
@property (nonatomic, assign) BOOL isSurePwd;// 确认支付密码 不需要清除密码

/** 输入完成 code */
@property (copy, nonatomic) void(^hl_circleCode)(NSString *circleCode);
/** 输入过程 next 置灰 */
@property (copy, nonatomic) void(^hl_circleRun)(void);

- (instancetype)initWithCount:(NSInteger)count margin:(CGFloat)margin;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;

@end

NS_ASSUME_NONNULL_END
