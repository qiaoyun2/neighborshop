//
//  HLCircleCodeView.m
//  VCloud
//
//  Created by null on 2019/12/17.
//  Copyright © 2019 锤子科技. All rights reserved.
//

/** View 圆角和加边框 */
#define ViewBorderRadius(View, Radius, Width, Color)\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

#import "HLCircleCodeView.h"

@interface HLCircleCodeView ()

@property (nonatomic, assign) NSInteger itemCount;

@property (nonatomic, assign) CGFloat itemMargin;

@property (nonatomic, weak) UIControl *maskView;

@property (nonatomic, strong) NSMutableArray<UILabel *> *labels;

@end

@implementation HLCircleCodeView

- (instancetype)initWithCount:(NSInteger)count margin:(CGFloat)margin {
    if (self = [super init]) {
        
        self.itemCount = count;
        self.itemMargin = margin;
        
        [self configTextField];
    }
    return self;
}

- (void)configTextField {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.labels = @[].mutableCopy;
    
    self.textField = [[UITextField alloc] init];
    self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.textField.keyboardType = UIKeyboardTypeNumberPad;
    [self.textField addTarget:self action:@selector(tfEditingChanged:) forControlEvents:(UIControlEventEditingChanged)];
    
    [self addSubview:self.textField];
    
    UIButton *maskView = [UIButton new];
    maskView.backgroundColor = [UIColor whiteColor];
    maskView.layer.cornerRadius = 3;
    maskView.layer.masksToBounds = YES;
    [maskView addTarget:self action:@selector(clickMaskView) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:maskView];
    self.maskView = maskView;
    
    [self.labels removeAllObjects];
    for (NSInteger i = 0; i < self.itemCount; i++) {
        
        UILabel *label = [UILabel new];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = UIColorFromRGB(0x000000);
        label.font = [UIFont systemFontOfSize:70];
//        label.layer.borderColor = BBHex(0xCCCCCC).CGColor;
//        label.layer.borderWidth = 0.3;
        label.tag = 7000 + i;
        ViewBorderRadius(label, 0, 0.5, UIColor.grayColor);
        label.backgroundColor = UIColor.clearColor;
        [self addSubview:label];
        [self.labels addObject:label];
    }
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    if (self.labels.count != self.itemCount) return;
    
    CGFloat temp = self.bounds.size.width - (self.itemMargin * (self.itemCount - 1));
    CGFloat w = temp / self.itemCount;
    CGFloat x = 0;
    
    for (NSInteger i = 0; i < self.labels.count; i++) {
        x = i * (w + self.itemMargin);
        
        UILabel *label = self.labels[i];
        label.frame = CGRectMake(x, 0, w, self.bounds.size.height);
    }
    
    self.textField.frame = self.bounds;
    self.maskView.frame = self.bounds;
}

#pragma mark - 编辑改变
- (void)tfEditingChanged:(UITextField *)textField {
    
    if (textField.text.length > self.itemCount) {
        textField.text = [textField.text substringWithRange:NSMakeRange(0, self.itemCount)];
    }
    
    for (int i = 0; i < self.itemCount; i++) {
        
        UILabel *label = [self.labels objectAtIndex:i];
        
        if (i < textField.text.length) {
//            label.text = [textField.text substringWithRange:NSMakeRange(i, 1)];
            label.text = @"·";
        } else {
            label.text = nil;
        }
    }
    
    /** 输入完毕后，自动隐藏键盘 */
    if (textField.text.length >= self.itemCount) {
        if (_hl_circleCode) {
            _hl_circleCode(self.textField.text);
        }
        [textField resignFirstResponder];
        
        if (!_isSurePwd) {
            for (NSInteger i = 0; i < self.labels.count; i++) {
                UILabel *label = self.labels[i];
                label.text = @"";
            }
            self.textField.text = @"";
        }
        
    } else {
        if (_hl_circleRun) {
            _hl_circleRun();
        }
    }
}

- (void)setIsClear:(BOOL)isClear {
    if (isClear) {
        for (NSInteger i = 0; i < self.labels.count; i++) {
            UILabel *label = self.labels[i];
            label.text = @"";
        }
        self.textField.text = @"";
        [self.textField becomeFirstResponder];
    }
}

- (void)clickMaskView {
    [self.textField becomeFirstResponder];
}

- (BOOL)endEditing:(BOOL)force {
    [self.textField endEditing:force];
    return [super endEditing:force];
}


@end
