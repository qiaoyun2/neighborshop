//
//  BaseViewController.h
//  ZZR
//
//  Created by null on 2018/12/13.
//  Copyright © 2018 null. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BlankView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) BOOL isDark; //状态栏颜色
// 无数据view
@property (nonatomic,strong)BlankView *noDataView;
// 通用 适配好的tableView
@property (nonatomic, strong)UITableView *plainTableView;

@property (nonatomic, assign) NSInteger pageNum; // 分页加载  页数

- (void)backItemClicked;

/*设置导航栏右侧的按钮 和点击事件*/
- (void)setNavigationRightBarButtonWithTitle:(NSString *)title color:(UIColor *)color;
- (void)setNavigationRightBarButtonWithTitle:(NSString *)title;
- (void)setNavigationRightBarButtonWithImage:(NSString *)image;
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender;

- (void)changeNavBarBottomLineColor:(UIColor*)coler;

- (void)showNavBarBottomLine;

- (void)hideNavBarBottomLine;

- (void)addBlankOnView:(UIView*)view;

- (void)addBlankOnView:(UIView*)view withY:(CGFloat)y;

- (NSString*)getYMD;


@end

NS_ASSUME_NONNULL_END
