//
//  BaseModel.m
//  地铁
//
//  Created by 李 on 16/12/5.
//  Copyright © 2016年 null. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

- (instancetype)initWithDictionary:(NSDictionary *)dic{
    
    self = [super init];
    
    if (self) {
        if ([dic isKindOfClass:[NSDictionary class]]) {
            [self setValuesForKeysWithDictionary:dic];
        }
    }
    return self;
}

- (void)setValue:(id)value forKey:(NSString *)key{
    if ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSDictionary class]]) {
        [super setValue:value forKey:key];
    }else if ([value isKindOfClass:[NSNumber class]]) {
        [super setValue:[self removeSuffix:value] forKey:key];
    }else{
        [super setValue:[NSString stringWithFormat:@"%@",value] forKey:key];
    }
}

- (NSString *)removeSuffix:(NSString *)numberStr{
    
    numberStr = [NSString stringWithFormat:@"%.2lf",[numberStr doubleValue]];
    if (numberStr.length > 1) {
        if ([numberStr componentsSeparatedByString:@"."].count == 2) {
            NSString *last = [numberStr componentsSeparatedByString:@"."].lastObject;
            if ([last isEqualToString:@"00"]) {
                numberStr = [numberStr substringToIndex:numberStr.length - (last.length + 1)];
                return numberStr;
            }else{
                if ([[last substringFromIndex:last.length -1] isEqualToString:@"0"]) {
                    numberStr = [numberStr substringToIndex:numberStr.length - 1];
                    return numberStr;
                }
            }
        }
        return numberStr;
    }else{
        return nil;
    }
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}
@end
