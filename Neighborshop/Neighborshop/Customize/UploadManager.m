//
//  UploadManager.m
//  ShengYuanUser
//
//  Created by null on 2021/1/16.
//

#import "UploadManager.h"

static UploadManager *manager = nil;

@interface UploadManager ()

@property (nonatomic, copy) void(^callBack)(NSString *ids,NSString *imageUrl);
@property (nonatomic, copy) void(^Block)(NSString *imageUrl);
@property (nonatomic, strong) NSMutableArray *imgArray;
@property (nonatomic, strong) NSMutableArray *urlArray;

@end

@implementation UploadManager

+(instancetype)shareManage
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

+ (void)uploadImageArray:(NSArray *)imageArray block:(void (^)(NSString *ids, NSString *imageUrl))block {
    UploadManager *helper = [UploadManager shareManage];
    helper.callBack  = block;
    [helper uploadImageArray:imageArray block:block];
}

- (void)uploadImageArray:(NSArray *)imageArray block:(void (^)(NSString *ids, NSString *imageUrl))block {
    [self.imgArray removeAllObjects];
    [self.urlArray removeAllObjects];
    for (id object in imageArray) {
        if ([object isKindOfClass:[UIImage class]]) {
            [self.imgArray addObject:object];
        } else {
            [self.urlArray addObject:object];
        }
    }
    if (self.imgArray.count) {
        [NetworkingTool uploadWithUrl:kUploadFileURL params:nil fileData:self.imgArray name:@"files" fileName:@"files.jpg" mimeType:@"image/png" progress:^(NSProgress *progress) {
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"] integerValue] == 1) {
                
                NSString *dataStr = responseObject[@"data"];
                if (self.callBack) {
                    self.callBack(dataStr, dataStr);
                }
//                NSMutableArray *urlList = [NSMutableArray array];
//                NSArray *dataList = responseObject[@"data"];
//                for (NSDictionary *info in dataList) {
//                    [urlList addObject:info[@"path"]];
//                    [self.urlArray addObject:info[@"id"]];
//                }
//
//                if (self.callBack) {
//                    self.callBack([self.urlArray componentsJoinedByString:@","], [urlList componentsJoinedByString:@","]);
//                }
            } else {
                [LJTools showNOHud:responseObject[@"msg"] delay:2];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:2];
        } IsNeedHub:YES];
    } else {
        block([self.urlArray componentsJoinedByString:@","], @"");
    }
}

+(void)uploadWithImageArray:(NSArray *)imageArray block:(void(^)(NSString *imageUrl))block
{
//    if (imageArray.count == 1 && block) {
//        block([NSString stringWithFormat:@"%d", arc4random() % 2 + 1]);
//        return;
//    } else if (imageArray.count > 1) {
//        block(@"1,2");
//        return;
//    }
//    if (block) {
//        block([UploadManager getTempImageUrls:imageArray]);
//    }
//    return;
    UploadManager *helper = [UploadManager shareManage];
    helper.Block = block;
    [helper uploadWithImageArray:imageArray block:block];
}

+ (NSString *)getTempImageUrls:(NSArray *)imageArray {
    NSArray *tempImageList = @[@"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/b5b7ce1462b0b503a5f09d793b2ff848.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/1dd81f0f6892d19adcbb1c3b4c7aadad.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/8c44f7b87dcc892495d4a392a4ecf99b.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/d7c18f0a4646a3d22377244b9a96337c.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/d01210b50c7a0709ff114f8367435783.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/9ea38a1b9de05be8165e81f0d92aa16b.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/498a3cefa569897e4342e71a24ea02e4.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/19a7c2635a32ffe9f5826cef7f9055ca.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/991079409ea119b67a88dfa40eb17bb2.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/d9d3191be0617e418b7a92d1bcefdeda.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/e6f12349b47a4cb8c688020fdf47807f.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/6b2f1a10638316a3a4c2d1c25db8bc82.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/8db75afdd8db225361164b438f52df06.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/1afc693fe59a823a9cfe186a2ac0bf53.jpg",
                               @"http://file.gxbsyd.com/uploads/images/user/9eb5d0217db01037b525e6b745eb8350/4fd1928cab53570f6fb2aa91e7a7716f.jpg"];
    NSMutableArray *imageList = [NSMutableArray array];
    for (UIImage *image in imageArray) {
        int i = arc4random() % 15;
        [imageList addObject:tempImageList[i]];
    }
    return [imageList componentsJoinedByString:@","];
}

-(void)uploadWithImageArray:(NSArray *)imageArray block:(void(^)(NSString *imageUrl))block
{
    [self.imgArray removeAllObjects];
    [self.urlArray removeAllObjects];
    for (id object in imageArray) {
        if ([object isKindOfClass:[UIImage class]]) {
            [self.imgArray addObject:object];
        }else
        {
            [self.urlArray addObject:object];
        }
    }
    if (self.imgArray.count) {
        [NetworkingTool uploadWithUrl:kUploadFileURL params:nil fileData:self.imgArray name:@"file" fileName:@"files.jpg" mimeType:@"image/png" progress:^(NSProgress *progress) {
            
        } success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"] integerValue] == 1) {
                
                NSString *imageUrl = [self.urlArray componentsJoinedByString:@","];
                NSString *imagePath = responseObject[@"data"][@"thumb"];
                NSString *urls;
                urls = imagePath;
                if (self.urlArray.count) {
                    urls = [NSString stringWithFormat:@"%@,%@",imageUrl,imagePath];
                }
                if (self.Block) {
                    self.Block(urls);
                }
            } else {
                [LJTools showNOHud:responseObject[@"msg"] delay:2];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:2];
        } IsNeedHub:YES];
    }else
    {
        block([self.urlArray componentsJoinedByString:@","]);
    }
    
}
+(void)uploadWithVideoData:(NSData *)VideoData block:(void(^)(NSString *videoUrl))block
{
    UploadManager *helper = [UploadManager shareManage];
    helper.Block = block;
    [helper uploadWithVideoData:VideoData block:block];
}
-(void)uploadWithVideoData:(NSData *)VideoData block:(void(^)(NSString *videoUrl))block
{
    [NetworkingTool uploadFileDataWithUrl:kUploadFileURL params:nil fileData:VideoData name:@"files" fileName:@"files.mp4" mimeType:@"file/mp4" progress:^(NSProgress *progress) {} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue] == 1) {
            NSString *imagePath = responseObject[@"data"];
            if (self.Block) {
                self.Block(imagePath);
            }
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:2];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:2];
    } IsNeedHub:YES];
}
-(NSMutableArray *)imgArray
{
    if (_imgArray==nil) {
        _imgArray=[NSMutableArray array];
    }
    return _imgArray;
}
-(NSMutableArray *)urlArray
{
    if (_urlArray==nil) {
        _urlArray=[NSMutableArray array];
    }
    return _urlArray;
}
@end
