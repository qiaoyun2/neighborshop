//
//  MyMessageViewController.h
//  PPKMaster
//
//  Created by qiaoyun on 2022/10/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyMessageViewController : BaseViewController
/// 1 @"系统消息",2 @"订单消息"
@property (nonatomic, assign) NSInteger type;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

NS_ASSUME_NONNULL_END
