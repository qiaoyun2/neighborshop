//
//  MyMessageDetailViewController.m
//  PPKMaster
//
//  Created by qiaoyun on 2022/10/12.
//

#import "MyMessageDetailViewController.h"
#import "MessageModel.h"

@interface MyMessageDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *detailImageView;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) MessageModel *model;


@end

@implementation MyMessageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"消息详情";
    [self requestForFinish];
    [self setNavigationRightBarButtonWithTitle:@"删除" color:UIColorFromRGB(0x333333)];
    
}
#pragma mark - 网络请求
- (void)requestForFinish {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
  
    [params setValue:self.messageId forKey:@"messageId"];
    NSString *url = kMessageGetByMessageIdURL;
   
    [NetworkingTool getWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            NSDictionary *dict = responseObject[@"data"];
            MessageModel *model = [[MessageModel alloc] initWithDictionary:dict];
            self.model = model;
            self.timeLabel.text = model.publishTime;
            self.detailLabel.text = model.content;
            self.titleLabel.text = model.title;
            
            if([model.picture safeString] && [model.picture length] > 0 ){
                self.detailImageView.hidden = NO;
                [self.detailImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.picture)] placeholderImage:DefaultImgWidth];
            }else{
                self.detailImageView.hidden = YES;
            }
        }else{
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 右边按钮
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender{
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除该消息吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
        [weakSelf requestForDeleteOrder:self.model];
    } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
}

#pragma mark - 删除
- (void)requestForDeleteOrder:(MessageModel *)model
{
    NSString *url = kMessageDeleteByIdURL;
    WeakSelf
    [NetworkingTool postWithUrl:url params:@{@"messageId":model.messageId} success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {

    } IsNeedHub:YES];
}


@end
