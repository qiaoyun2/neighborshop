//
//  MyMessageDetailViewController.h
//  PPKMaster
//
//  Created by qiaoyun on 2022/10/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyMessageDetailViewController : BaseViewController

@property (nonatomic,strong) NSString *titleStr;
@property (nonatomic,strong) NSString *messageId;

@end

NS_ASSUME_NONNULL_END
