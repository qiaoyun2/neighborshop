//
//  MessageModel.h
//  PPKMaster
//
//  Created by qiaoyun on 2022/10/12.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessageModel : BaseModel
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *messageId;
@property (nonatomic, strong) NSString *picture; //
@property (nonatomic, strong) NSString *publishTime; //
@property (nonatomic, strong) NSString *readFlag; //
@property (nonatomic, strong) NSString *title; //
@end

NS_ASSUME_NONNULL_END
