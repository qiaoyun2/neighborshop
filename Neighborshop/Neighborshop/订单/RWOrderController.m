//
//  RWOrderController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/16.
//

#import "RWOrderController.h"
#import "RWOrderListViewController.h"
#import "YNPageViewController.h"
#import "UIView+YNPageExtend.h"
#import "ReLayoutButton.h"
#import "RWOrderHeaderView.h"
#import "HomeTimeSelectView.h"

@interface RWOrderController ()<YNPageViewControllerDataSource, YNPageViewControllerDelegate>

@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, strong) RWOrderHeaderView *headerView;
@property (nonatomic , assign) CGFloat height;
@property (nonatomic, strong) YNPageViewController *vc;


@end

@implementation RWOrderController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单";
    self.titles = @[@"全部",@"待付款",@"待接单",@"待送达/自提",@"已完成"];
    [self setupPageVC];
}

- (void)setupPageVC {
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleSuspensionTopPause;
    /// 控制tabbar 和 nav
    configration.showTabbar = YES;
    configration.showNavigation = YES;
    //scrollMenu = NO,aligmentModeCenter = NO 会变成平分
    configration.scrollMenu = YES;
    configration.aligmentModeCenter = NO;
    configration.showBottomLine = NO;
    configration.showScrollLine = NO;
//    configration.lineWidthEqualFontWidth = YES;
    configration.itemMargin = 10;
    configration.menuHeight = 50;
    configration.normalItemColor = RGBA(51, 51, 51, 1);
    configration.selectedItemColor = RGBA(51, 51, 51, 1);
    configration.itemFont = [UIFont systemFontOfSize:16];
    configration.selectedItemFont = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    configration.cutOutHeight = 0;
    
    NSMutableArray *buttonArrayM = [NSMutableArray array];
    for (int i = 0; i<self.titles.count; i++) {
        ReLayoutButton *button = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        button.layoutType = 3;
        button.margin = 3;
        UIImage *selectImage = [UIImage imageNamed:@"矩形 1884"];
        [button setImage:[UIImage imageNamed:@"矩形 1884-1"] forState:UIControlStateNormal];
        [button setImage:selectImage forState:UIControlStateSelected];
        [buttonArrayM addObject:button];
    }
    configration.buttonArray = buttonArrayM;
    
    YNPageViewController *vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs titles:self.titles  config:configration];
    vc.dataSource = self;
    vc.delegate = self;
    /// 指定默认选择index 页面
    vc.pageIndex = 0;
    
    RWOrderHeaderView *header = [[RWOrderHeaderView alloc] initWithFrame:CGRectMake(0,0, SCREEN_WIDTH,  0)];
    self.headerView = header;
    self.headerView.hidden = YES;
    WeakSelf
    [self.headerView.errorBtn jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        weakSelf.headerView .frame = CGRectMake(0,0, SCREEN_WIDTH,  0);
        weakSelf.headerView.hidden = YES;
        [weakSelf.vc reloadSuspendHeaderViewFrame];
        RWOrderListViewController *orderVC = weakSelf.getArrayVCs[weakSelf.vc.pageIndex];
        orderVC.start_data = @"";
        orderVC.end_data = @"";
        [orderVC.tableView.mj_header beginRefreshing];
    }];
    
    vc.headerView = header;
    self.vc = vc;
    /// 作为自控制器加入到当前控制器
    [vc addSelfToParentViewController:self];
}

- (NSArray *)getArrayVCs {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSInteger i = 0; i< self.titles.count; i++) {
        RWOrderListViewController *vc = [[RWOrderListViewController alloc] init];
        vc.type = i;
        [tempArray addObject:vc];
    }
    return [tempArray copy];
}

#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    UIViewController *vc = pageViewController.controllersM[index];
    if ([User getAuthMasterType].intValue == 1) {
        return [(RWOrderListViewController *)vc tableView];
    }
    return [(RWOrderListViewController *)vc tableView];
}

#pragma mark - YNPageViewControllerDelegate
- (void)pageViewController:(YNPageViewController *)pageViewController
            contentOffsetY:(CGFloat)contentOffset
                  progress:(CGFloat)progress {
}

- (void)pageViewController:(YNPageViewController *)pageViewController
                 didScroll:(UIScrollView *)scrollView
                  progress:(CGFloat)progress
                 formIndex:(NSInteger)fromIndex
                   toIndex:(NSInteger)toIndex {
    
}

#pragma mark - 右边按钮的点击
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender {
    self.headerView.hidden = NO;
    self.headerView.frame = CGRectMake(0,0, SCREEN_WIDTH,  40);
    self.vc.headerView = self.headerView ;
    [self.vc reloadSuspendHeaderViewFrame];
    WeakSelf
    [HomeTimeSelectView show:^(NSString * _Nonnull startDate, NSString * _Nonnull endDate) {
//        weakSelf.start_data = startDate;
//        weakSelf.end_data = endDate;
        weakSelf.headerView.timeLabel.text = [NSString stringWithFormat:@"筛选区域：%@ 至%@",startDate,endDate];
        RWOrderListViewController *orderVC = weakSelf.getArrayVCs[weakSelf.vc.pageIndex];
        orderVC.start_data = startDate;
        orderVC.end_data = endDate;
        [orderVC.tableView.mj_header beginRefreshing];
    }];
    
    
}

@end
