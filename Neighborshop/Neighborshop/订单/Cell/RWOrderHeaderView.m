//
//  RWOrderHeaderView.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/17.
//

#import "RWOrderHeaderView.h"

@implementation RWOrderHeaderView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[NSBundle mainBundle] loadNibNamed:@"RWOrderHeaderView" owner:self options:nil][0];
        self.frame = frame;
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
