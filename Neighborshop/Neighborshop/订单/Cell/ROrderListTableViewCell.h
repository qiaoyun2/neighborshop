//
//  ROrderListTableViewCell.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ROrderListTableViewCell : UITableViewCell
-(void)setItemObject:(id)object;
@property (nonatomic, copy) void(^btnClickBlock)(NSInteger type);
@end

NS_ASSUME_NONNULL_END
