//
//  ROrderListTableViewCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/17.
//

#import "ROrderListTableViewCell.h"
#import "TOShopListGoodsItem.h"
#import "HomeOrderListModel.h"
@interface ROrderListTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIStackView *bottomStackView;
@property (nonatomic, strong) HomeOrderListModel *model;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeight;

@property (weak, nonatomic) IBOutlet UILabel *shippingTypeLabel;  // 商家配送 自提
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel;

#pragma mark -- <倒计时>
/** timer */
@property(nonatomic , strong)dispatch_source_t timer;

@end
@implementation ROrderListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    [self.bottomStackView.arrangedSubviews enumerateObjectsUsingBlock:^(__kindof UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([button.titleLabel.text isEqualToString:@"取消订单"]) {
            button.hidden = NO;
        }else {
            button.hidden = YES;
        }
    }];
 
    [self.collectionView registerNib:[UINib nibWithNibName:@"TOShopListGoodsItem" bundle:nil] forCellWithReuseIdentifier:@"TOShopListGoodsItem"];
    
}
-(void)setItemObject:(HomeOrderListModel *)object{
    self.model = object;
    self.orderNumLabel.attributedText = [LJTools attributString:@"订单编号：" twoStr:object.orderNo color:UIColorFromRGB(0x999999) oneHeight:14 andTColor:UIColorFromRGB(0x333333) twoHeight:14];
    self.priceLabel.attributedText = [LJTools attributedString:[NSString stringWithFormat:@"%.2lf",[object.totalFoodMoney doubleValue]] color:UIColorFromRGB(0x333333) oneHeight:10 andTColor:UIColorFromRGB(0x333333) twoHeight:16 andThreeTColor:UIColorFromRGB(0x333333) threeHeight:11];
   
    for (UIButton *button in self.bottomStackView.arrangedSubviews) {
        button.hidden = YES;
    }
    
    self.numberLabel.text = [NSString stringWithFormat:@"共%@件",object.totalFoodNum];

    self.shippingTypeLabel.text = object.type.intValue == 1?@"商家配送":@"用户自提";
    self.shippingTypeLabel.hidden = NO;
    self.countDownLabel.hidden = YES;
    
    //外卖订单状态 0-已取消;1-待付款;2-待接单;3-已接单;4-配送中/待自提;5-已完成待评;6-已完成;7-已关闭
    if(object.refundStatus.intValue > 0){
        if(object.status.intValue < 5){
            [self getStatus:object.refundStatus];
        }else{
            [self updateStatusUI:object];
        }
    }else{
        [self updateStatusUI:object];
    }
    self.collectionViewHeight.constant = object.itemList.count == 0 ?0:95;
    self.collectionView.hidden = object.itemList.count == 0 ?YES:NO;
    
}
-(void)updateStatusUI:(HomeOrderListModel *)model{
    self.statusLabel.text = [LJTools getTOOoderStatusFormat:model.status.intValue type:model.type.intValue isDetail:NO];
    
    switch (model.status.intValue) {
        case 0://已取消
        {
            UIButton *btn = self.bottomStackView.arrangedSubviews[7];
            [btn setHidden:NO];
        }
            break;
        case 1://待付款
        {
            if( model.payTime.intValue  > 0){
                self.shippingTypeLabel.text = @"剩余支付时间";
                self.shippingTypeLabel.hidden = self.countDownLabel.hidden = NO;
                [self startTimer:model.payTime.intValue];
                
            }else{
                self.shippingTypeLabel.hidden = NO;
                self.countDownLabel.hidden = YES;
                self.shippingTypeLabel.text = model.type.intValue == 1?@"商家配送":@"自提";
            }
            UIButton *btn = self.bottomStackView.arrangedSubviews[0];
            [btn setHidden:NO];
            UIButton *btn1 = self.bottomStackView.arrangedSubviews[1 ];
            [btn1 setHidden:NO];
        }
            break;
        case 2://待接单
        {
            UIButton *btn = self.bottomStackView.arrangedSubviews[0];
            [btn setHidden:NO];
            
            UIButton *btn1 = self.bottomStackView.arrangedSubviews[1 ];
            [btn1 setHidden:NO];
            UIButton *btn2 = self.bottomStackView.arrangedSubviews[2 ];
            [btn2 setHidden:NO];
        }
            break;
        case 3://已接单
        {
            UIButton *btn3 = self.bottomStackView.arrangedSubviews[3];
            [btn3 setHidden:NO];
        }
            break;
        case 4://配送中/待自提
        {
            if(model.type.intValue == 2){
                UIButton *btn = self.bottomStackView.arrangedSubviews[1];
                [btn setHidden:NO];
                UIButton *btn1 = self.bottomStackView.arrangedSubviews[4];
                [btn1 setTitle:@"提货验证" forState:UIControlStateNormal];
                [btn1 setHidden:NO];
            }else{
                UIButton *btn1 = self.bottomStackView.arrangedSubviews[1];
                [btn1 setHidden:NO];
   
            }
        }
            break;
        case 5://已完成待评价
        {
            UIButton *btn = self.bottomStackView.arrangedSubviews[1];
            [btn setHidden:NO];
            UIButton *btn1 = self.bottomStackView.arrangedSubviews[7];
            [btn1 setHidden:NO];

        }
            break;
        case 6://已完成
        {
            UIButton *btn = self.bottomStackView.arrangedSubviews[1];
            [btn setHidden:NO];
            UIButton *btn1 = self.bottomStackView.arrangedSubviews[7];
            [btn1 setHidden:NO];
            UIButton *btn2 = self.bottomStackView.arrangedSubviews[8];
            [btn2 setHidden:NO];
        }
            break;
        case 7://已关闭
        {
            UIButton *btn = self.bottomStackView.arrangedSubviews[7];
            [btn setHidden:NO];
        }
            break;
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.model.itemList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TOShopListGoodsItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TOShopListGoodsItem" forIndexPath:indexPath];
    TOOrderGoodsModel *list = self.model.itemList[indexPath.item];
    [cell.thumbImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(list.picture)] placeholderImage:DefaultImgWidth];
    cell.priceLabel.text = list.title;
    cell.priceLabel.textColor = UIColorFromRGB(0x333333);
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(70, 94);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
///取消订单0 查看详情1 接单2 备货完成3 确认送达4 拒绝退款5 确认退款6 删除7 查看评价8
- (IBAction)orderBtnClick:(UIButton *)sender {
    if(self.btnClickBlock){
        self.btnClickBlock(sender.tag);
    }
}
#pragma mark - 退款状态
-(void) getStatus:(NSString *)status{
  //  NSString *statusS = @"";
    ////退款状态:0-未申请;1-申请中;2-退款失败;3-退款成功
    switch (status.intValue) {
        case 1:
        {
            self.statusLabel.text  = @"售后处理中";
            UIButton *btn = self.bottomStackView.arrangedSubviews[1];
            [btn setHidden:NO];
            UIButton *btn1 = self.bottomStackView.arrangedSubviews[5];
            [btn1 setHidden:NO];
            UIButton *btn2 = self.bottomStackView.arrangedSubviews[6];
            [btn2 setHidden:NO];
        }
            break;
        case 2:
        {
            [self updateStatusUI:self.model];
            
        }
            break;
        case 3:
        {
            self.statusLabel.text  = @"售后已完成";
            UIButton *btn = self.bottomStackView.arrangedSubviews[1];
            [btn setHidden:NO];
            UIButton *btn1 = self.bottomStackView.arrangedSubviews[7];
            [btn1 setHidden:NO];
        }
            break;
        default:
            break;
    }
//    return statusS;
}
#pragma mark - 定时器
- (void)startTimer:(int)time
{
    __block int timeout=time;
    //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_async(dispatch_get_main_queue(), ^{
                self.shippingTypeLabel.hidden = self.countDownLabel.hidden = YES;
            });
        }else{
            int seconds = timeout;
            //format of minute
            NSString *str_minute = [NSString stringWithFormat:@"%02d",(seconds%3600)/60];
            //format of second
            NSString *str_second = [NSString stringWithFormat:@"%02d",seconds%60];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.countDownLabel.text = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}
@end
