//
//  RWOrderHeaderView.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RWOrderHeaderView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *errorBtn;

@end

NS_ASSUME_NONNULL_END
