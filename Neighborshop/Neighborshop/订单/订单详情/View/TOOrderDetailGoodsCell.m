//
//  TOOrderDetailGoodsCell.m
//  PPK
//
//  Created by null on 2022/7/28.
//

#import "TOOrderDetailGoodsCell.h"
#import "HomeOrderListModel.h"
@interface TOOrderDetailGoodsCell ()
@property (nonatomic, strong) TOOrderGoodsModel *model;
@end

@implementation TOOrderDetailGoodsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setItemObject:(TOOrderGoodsModel *)object
{
    self.model = object;
    [self.thumbImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(object.picture)] placeholderImage:DefaultImgWidth];
    [self.nameLabel setText:object.title];
    [self.moneyLabel setText:object.sellPrice];
    self.numLabel.text = [NSString stringWithFormat:@"x%@",object.number];
    [self.skuNameLabel setText:[NSString safeString:object.skuName]];

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
