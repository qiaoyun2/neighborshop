//
//  RWOrderDetailViewController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/17.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWOrderDetailViewController : BaseViewController
@property (nonatomic, strong) NSString *orderNo;
@end

NS_ASSUME_NONNULL_END
