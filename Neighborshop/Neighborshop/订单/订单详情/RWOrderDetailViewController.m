//
//  RWOrderDetailViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/17.
//

#import "RWOrderDetailViewController.h"
#import "FoundListImage9TypographyView.h"
#import "FeeIntroduceView.h"
#import "TOOrderDetailGoodsCell.h"
#import "TOOrderDetailListModel.h"
#import "HomeOrderListModel.h"
#import "HomeCookVerifyViewController.h"
#import "OrderCommentViewController.h"
#import "LMMapViewController.h"
#import "RefundViewController.h"

@interface RWOrderDetailViewController ()
/**订单状态*/
@property (weak, nonatomic) IBOutlet UILabel *statusLabel; // 待付款
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;//删除
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
/**subStatusView*/
@property (weak, nonatomic) IBOutlet UIView *subStatusView;
@property (weak, nonatomic) IBOutlet UILabel *subStatusLabel; // 剩余支付时间
@property (weak, nonatomic) IBOutlet UILabel *countDownLabel; // 14:30
@property (weak, nonatomic) IBOutlet UIView *managerView;
@property (weak, nonatomic) IBOutlet UIStackView *managerStackView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonViewHright;
/**退款信息*/
@property (weak, nonatomic) IBOutlet UIView *refundInfoView;
@property (weak, nonatomic) IBOutlet UIView *refundMoneyView; // 退款金额  退款账户 退款时间
@property (weak, nonatomic) IBOutlet UILabel *refundMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundTimeLabel;
@property (weak, nonatomic) IBOutlet UIStackView *refundReasonView; // 服务类型  退款原因 申请时间 退款编号 退款凭证
@property (weak, nonatomic) IBOutlet UILabel *refundTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundReasonLabel;
@property (weak, nonatomic) IBOutlet UILabel *applyTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *refundRemarkLabel;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;
/**配送信息*/
@property (weak, nonatomic) IBOutlet UIView *sendInfoView;
@property (weak, nonatomic) IBOutlet UILabel *sendTimeLabel; // 配送时间
@property (weak, nonatomic) IBOutlet UILabel *sendAddressLabel; // 配送地址
/**自提信息*/
@property (weak, nonatomic) IBOutlet UIView *pickInfoView;
@property (weak, nonatomic) IBOutlet UILabel *pickTimeLabel; // 自提时间
@property (weak, nonatomic) IBOutlet UILabel *pickAddressLabel; // 自提地址
@property (weak, nonatomic) IBOutlet UILabel *pickMobileLabel; // 自提人电话
/**商品信息*/
@property (weak, nonatomic) IBOutlet UIView *goodsInfoView;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel; // 店铺名字
@property (weak, nonatomic) IBOutlet UITableView *tableView; // 商品列表
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *packagingFeeLabel; // 打包费
@property (weak, nonatomic) IBOutlet UILabel *sendFeeLabel; // 配送费
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLabel; // 总金额
/**订单信息*/
@property (weak, nonatomic) IBOutlet UIView *orderInfoView;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel; // 备注
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel; // 订单号
@property (weak, nonatomic) IBOutlet UILabel *createTimeLabel; // 下单时间
@property (weak, nonatomic) IBOutlet UILabel *payTypeLabel;  // 支付方式
@property (weak, nonatomic) IBOutlet UILabel *payMoneyLabel; // 支付金额
/**取消*/
@property (weak, nonatomic) IBOutlet UIView *cancelView;

@property (weak, nonatomic) IBOutlet UIView *selfView;
@property (weak, nonatomic) IBOutlet UIView *addressView;
#pragma mark -- <倒计时>
/** timer */
@property(nonatomic , strong)dispatch_source_t timer;

@property (nonatomic, strong) TOOrderDetailListModel *model;
@end

@implementation RWOrderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单详情";
    self.view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    self.tableViewHeight.constant  = 82*0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addSpecSuccess:) name:VerifyPickupCode object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addSpecSuccess:) name:OrderRefresh object:nil];
    WeakSelf
    [self.addressView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        LMMapViewController *vc = [[LMMapViewController alloc] init];
        vc.coordinate = CLLocationCoordinate2DMake([weakSelf.model.latitude floatValue], [weakSelf.model.longitude floatValue]);
        vc.city = @"";
        vc.address = weakSelf.model.sendAddress;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    [self.selfView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        LMMapViewController *vc = [[LMMapViewController alloc] init];
        vc.coordinate = CLLocationCoordinate2DMake([weakSelf.model.latitude floatValue], [weakSelf.model.longitude floatValue]);
        vc.city = @"";
        vc.address = weakSelf.model.shopAddress;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self loadBasicData];
}
#pragma mark - 通知更新数据
- (void) addSpecSuccess:(NSNotification *)noti{
    [self loadBasicData];
}
#pragma mark - 网络请求
-(void)loadBasicData{
    WeakSelf
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"orderNo"] = self.orderNo;
    [NetworkingTool getWithUrl:kTakeoutOrderQueryByIdURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *dataDic = responseObject[@"data"];
            TOOrderDetailListModel *model = [[TOOrderDetailListModel alloc] initWithDictionary:dataDic];
            weakSelf.model = model;
            weakSelf.tableViewHeight.constant = model.itemList.count * 82;
            [weakSelf updateUI];
            [weakSelf updateRefundUI];
        } else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 更新UI
-(void)updateUI{
    [self.managerStackView.arrangedSubviews enumerateObjectsUsingBlock:^(__kindof UIButton * _Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        button.hidden = YES;
    }];
    self.managerView.hidden = YES;
    
    self.shopNameLabel.text = self.model.shopName;
    self.packagingFeeLabel.text = [NSString stringWithFormat:@"¥%@",self.model.boxCost];
    self.sendFeeLabel.text = [NSString stringWithFormat:@"¥%@",self.model.sendCost];
    self.totalMoneyLabel.text  = [NSString stringWithFormat:@"%.2lf",[self.model.totalMoney doubleValue]];
    self.remarkLabel.text =  self.model.buyerMessage;
    self.orderNoLabel.text = self.model.orderNo;
    self.createTimeLabel.text = self.model.orderTime;
    //1-微信;2-支付宝
    self.payTypeLabel.text = self.model.paymentType.intValue == 1?@"微信支付":@"支付宝支付";
    
    self.payMoneyLabel.text = [NSString stringWithFormat:@"¥%.2lf",[self.model.totalMoney doubleValue]];
    self.pickTimeLabel.text = self.model.sendTime;
    self.pickAddressLabel.text = self.model.shopAddress;
    self.pickMobileLabel.text = self.model.selfDeliveryMobile;
    self.sendTimeLabel.text = self.model.sendTime;
    self.sendAddressLabel.text = self.model.sendAddress;
    self.refundMoneyLabel.text = [NSString stringWithFormat:@"¥%@",self.model.refundReason];
}
#pragma mark - 更新退款UI
-(void) updateRefundUI{
    self.refundInfoView.hidden = self.model.refundStatus.intValue == 0;
    self.refundMoneyLabel.text = [NSString stringWithFormat:@"¥%@",self.model.refundMoney];
    //退款账户类型: 1-退回余额;2-退回付款账户
    self.refundAccountLabel.text = self.model.refundAccountType.intValue == 1?@"退回余额":@"退回付款账户";
    self.refundTimeLabel.text = [NSString stringWithFormat:@"%@",self.model.refundTime];
    self.refundRemarkLabel.text = self.model.refundReason;
    self.applyTimeLabel.text = self.model.applyRefundTime;
    self.refundNoLabel.text = self.model.refundOrderNo;
    if (K_DEFULT(self.model.refundPicture).length > 0) {
        NSArray *imageUrls = [self.model.refundPicture componentsSeparatedByString:@","];
        self.imgsView.imageArray = imageUrls;
        self.imgsView.is_tap = YES;
    }
    self.refundInfoView.hidden = self.cancelView.hidden = self.subStatusView.hidden = YES;
    self.bottomViewHeight.constant = 0;
    //1-商家配送;2-到店自取
    self.sendInfoView.hidden = self.model.type.intValue == 2;
    self.pickInfoView.hidden = self.model.type.intValue == 1;
    
    if(self.model.refundStatus.intValue > 0){
        if(self.model.status.intValue < 5){
            [self getStatus:self.model.refundStatus];
        }else{
            [self updateStatusUI];
        }
    }else{
        [self updateStatusUI];
    }
}
#pragma mark - 更新UI··
-(void)updateStatusUI{
    self.statusLabel.text = [LJTools getTOOoderStatusFormat:self.model.status.intValue type:self.model.type.intValue isDetail:YES];
   // self.subStatusView.hidden = YES;
    //外卖订单状态 0-已取消;1-待付款;2-待接单;3-已接单;4-配送中/待自提;5-已完成待评;6-已完成;7-已关闭
    switch (self.model.status.intValue) {
        case 0://已取消
        {
            self.cancelView.hidden = NO;
            self.bottomViewHeight.constant = 50;
        }
            break;
        case 1://待付款
        {
            if( self.model.payTime.intValue  > 0){
                self.subStatusLabel.text = @"剩余支付时间";
                self.subStatusView.hidden = NO;
                [self startTimer:self.model.payTime.intValue];
            }else{
                self.subStatusView.hidden = self.subStatusLabel.hidden = NO;
                self.countDownLabel.hidden = YES;
            }
            self.managerView.hidden = NO;
            UIButton *btn = self.managerStackView.arrangedSubviews[0];
            [btn setHidden:NO];
            
        }
            break;
        case 2://待接单
        {
            self.managerView.hidden =  NO;
           
            UIButton *btn1 = self.managerStackView.arrangedSubviews[1];
            [btn1 setHidden:NO];
        }
            break;
        case 3://已接单
        {
            self.managerView.hidden = NO;
          
            UIButton *btn2 = self.managerStackView.arrangedSubviews[2];
            [btn2 setHidden:NO];
        }
            break;
        case 4:{//配送中/待自提
           
            if(self.model.type.intValue == 2){
                self.managerView.hidden  = NO;
                UIButton *btn3 = self.managerStackView.arrangedSubviews[3];
                [btn3 setTitle:@"提货验证" forState:UIControlStateNormal];
                [btn3 setHidden:NO];
            }else{
                self.managerView.hidden  = YES;
            }
        }
            break;
        case 5://已完成待评价
        {
            self.cancelView.hidden = NO;
            self.bottomViewHeight.constant = 50;
            
        }
            break;
        case 6://已完成
        {
            self.cancelView.hidden =  self.managerView.hidden =  NO;
            self.bottomViewHeight.constant = 50;
            UIButton *btn2 = self.managerStackView.arrangedSubviews[7];
            [btn2 setHidden:NO];
        }
            break;
        case 7://已关闭
        {
            self.cancelView.hidden = NO;
            self.bottomViewHeight.constant = 50;
        }
            break;
        default:
            break;
    }
}
#pragma mark - 退款状态
-(void) getStatus:(NSString *)status{
    //  NSString *statusS = @"";
    self.refundInfoView.hidden = NO;
    self.refundMoneyView.hidden = YES;
    ////退款状态:0-未申请;1-申请中;2-退款失败;3-退款成功
    switch (status.intValue) {
        case 1:  {
            self.statusLabel.text  = @"申请中";
            self.managerView.hidden =  self.subStatusView.hidden = NO;
            self.countDownLabel.hidden = YES;
            self.subStatusLabel.text = @"等待商家处理";
            UIButton *btn = self.managerStackView.arrangedSubviews[4];
            [btn setHidden:NO];
            UIButton *btn1 = self.managerStackView.arrangedSubviews[5];
            [btn1 setHidden:NO];
        }
            break;
        case 2: {
            [self updateStatusUI];
        }
            break;
        case 3: {
            self.refundMoneyView.hidden = self.cancelView.hidden = NO;
            self.managerView.hidden = self.subStatusView.hidden =self.countDownLabel.hidden = YES;
            self.statusLabel.text =  @"退款成功";
            self.bottomViewHeight.constant = 50;
        }
            break;
        default:
            break;
    }
}
#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.itemList.count;
    //    return 6;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TOOrderDetailGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOOrderDetailGoodsCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TOOrderDetailGoodsCell" owner:nil options:nil] firstObject];
    }
    TOOrderGoodsModel *model = self.model.itemList[indexPath.row];
    [cell setItemObject:model];
    return cell;
}

#pragma mark - XibFunction
#pragma mark - 客服
- (IBAction)contactButtonAction:(id)sender {
    
}
#pragma mark - 电话
- (IBAction)callButtonAction:(id)sender {
    NSString *phone = self.model.type.intValue == 2?self.model.selfDeliveryMobile:self.model.userMobile;
    [LJTools call:phone];
}
#pragma mark - 打包费
- (IBAction)questionButtonAction:(UIButton *)sender {
    
    NSDictionary *dict = @{
        @"configName":sender.tag == 10?@"takeoutUnpackDesc":@"takeoutSendDesc"
    };
    [NetworkingTool getWithUrl:kConfigNameURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSString *privacy = (NSString *)responseObject[@"data"];
            FeeIntroduceView *view = [[[NSBundle mainBundle] loadNibNamed:@"FeeIntroduceView" owner:nil options:nil] firstObject];
            view.introduceLabel.text = privacy;
            view.titleLabel.text = sender.tag == 10?@"打包费说明":@"配送费说明";
            view.frame = [UIScreen mainScreen].bounds;
            [[UIApplication sharedApplication].keyWindow addSubview:view];
            [view show];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - 复制
- (IBAction)copyButtonAction:(UIButton *)sender {
    UIPasteboard * pastboard = [UIPasteboard generalPasteboard];
    pastboard.string = sender.tag == 1?self.model.orderNo:self.model.refundOrderNo;
    if ([pastboard.string length] > 0) {
        [LJTools showOKHud:@"复制成功" delay:1];
    }else{
        [LJTools showNOHud:@"复制失败" delay:1];
    }
}
#pragma mark - 删除
- (IBAction)cancelButtonAction:(UIButton *)sender {
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除该订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
        [weakSelf requestForDeleteOrder];
    } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
}
#pragma mark - 按钮点击事件 ///取消订单0 查看详情 接单2 备货完成3 确认送达4 拒绝退款5 确认退款6 删除7 查看评价8
- (IBAction)managerButtonsAction:(UIButton *)sender {
    [self cellBtnClickTag:sender.tag];
}
#pragma mark - cell上按钮的点击
-(void)cellBtnClickTag:(NSInteger) tag{///取消订单0 查看详情1 接单2 备货完成3 提货验证4 拒绝退款5 确认退款6 删除7 查看评价8
    switch (tag) {
        case 0:
        {
            WeakSelf
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认取消该订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForCancelOrder];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }
            break;
        case 1://查看详情
            break;
        case 2:
        {
            [self takeoutOrderReceive:self.orderNo];
        }
            break;
        case 3:
        {
            [self takeoutOrderDelivery:self.orderNo];
        }
            break;
        case 4:
        {
            HomeCookVerifyViewController *vc = [HomeCookVerifyViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 5://拒绝退款
        {
            RefundViewController *vc = [RefundViewController new];
            vc.orderNo = self.orderNo;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 6://
        {
            WeakSelf
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认同意退款吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf takeoutOrderRefund:weakSelf.orderNo stuatus:2];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
            
          
        }
            break;
        case 7:
        {
            WeakSelf
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除该订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForDeleteOrder];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }
            break;
        case 8:
        {
            OrderCommentViewController *vc = [OrderCommentViewController new];
            vc.orderNo = self.orderNo;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark - 取消订单
- (void)requestForCancelOrder{
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderCancelURL params:@{@"orderNo":self.orderNo} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf loadBasicData];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 删除订单
- (void)requestForDeleteOrder {
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderDeleteURL params:@{@"orderNo":self.orderNo} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 拒绝/同意退款
-(void) takeoutOrderRefund:(NSString *)orderNo stuatus:(NSInteger)stuatus{
    //
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"orderNo"] = orderNo;
    dict[@"refundStatus"] = @(stuatus);
    
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderRefundURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"退款已完成" delay:1.0];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf loadBasicData];
            });
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - 标记配送中/待自提
-(void) takeoutOrderDelivery:(NSString *)orderNo {
    NSDictionary *dict = @{
        @"orderNo":orderNo
    };
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderDeliveryURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"备货已完成" delay:1.0];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf loadBasicData];
            });
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - 接单
-(void) takeoutOrderReceive:(NSString *)orderNo {
    NSDictionary *dict = @{
        @"orderNo":orderNo
    };
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderQueryReceiveURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"接单成功" delay:1.0];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf loadBasicData];
            });
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - 倒计时
- (void)startTimer:(int)time {
    WeakSelf
    __block int timeout=time;
    //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0);
    //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.subStatusView.hidden = YES;
            });
        }else{
            int seconds = timeout;
            //format of minute
            NSString *str_minute = [NSString stringWithFormat:@"%02d",(seconds%3600)/60];
            //format of second
            NSString *str_second = [NSString stringWithFormat:@"%02d",seconds%60];
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.countDownLabel.text = [NSString stringWithFormat:@"%@:%@",str_minute,str_second];
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
}


@end
