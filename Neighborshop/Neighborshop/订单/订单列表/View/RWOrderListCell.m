//
//  RWOrderListCell.m
//  PPK
//
//  Created by liuhan on 2022/4/12.
//

#import "RWOrderListCell.h"

@implementation RWOrderListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)buttonsAction:(UIButton *)sender {
    if (self.onButtonsClick) {
        self.onButtonsClick(sender.tag);
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
