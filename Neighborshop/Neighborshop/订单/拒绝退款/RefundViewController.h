//
//  RefundViewController.h
//  AdvorsysBusiness
//
//  Created by hailun on 2021/12/9.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RefundViewController : BaseViewController
//@property (nonatomic, copy) NSString *refund_id;
@property (nonatomic, copy) NSString *orderNo;
@end

NS_ASSUME_NONNULL_END
