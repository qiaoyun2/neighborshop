//
//  RefundViewController.m
//  AdvorsysBusiness
//
//  Created by hailun on 2021/12/9.
//

#import "RefundViewController.h"
#import "PlaceholderTextView.h"

@interface RefundViewController ()

@property (weak, nonatomic) IBOutlet UIView *textBgView;

@property (nonatomic, strong) PlaceholderTextView *textView;
@property (nonatomic, strong) NSString *content;

@end

@implementation RefundViewController

#pragma mark - 💓💓 Life Cycle Methods ------
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"拒绝退款";
    [self setUpTextView];
}

#pragma mark - 💓💓 Network ------

#pragma mark - 💓💓 Delegate ------

#pragma mark - 💓💓 Function ------
- (void)setUpTextView {
    
    self.textView = [[PlaceholderTextView alloc] init];
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.placeholderLabel.font = [UIFont systemFontOfSize:14];
    self.textView.placeholder = LocalizedString(@"请输入");
    self.textView.textColor = UIColorFromRGB(0x333333);
    self.textView.font = [UIFont systemFontOfSize:14];
    self.textView.frame = CGRectMake(12, 12, SCREEN_WIDTH-32-24, 120-24);
    self.textView.maxLength = 50;
    [self.textView didChangeText:^(PlaceholderTextView *textView) {
        self.content = textView.text;
    }];
    [self.textBgView addSubview:self.textView];
 
}

#pragma mark - 💓💓 XibFunction ------
- (IBAction)submitAction:(UIButton *)sender {
    if(self.content.length == 0){
        [LJTools showText:@"请输入拒绝理由" delay:1];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"orderNo"] = self.orderNo;
    dict[@"refundStatus"] = @(3);
    dict[@"refundFairDesc"] = self.content;
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderRefundURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"已经拒绝成功" delay:1.0];
            [[NSNotificationCenter defaultCenter]postNotificationName:OrderRefresh object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
    
}
#pragma mark - 💓💓 Lazy Loads ------
@end
