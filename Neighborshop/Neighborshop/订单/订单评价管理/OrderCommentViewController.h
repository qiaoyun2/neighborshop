//
//  OrderCommentViewController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/12/18.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderCommentViewController : BaseViewController
@property (nonatomic, strong) NSString *orderNo;
@end

NS_ASSUME_NONNULL_END
