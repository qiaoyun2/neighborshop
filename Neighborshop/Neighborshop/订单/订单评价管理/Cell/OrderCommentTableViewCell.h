//
//  OrderCommentTableViewCell.h
//  CookMaster
//
//  Created by qiaoyun on 2022/12/19.
//

#import <UIKit/UIKit.h>
#import "GBStarRateView.h"
#import "FoundListImage9TypographyView.h"
#import "OrderCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet GBStarRateView *starView;
@property (weak, nonatomic) IBOutlet FoundListImage9TypographyView *imgsView;

@property (nonatomic, strong) OrderCommentModel *model;

@property (nonatomic, copy) void(^btnClickBlock)(NSInteger type);
@end

NS_ASSUME_NONNULL_END
