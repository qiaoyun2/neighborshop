//
//  OrderCommentTableViewCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/12/19.
//

#import "OrderCommentTableViewCell.h"
@interface OrderCommentTableViewCell()
@property (weak, nonatomic) IBOutlet UIView *answerView;
@property (weak, nonatomic) IBOutlet UIView *masterView;
///回复
@property (weak, nonatomic) IBOutlet UILabel *masterAnsLabel;

@end

@implementation OrderCommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.starView.allowClickScore = NO;
    self.starView.allowSlideScore = NO;
    self.starView.spacingBetweenStars = 2;
    self.starView.starSize = CGSizeMake(10, 10);
    self.starView.starImage = [UIImage imageNamed:@"star_off"];
    self.starView.currentStarImage = [UIImage imageNamed:@"star_on"];
    self.starView.style = GBStarRateViewStyleIncompleteStar;
    self.starView.isAnimation = NO;
    self.starView.currentStarRate = 4;
}
- (void)setModel:(OrderCommentModel *)model
{
    _model = model;
    
    self.contentLabel.text = model.content;
    self.timeLabel.text = model.evaluateDate;
    if (model.videoUrl.length>0) {
        self.imgsView.videoPath = model.videoUrl;
        self.imgsView.imageArray= @[model.coverImage];
    }
    else {
        if (model.pictures.length>0) {
            NSArray *images = [model.pictures componentsSeparatedByString:@","];
            self.imgsView.imageArray= images;
        }
    }
    self.starView.currentStarRate = [model.star floatValue];
//    if (model.isAnonymous.integerValue==1) {
//        self.headImageView.image = DefaultImgHeader;
//        self.nameLabel.text = @"匿名用户";
//    }else {
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.avatar)] placeholderImage:DefaultImgHeader];
        self.nameLabel.text = model.nickName;
//    }
    
    self.answerView.hidden = model.shopReply.length > 0;
    self.masterView.hidden = model.shopReply.length  == 0;
    self.masterAnsLabel.text = model.shopReply;
    
}

- (IBAction)deleteBtnClick:(id)sender {
    if(self.btnClickBlock){
        self.btnClickBlock(1);
    }
}
- (IBAction)answerBtnClick:(id)sender {
    if(self.btnClickBlock){
        self.btnClickBlock(2);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
