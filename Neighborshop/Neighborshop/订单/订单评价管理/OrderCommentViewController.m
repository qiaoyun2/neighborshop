//
//  OrderCommentViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/12/18.
//

#import "OrderCommentViewController.h"
#import "TOOrderDetailGoodsCell.h"
#import "TOOrderDetailListModel.h"
#import "HomeOrderListModel.h"
#import "MyCommentCell.h"
#import "OrderCommentTableViewCell.h"
#import "OrderCommentModel.h"

#import "AnswerCommentController.h"

@interface OrderCommentViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) TOOrderDetailListModel *model;
@property (nonatomic, strong) OrderCommentModel *commentModel;

@end

@implementation OrderCommentViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getByOrderNo];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单评价";
 
    [self loadBasicData];
}
#pragma mark - 评价
-(void)getByOrderNo{
    WeakSelf
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"orderNo"] = self.orderNo;
    [NetworkingTool getWithUrl:kTakeoutEvaluateGetByOrderNoURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *dataDic = responseObject[@"data"];
            OrderCommentModel *model = [[OrderCommentModel alloc] initWithDictionary:dataDic];
            weakSelf.commentModel = model;
        } else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 网络请求
-(void)loadBasicData{
    WeakSelf
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"orderNo"] = self.orderNo;
    [NetworkingTool getWithUrl:kTakeoutOrderQueryByIdURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *dataDic = responseObject[@"data"];
            TOOrderDetailListModel *model = [[TOOrderDetailListModel alloc] initWithDictionary:dataDic];
            weakSelf.model = model;
        } else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.itemList.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == self.model.itemList.count){
        OrderCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderCommentTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"OrderCommentTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.model = self.commentModel;
        WeakSelf
        cell.btnClickBlock = ^(NSInteger type) {
            if(type == 1){
                [weakSelf  deleteComment];
            }else{
                [weakSelf  answerComment];
            }
        };
        return cell;
    }else{
        TOOrderDetailGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TOOrderDetailGoodsCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"TOOrderDetailGoodsCell" owner:nil options:nil] firstObject];
        }
        TOOrderGoodsModel *model = self.model.itemList[indexPath.row];
        [cell setItemObject:model];
        return cell;
    }
}
#pragma mark - 回复
-(void)answerComment{
    AnswerCommentController  *vc = [AnswerCommentController new];
    vc.evaluateId =  self.commentModel.evaluateId;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 删除
-(void)deleteComment{
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除回复吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
        [weakSelf deleteCommentRequaste];
    } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
}
#pragma mark - 删除接口
-(void)deleteCommentRequaste{
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutEvaluateDelReplyURL params:@{@"evaluateId":self.commentModel.evaluateId} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [weakSelf  getByOrderNo];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
    } IsNeedHub:YES];
}
@end
