//
//  OrderCommentModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/12/22.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface OrderCommentModel : BaseModel
@property (nonatomic, strong) NSString *avatar; //头像
@property (nonatomic, strong) NSString *content; // 评价内容
@property (nonatomic, strong) NSString *coverImage; //评价视频封面
@property (nonatomic, strong) NSString *evaluateDate;///评价时间
@property (nonatomic, strong) NSString *nickName;///用户昵称
@property (nonatomic, strong) NSString *pictures;///评价图片
@property (nonatomic, strong) NSString *shopReply;///商家回复内容
@property (nonatomic, strong) NSString *star;///星级
@property (nonatomic, strong) NSString *videoUrl;///评价视频地址
@property (nonatomic, strong) NSString *evaluateId;


@end

NS_ASSUME_NONNULL_END
