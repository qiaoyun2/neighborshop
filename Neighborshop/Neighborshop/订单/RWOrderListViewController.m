//
//  RWOrderListViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/16.
//

#import "RWOrderListViewController.h"
#import "ROrderListTableViewCell.h"
#import "HomeOrderListModel.h"

#import "RWOrderDetailViewController.h"
#import "HomeCookVerifyViewController.h"
#import "OrderCommentViewController.h"
#import "RefundViewController.h"

@interface RWOrderListViewController ()
@property (nonatomic, assign) NSInteger page;
@end

@implementation RWOrderListViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = UIColorFromRGB(0xF5F6F9);
    
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addSpecSuccess:) name:VerifyPickupCode object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addSpecSuccess:) name:OrderRefresh object:nil];
}
#pragma mark - 通知更新数据
- (void) addSpecSuccess:(NSNotification *)noti{
    [self refresh];
}
#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    /// -1-全部;0-已取消;1-待付款;2-待接单;3-配送中/自提;4-已送达/自提;5-已完成待评价;6-已完成;7-已接单;8-已关闭
    params[@"orderStatus"] = [self getStatus:self.type];
    NSString *url = kTakeoutOrderListURL;
    
    WeakSelf
    [NetworkingTool getWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"orderList"];
            for (NSDictionary *obj in dataArray) {
                HomeOrderListModel *model = [[HomeOrderListModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf addBlankOnView:weakSelf.tableView];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        [weakSelf.tableView.mj_header endRefreshing];
        if(weakSelf.dataArray.count < 10){
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [weakSelf.tableView.mj_footer endRefreshing];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
    //    return  6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ROrderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ROrderListTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ROrderListTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    HomeOrderListModel *model =  self.dataArray[indexPath.row];
    [cell setItemObject:model];
    WeakSelf
    cell.btnClickBlock = ^(NSInteger type) {
        [weakSelf cellBtnClick:model tag:type];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeOrderListModel *model =  self.dataArray[indexPath.row];
    RWOrderDetailViewController *vc = [RWOrderDetailViewController new];
    vc.orderNo = model.orderNo;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = UIColorFromRGB(0xF5F6F9);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}
#pragma mark - cell上按钮的点击
-(void)cellBtnClick:(HomeOrderListModel *)model tag:(NSInteger) tag{///取消订单0 查看详情1 接单2 备货完成3 提货验证4 拒绝退款5 确认退款6 删除7 查看评价8
    switch (tag) {
        case 0:
        {
            WeakSelf
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认取消该订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForCancelOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }
            break;
        case 1://查看详情
            break;
        case 2:
        {
            [self takeoutOrderReceive:model.orderNo];
        }
            break;
        case 3:
        {
            [self takeoutOrderDelivery:model.orderNo];
        }
            break;
        case 4:
        {
            HomeCookVerifyViewController *vc = [HomeCookVerifyViewController new];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 5://拒绝退款
        {
            RefundViewController *vc = [RefundViewController new];
            vc.orderNo = model.orderNo;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 6://
        {
            WeakSelf
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认同意退款吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf takeoutOrderRefund:model.orderNo stuatus:2];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
           
        }
            break;
        case 7:
        {
            WeakSelf
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"确认删除该订单吗？" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForDeleteOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
        }
            break;
        case 8:{
            OrderCommentViewController *vc = [OrderCommentViewController new];
            vc.orderNo = model.orderNo;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}
#pragma mark - 取消订单
- (void)requestForCancelOrder:(HomeOrderListModel *)model {
    WeakSelf
    NSDictionary *dict = @{
        @"orderNo":model.orderNo
    };
    [NetworkingTool postWithUrl:kTakeoutOrderCancelURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"取消成功" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf refresh];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
    } IsNeedHub:YES];
}
#pragma mark - 删除订单
- (void)requestForDeleteOrder:(HomeOrderListModel *)model {
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderDeleteURL params:@{@"orderNo":model.orderNo} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功" delay:1.5];
            [weakSelf.dataArray removeObject:model];
            [weakSelf.tableView reloadData];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}
#pragma mark - 拒绝/同意退款
-(void) takeoutOrderRefund:(NSString *)orderNo stuatus:(NSInteger)stuatus  {
    //
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"orderNo"] = orderNo;
    dict[@"refundStatus"] = @(stuatus);
    
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderRefundURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"退款已完成" delay:1.0];
            [weakSelf refresh];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - 标记配送中/待自提
-(void) takeoutOrderDelivery:(NSString *)orderNo {
    //
    NSDictionary *dict = @{
        @"orderNo":orderNo
    };
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderDeliveryURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"备货已完成" delay:1.0];
            [weakSelf refresh];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - 接单
-(void) takeoutOrderReceive:(NSString *)orderNo {
    //
    NSDictionary *dict = @{
        @"orderNo":orderNo
    };
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutOrderQueryReceiveURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"接单成功" delay:1.0];
            [weakSelf refresh];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - 设置状态
-(NSString *)getStatus:(NSInteger)type{
    NSString *status = @"";
    switch (self.type) {
        case 0:
            status = @"-1";
            break;
        case 1:
            status = @"1";
            break;
        case 2:
            status = @"2";
            break;
        case 3:
            status = @"4";
            break;
        case 4:
            status = @"6";
            break;
        default:
            break;
    }
    return status;
}
@end
