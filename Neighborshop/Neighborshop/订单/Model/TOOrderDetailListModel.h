//
//  TOOrderDetailListModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TOOrderDetailListModel : BaseModel
@property (nonatomic, strong) NSString *boxCost; // 打包费/餐盒费
@property (nonatomic, strong) NSString *sendCost; // 配送费用
@property (nonatomic, strong) NSString *orderTime; //下单时间
@property (nonatomic, strong) NSString *enableCancel;///是否可取消订单
@property (nonatomic, strong) NSString *paymentType; //支付方式：1-微信;2-支付宝
@property (nonatomic, strong) NSArray *itemList;
@property (nonatomic, strong) NSString *pickupCode;//取货码
@property (nonatomic, strong) NSString *orderNo;//orderNo
@property (nonatomic, strong) NSString *payTime;//剩余支付时间
@property (nonatomic, strong) NSString *refundAccountType;//退款账户类型: 1-退回余额;2-退回付款账户
@property (nonatomic, strong) NSString *refundDesc;//退款说明
@property (nonatomic, strong) NSString *refundMoney;//退款金额
@property (nonatomic, strong) NSString *refundStatus;//退款状态:0-未申请;1-申请中;2-退款失败;3-退款成功
@property (nonatomic, strong) NSString *refundTime;//退款时间
@property (nonatomic, strong) NSString *refundPicture;//退款图片
@property (nonatomic, strong) NSString *refundReason;//退款原因
@property (nonatomic, strong) NSString *refundOrderNo;//退款编号

@property (nonatomic, strong) NSString *sendAddress;//配送地址
@property (nonatomic, strong) NSString *sendTime;//配送/自提时间
@property (nonatomic, strong) NSString *shopAddress;//自提地址
@property (nonatomic, strong) NSString *shopId;//商铺id
@property (nonatomic, strong) NSString *shopName;//商铺名称
@property (nonatomic, strong) NSString *applyRefundTime;//申请退款时间
@property (nonatomic, strong) NSString *status;//订单状态：-1-全部;0-已取消;1-待付款;2-待接单;3-配送中/自提;4-已送达/自提;5-已完成待评价;6-已完成;7-已接单;8-已关闭
@property (nonatomic, strong) NSString *telphone;//商家联系电话
@property (nonatomic, strong) NSString *statusStr;//订单状态
@property (nonatomic, strong) NSString *totalMoney;//支付金额
@property (nonatomic, strong) NSString *type;//1-商家配送;2-到店自取
@property (nonatomic, strong) NSString *userMobile;
@property (nonatomic, strong) NSString *buyerMessage;//备注
@property (nonatomic, strong) NSString *selfDeliveryMobile;//自提人电话
///下单1分钟可取消订单0-不可取消;1-可取消
@property (nonatomic, strong) NSString *cancelOrderFlag;

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;

@end

NS_ASSUME_NONNULL_END
