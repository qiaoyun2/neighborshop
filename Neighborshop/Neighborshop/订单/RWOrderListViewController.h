//
//  RWOrderListViewController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/16.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RWOrderListViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, copy) NSString *start_data;
@property (nonatomic, copy) NSString *end_data;

@property (nonatomic, assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
