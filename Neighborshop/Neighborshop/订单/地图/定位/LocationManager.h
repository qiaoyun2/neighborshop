//
//  LocationManager.h
//  ZZR
//
//  Created by null on 2019/9/3.
//  Copyright © 2019 null. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
NS_ASSUME_NONNULL_BEGIN
typedef void (^LocationManagerBlock) (BOOL isLocation, CLLocation *nowLocation,NSString *province,NSString *city,NSString *area,NSString *address,double lat,double lng);
               
@interface LocationManager : NSObject


+(LocationManager*)shareManager;
/**
 开始定位
 
 @param location BOOL 开始定位
 */
-(void)startLocation:(BOOL)location;


/**
 开始定位
 
 @param location BOOL 开始定位
 */
-(void)startLocation:(BOOL)location withlocationManageBlock:(LocationManagerBlock)block;
@end

NS_ASSUME_NONNULL_END
