//
//  HomeCookTableCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeCookTableCell.h"
#import "HomeOrderListModel.h"

@interface HomeCookTableCell ()
///订单编号
@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
///订单编号
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
///商品图
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;
///标题
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
///规格
@property (weak, nonatomic) IBOutlet UILabel *specLabel;
///时间
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
///价格
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
///姓名
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
///电话
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
///地址
@property (weak, nonatomic) IBOutlet UILabel *addresslabel;
@property (weak, nonatomic) IBOutlet UIView *goodsView;
@property (weak, nonatomic) IBOutlet UIView *userView;

@property (nonatomic , strong) HomeOrderListModel *model;

@end

@implementation HomeCookTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setItemObject:(HomeOrderListModel *)object{
    self.model = object;
    self.orderNumLabel.attributedText = [LJTools attributString:@"订单编号：" twoStr:object.orderNo color:UIColorFromRGB(0x999999) oneHeight:14 andTColor:UIColorFromRGB(0x333333) twoHeight:14];

    self.priceLabel.attributedText = [LJTools attributedString:[NSString stringWithFormat:@"%.2lf",[object.totalFoodMoney doubleValue]] color:UIColorFromRGB(0xFA2033) oneHeight:11 andTColor:UIColorFromRGB(0xFA2033) twoHeight:16 andThreeTColor:UIColorFromRGB(0xFA2033) threeHeight:16];
    NSArray *itemList = object.itemList;
    if(itemList.count > 0){
        self.goodsView.hidden = NO;
        TOOrderGoodsModel *listModel = itemList[0];
        [self.goodsImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(listModel.picture)] placeholderImage:DefaultImgWidth];
        self.titleLabel.text = listModel.title;
        self.timeLabel.text = [NSString stringWithFormat:@"订单时间：%@",object.createTime];
        self.specLabel.text = [NSString stringWithFormat:@"商品规格：%@",listModel.skuName];
    }else{
        self.goodsView.hidden = YES;
    }
    if(object.type.intValue == 2){//自提
        self.statusLabel.text = @"已支付，待提取";
        self.nameLabel.text = object.shopName;
        self.phoneLabel.text = object.contactMobile;
        self.addresslabel.text = object.shopAddress;
    }else{
        self.statusLabel.text = @"已支付，待配送";
        self.nameLabel.text = object.userName;
        self.phoneLabel.text = object.userMobile;
        self.addresslabel.text = object.sendAddress;
    }
  
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
