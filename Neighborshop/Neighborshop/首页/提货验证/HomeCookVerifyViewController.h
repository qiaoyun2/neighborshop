//
//  HomeCookVerifyViewController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCookVerifyViewController : BaseViewController
@property (nonatomic, strong) NSString *orderNo;
@end

NS_ASSUME_NONNULL_END
