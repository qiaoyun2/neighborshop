//
//  HomeCookVerifyViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeCookVerifyViewController.h"

@interface HomeCookVerifyViewController ()
@property (weak, nonatomic) IBOutlet UITextField *codeText;

@end

@implementation HomeCookVerifyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"提货验证";
    [self.codeText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}
#pragma mark - 输入框观察者事件
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == self.codeText && self.codeText.text.length > 10) {
        self.codeText.text = [self.codeText.text substringToIndex:10];
    }
}

#pragma mark - Navigation
- (IBAction)sureBtnClick:(id)sender {
    if(self.codeText.text.length == 0){
        [LJTools showNOHud:@"请输入提货验证码" delay:1];
        return;
    }
    NSDictionary *dict = @{
        @"pickupCode":self.codeText.text
    };
    WeakSelf
    [NetworkingTool getWithUrl:kVerifyPickupCodeURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [LJTools showOKHud:@"提货成功" delay:1.0];
            [[NSNotificationCenter defaultCenter] postNotificationName:VerifyPickupCode object:nil userInfo:nil];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
    
}

@end
