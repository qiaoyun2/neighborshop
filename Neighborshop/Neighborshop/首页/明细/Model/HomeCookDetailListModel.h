//
//  HomeCookDetailListModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/26.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeCookDetailListModel : BaseModel
@property (nonatomic, strong) NSString *orderNo;
@property (nonatomic, strong) NSString *orderPrice;
@property (nonatomic, strong) NSString *orderStatus;
@property (nonatomic, strong) NSString *orderTime;
@end

NS_ASSUME_NONNULL_END
