//
//  HomeCookDetailTableCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeCookDetailTableCell.h"
#import "HomeCookDetailListModel.h"

@interface HomeCookDetailTableCell ()
///订单编号：1234567891
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
///订单时间：2022.9.23 14:23:32
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
///¥ 50.00
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
///状态已完成
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (nonatomic, strong) HomeCookDetailListModel *model;

@end
@implementation HomeCookDetailTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setItemObject:(HomeCookDetailListModel *)object{
    self.model = object;
    self.orderNoLabel.text = [NSString stringWithFormat:@"订单编号：%@",object.orderNo];
    self.priceLabel.text = [NSString stringWithFormat:@"¥%@",object.orderPrice];
    self.timeLabel.text = [NSString stringWithFormat:@"订单时间：%@",object.orderTime];
    //示例值：   -1全部;0-已取消;1-待付款;2-待接单;3-已拼音;4-配送中/自提;5-已完成待评价;6-已完成;7-已关闭
    self.statusLabel.text = [self getStatus:object.orderStatus];
}
-(NSString *)getStatus:(NSString *)status{
    NSString *title = @"";
    switch (status.intValue) {
        case 0:
            title = @"已取消";
            break;
        case 1:
            title = @"已取消";
            break;
        case 2:
            title = @"待付款";
            break;
        case 3:
            title = @"已接单";
            break;
        case 4:
            title = @"配送中/自提";
            break;
        case 5:
            title = @"已完成待评价";
            break;
        case 6:
            title = @"已完成";
            break;
        case 7:
            title = @"已关闭";
            break;
            
        default:
            break;
    }
    return title;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
