//
//  HomeCookDetailController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeCookDetailController.h"
#import "HomeCookDetailTableCell.h"
#import "SignInView.h"
#import "HomeTimeSelectView.h"
#import "HomeCookDetailListModel.h"

@interface HomeCookDetailController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeViewHeight;
///筛选区间
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic, copy) NSString *start_data;
@property (nonatomic, copy) NSString *end_data;
@property (nonatomic, assign) NSInteger page;
@end

@implementation HomeCookDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"查看明细";
    
    self.start_data = self.end_data = [NSDate jk_stringWithDate:[NSDate new] format:@"yyyy-MM-dd"];
    self.timeLabel.text = [NSString stringWithFormat:@"筛选区域：%@ 至%@",self.start_data,self.end_data];
    
    [self setNavigationRightBarButtonWithImage:@"路径 19589"];
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - 网络请求
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page ++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"startDate"] = self.start_data;
    params[@"endDate"] = self.end_data;
    WeakSelf
    [NetworkingTool getWithUrl:kTakeoutBusinessListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"orderList"];
            for (NSDictionary *obj in dataArray) {
                HomeCookDetailListModel *model = [[HomeCookDetailListModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf addBlankOnView:weakSelf.tableView];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        [weakSelf.tableView.mj_header endRefreshing];
        if(weakSelf.dataArray.count < 10){
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [weakSelf.tableView.mj_footer endRefreshing];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCookDetailTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCookDetailTableCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeCookDetailTableCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    HomeCookDetailListModel *model = self.dataArray[indexPath.row];
    [cell setItemObject:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - 删除
- (IBAction)deleteBtnClick:(id)sender {
    self.timeView.hidden = YES;
    self.timeViewHeight.constant = 0;
    self.start_data = self.end_data = nil;
    //    [self refresh];
}

#pragma mark – 选择时间
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender {
    WeakSelf;
    [HomeTimeSelectView show:^(NSString * _Nonnull startDate, NSString * _Nonnull endDate) {
        weakSelf.start_data = startDate;
        weakSelf.end_data = endDate;
        weakSelf.timeView.hidden = NO;
        weakSelf.timeViewHeight.constant = 40;
        weakSelf.timeLabel.text = [NSString stringWithFormat:@"筛选区域：%@ 至%@",startDate,endDate];
        [weakSelf refresh];
    }];
}

@end
