//
//  HomeTimeSelectView.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeTimeSelectView : UITableViewCell
+(HomeTimeSelectView *)show:(void(^)(NSString *startDate, NSString *endDate))block;
@end

NS_ASSUME_NONNULL_END
