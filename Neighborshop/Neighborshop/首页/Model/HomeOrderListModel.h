//
//  HomeOrderListModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/26.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface TOOrderGoodsModel : BaseModel

@property (nonatomic, strong) NSString *foodId; // 商品id
@property (nonatomic, strong) NSString *monthSales; // 月售
@property (nonatomic, strong) NSString *number; // 已选购数量
@property (nonatomic, strong) NSString *originPrice; //
@property (nonatomic, strong) NSString *picture; // 商品封面图(一张)
@property (nonatomic, strong) NSString *sellPrice; //
@property (nonatomic, strong) NSString *skuId; // 商品的skuid
@property (nonatomic, strong) NSString *skuName; // 商品的sku名称
@property (nonatomic, strong) NSString *title; // 商品名称
@property (nonatomic, strong) NSString *carItemId; // 购物车数据id
@property (nonatomic, strong) NSString *status; //

@end

@interface HomeOrderListModel : BaseModel
@property (nonatomic, strong) NSString *refundStatus;//退款状态:0-未申请;1-申请中;2-退款失败;3-退款成功
@property (nonatomic, strong) NSString *boxCost; // 打包费/餐盒费
@property (nonatomic, strong) NSString *sendCost; // 配送费用
@property (nonatomic, strong) NSString *totalOriginPrice; // 总原价
@property (nonatomic, strong) NSString *totalSellPrice; // 总原价
@property (nonatomic, strong) NSArray *itemList;
@property (nonatomic, strong) NSString *contactMobile;
@property (nonatomic, strong) NSString *orderNo;
@property (nonatomic, strong) NSString *payTime;
@property (nonatomic, strong) NSString *sendTime;
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *shopName;
@property (nonatomic, strong) NSString *createTime;
///下单1分钟可取消订单0-不可取消;1-可取消
@property (nonatomic, strong) NSString *cancelOrderFlag;
@property (nonatomic, strong) NSString *status;//订单状态：-1-全部;0-已取消;1-待付款;2-待接单;3-配送中/自提;4-已送达/自提;5-已完成待评价;6-已完成;7-已接单;8-已关闭
@property (nonatomic, strong) NSString *totalFoodMoney;
@property (nonatomic, strong) NSString *totalFoodNum;
///地址
@property (nonatomic, strong) NSString *shopAddress;
///手机号
@property (nonatomic, strong) NSString *userMobile;
///用户名
@property (nonatomic, strong) NSString *userName;
///用户名
@property (nonatomic, strong) NSString *selfDeliveryMobile;
///配送地址
@property (nonatomic, strong) NSString *sendAddress;
///2自提
@property (nonatomic, strong) NSString *type;
@end

NS_ASSUME_NONNULL_END
