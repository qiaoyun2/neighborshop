//
//  LMJobHeaderNoticeModel.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/26.
//

#import "LMJobHeaderNoticeModel.h"

@implementation LMJobHeaderNoticeModel
- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"id"]) {
        self.ID = [NSString stringWithFormat:@"%@",value];
    }
}
@end
