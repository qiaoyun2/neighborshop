//
//  HomeOrderListModel.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/26.
//

#import "HomeOrderListModel.h"
@implementation TOOrderGoodsModel

@end

@implementation HomeOrderListModel
- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"itemList"]||[key isEqual:@"foodList"]) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dic in value) {
            TOOrderGoodsModel *model = [[TOOrderGoodsModel alloc] initWithDictionary:dic];
            [array addObject:model];
        }
        self.itemList = [NSArray arrayWithArray:array];
    }
}
@end
