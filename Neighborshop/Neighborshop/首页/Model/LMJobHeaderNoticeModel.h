//
//  LMJobHeaderNoticeModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/26.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LMJobHeaderNoticeModel : BaseModel
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *title;
@end

NS_ASSUME_NONNULL_END
