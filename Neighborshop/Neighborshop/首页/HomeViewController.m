//
//  HomeViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/12.
//

#import "HomeViewController.h"
#import "HomeCookTableCell.h"
#import "ScrollNoticeCell.h"
#import <SDCycleScrollView.h>
#import "HomeCookDetailController.h"
#import "HomeCookVerifyViewController.h"
#import "RWOrderDetailViewController.h"
#import "MessageBgSetViewController.h"

#import "LMJobHeaderNoticeModel.h"
#import "HomeOrderListModel.h"


@interface HomeViewController ()<SDCycleScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navViewTop;
@property (weak, nonatomic) IBOutlet UIView *noticeBgView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
///店铺信息
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel;
///今日营业统计
@property (weak, nonatomic) IBOutlet UILabel *todaySaleLabel;
@property (weak, nonatomic) IBOutlet UILabel *todayOrderLabel;
///历史营业统计
@property (weak, nonatomic) IBOutlet UILabel *historySaleLabel;
@property (weak, nonatomic) IBOutlet UILabel *historyMoneyLabel;
@property (nonatomic, strong) SDCycleScrollView *noticeView;

@property (nonatomic, strong) NSArray *noticeArray;
@property (nonatomic, assign) NSInteger page;
@end

@implementation HomeViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self loadBaiscData];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = UIColorFromRGB(0xF6F7F9);
    self.navViewTop.constant = StatusHight;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 435);
    self.tableView.tableHeaderView = self.headerView;
    
    WeakSelf
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    
    [self refresh];
    
    [self.noticeBgView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        MessageBgSetViewController *vc = [MessageBgSetViewController new];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [self initNoticeView];
   
}
#pragma mark - UI
-(void)initNoticeView{
    _noticeView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, (SCREEN_WIDTH-56), 32) delegate:self placeholderImage:nil];
    _noticeView.scrollDirection = UICollectionViewScrollDirectionVertical;
    _noticeView.localizationImageNamesGroup = @[@"",@""];
    _noticeView.delegate = self;
    _noticeView.showPageControl = NO;
    _noticeView.autoScrollTimeInterval = 3;
    _noticeView.backgroundColor = [UIColor clearColor];
    [_noticeView disableScrollGesture];
    [self.noticeBgView addSubview:_noticeView];
}
#pragma mark - 网络请求
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page ++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
//    params[@"orderStatus"] = @(3);
    WeakSelf
    [NetworkingTool getWithUrl:kTakeoutOrderFirstListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"orderList"];
            for (NSDictionary *obj in dataArray) {
                HomeOrderListModel *model = [[HomeOrderListModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf addBlankOnView:weakSelf.tableView withY:435];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        if(weakSelf.dataArray.count < 10){
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [weakSelf.tableView.mj_footer endRefreshing];
        }
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}

#pragma mark - 网络请求
-(void)loadBaiscData {
    WeakSelf
    [NetworkingTool getWithUrl:kTakeoutShoppMsgURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *dict = responseObject[@"data"];
            [weakSelf updateUI:dict];

        } else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 更新UI
- (void)updateUI :(NSDictionary *)dict {
    if([dict[@"logoImg"] length] > 0){
        [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(dict[@"logoImg"])] placeholderImage:[UIImage imageNamed:@"组 9903"]];
    }
    if([dict[@"shopName"] length] > 0){
        self.shopNameLabel.text = dict[@"shopName"];
    }else{
        self.shopNameLabel.text = @"";
    }
    if([dict[@"todaySaleData"] integerValue] >= 0){
        self.todaySaleLabel.text = [NSString stringWithFormat:@"%@",dict[@"todaySaleData"]];
    }else{
        self.todaySaleLabel.text = @"0";
    }
    
    if([dict[@"todayOrderData"] integerValue] >= 0){
        self.todayOrderLabel.text = [NSString stringWithFormat:@"%@",dict[@"todayOrderData"]];
    }else{
        self.todayOrderLabel.text = @"0";
    }
    if([dict[@"totalOrderData"] integerValue] >= 0){
        self.historyMoneyLabel.text = [NSString stringWithFormat:@"%@",dict[@"totalOrderData"]];
    }else{
        self.historyMoneyLabel.text = @"0";
    }
    if([dict[@"totalSaleData"] integerValue] >= 0){
        self.historySaleLabel.text = [NSString stringWithFormat:@"%@",dict[@"totalSaleData"]];
    }else{
        self.historySaleLabel.text = @"0";
    }
    NSArray *array = dict[@"messageList"];
    self.noticeArray = [NSArray arrayWithArray:array];
    NSMutableArray *tempAray = [NSMutableArray array];
    for (int i = 0; i<self.noticeArray.count; i++) {
        [tempAray addObject:@"矩形 16382"];
    }
    self.noticeView.imageURLStringsGroup = tempAray;
}

#pragma mark - Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCookTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeCookTableCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeCookTableCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    HomeOrderListModel *model = self.dataArray[indexPath.row];
    [cell setItemObject:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RWOrderDetailViewController *vc = [RWOrderDetailViewController new];
    HomeOrderListModel *model = self.dataArray[indexPath.row];
    vc.orderNo  = model.orderNo;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - SDCycleScrollViewDelegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (self.noticeArray.count) {
//        LMJobHeaderNoticeModel *model = self.noticeArray[index];
//        WKWebViewController *vc = [[WKWebViewController alloc] init];
//        vc.contentStr = model.content;
//        vc.titleStr = model.title;
//        [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
    }
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index
{
    
}

/** 如果你需要自定义cell样式，请在实现此代理方法返回你的自定义cell的Nib。 */
- (UINib *)customCollectionViewCellNibForCycleScrollView:(SDCycleScrollView *)view
{
    if (view != self.noticeView) {
        return nil;
    }
    return [UINib nibWithNibName:@"ScrollNoticeCell" bundle:nil];
}

/** 如果你自定义了cell样式，请在实现此代理方法为你的cell填充数据以及其它一系列设置 */
- (void)setupCustomCell:(UICollectionViewCell *)cell forIndex:(NSInteger)index cycleScrollView:(SDCycleScrollView *)view
{
    ScrollNoticeCell *myCell = (ScrollNoticeCell *)cell;
    myCell.backgroundColor = [UIColor whiteColor];
    if (self.noticeArray.count) {
        LMJobHeaderNoticeModel *model = self.noticeArray[index];
        myCell.contentLabel.text = model.title;
    }
}


#pragma mark - 验证
- (IBAction)verifyBtnClick:(id)sender {
    HomeCookVerifyViewController *vc = [HomeCookVerifyViewController new];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 详情
- (IBAction)detailBtnClick:(id)sender {
    HomeCookDetailController *vc = [HomeCookDetailController new];
    [self.navigationController pushViewController:vc animated:YES];
}


@end
