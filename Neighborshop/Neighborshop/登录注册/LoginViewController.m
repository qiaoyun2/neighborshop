//
//  LoginViewController.m
//  ZZR
//
//  Created by null on 2018/12/13.
//  Copyright © 2018 null. All rights reserved.
//

#import "LoginViewController.h"
#import "CodeLoginViewController.h"
#import "ForgetPasswordViewController.h"
#import "PasswordLoginController.h"
#import "TabBarController.h"

@interface LoginViewController ()


@property (weak, nonatomic) IBOutlet UIButton *agreementButton;
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageViewTop;
///三方登录
@property (weak, nonatomic) IBOutlet UIView *threeLoginView;

@end

@implementation LoginViewController

#pragma mark - Life Cycle Methods

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.logoImageViewTop.constant = 40+StatusHight;
//    CDKOtherBtnBackgroundGradient(_wechatButton, @[UIColorNamed(@"F4D69E"), UIColorNamed(@"E5B971")]);
   
    [self updateLogin];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLogin) name:LoginChangeSuccess object:nil];
    
    [_mobileField addTarget:self action:@selector(textFieldDidChange:)
       forControlEvents:UIControlEventEditingChanged];
    
}

#pragma mark – UI
-(void)updateLogin{
    NSString *str = [[NSUserDefaults standardUserDefaults] objectForKey:AppIsOnline];
    self.threeLoginView.hidden = YES;
    if ([str isEqualToString:@"1"]) {
        self.threeLoginView.hidden = NO;
        [self.view layoutIfNeeded];
    }
}
#pragma mark - 💓💓 Function ------
/** 输入框观察者事件 */
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == _mobileField && _mobileField.text.length > 11) {
        _mobileField.text = [_mobileField.text substringToIndex:11];
    }
}
#pragma mark - Network

/// 微信登录
/// @param result <#result description#>
- (void)wechatLogin:(id)result {
    
    UMSocialUserInfoResponse *response = result;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    parameters[@"type"] = @"1";
    parameters[@"openId"] = response.unionId;
    
    [NetworkingTool postWithUrl:kThirdLoginURL params:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            User *user = [User mj_objectWithKeyValues:responseObject[@"data"][@"user"]];
            [User saveUser:user];
            [UIApplication sharedApplication].keyWindow.rootViewController = [[TabBarController alloc] init];
//            [self.navigationController popToRootViewControllerAnimated:YES];
        } else if ([responseObject[@"code"] intValue] == 0) {
            ForgetPasswordViewController *vc = [ForgetPasswordViewController new];
            vc.openId = response.unionId;
            vc.type =  2;
            [self.navigationController pushViewController:vc animated:YES];
            
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

- (void)requestForSendCode:(UIButton *)sender
{
    if (self.mobileField.text.length == 0) {
        [LJTools showText:@"请输入手机号" delay:1.5];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1.5];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.mobileField.text;
    params[@"event"] = @"login";
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] integerValue]==1) {
            [sender setCountdown:60 WithStartString:@"" WithEndString:@"获取验证码"];
            CodeLoginViewController *viewController = [CodeLoginViewController new];
            viewController.mobile = self.mobileField.text;
            [self.navigationController pushViewController:viewController animated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        
    } IsNeedHub:YES];
}

#pragma mark - Delegate
#pragma mark - UITableViewDelgate
#pragma mark – Function
#pragma mark – XibFunction
/// 发送验证码
- (IBAction)codeButtonAction:(UIButton *)sender {
    if (!_agreementButton.selected) {
        [LJTools showText:@"请勾选阅读并同意《用户注册协议》与《隐私政策》" delay:1];
        return;
    }
    [self requestForSendCode:sender];
}
/// 微信登录
/// @param sender <#sender description#>
- (IBAction)wechatLoginAction:(id)sender {
    if (!_agreementButton.selected) {
        [LJTools showText:@"请勾选阅读并同意《用户注册协议》与《隐私政策》" delay:1];
        return;
    }
    @weakify(self);
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:self completion:^(id result, NSError *error) {
        if (error) {
            /// 第三方登录数据(为空表示平台未提供)
            NSLog(@"=====> error : %@", error);
        } else {
            @strongify(self);
            [self wechatLogin:result];
        }
    }];
}

/// 手机号登录
/// @param sender <#sender description#>
- (IBAction)mobileLoginAction:(id)sender {
    if (!_agreementButton.selected) {
        [LJTools showText:@"请勾选阅读并同意《用户注册协议》与《隐私政策》" delay:1];
        return;
    }
    PasswordLoginController *viewController = [PasswordLoginController new];
    [self.navigationController pushViewController:viewController animated:YES];
}

/// 选中协议
/// @param sender <#sender description#>
- (IBAction)selectedAgreementAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}

/// 用户注册协议
/// @param sender <#sender description#>
- (IBAction)registrationAgreement:(id)sender {
    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"merchantXyUrl"} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSString *url = (NSString *)responseObject[@"data"];
            WKWebViewController *vc = [WKWebViewController new];
            vc.titleStr = @"用户协议";
            vc.urlStr = url;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

/// 隐私政策
/// @param sender <#sender description#>
- (IBAction)privacyPolicy:(id)sender {
    /// 隐私政策
    [NetworkingTool getWithUrl:kConfigNameURL params:@{@"configName":@"merchantPolicyUrl"} success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSString *url = (NSString *)responseObject[@"data"];
            WKWebViewController *vc = [WKWebViewController new];
            vc.titleStr = @"隐私政策";
            vc.urlStr = url;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

#pragma mark - Lazy Loads

@end
