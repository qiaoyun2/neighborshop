//
//  ForgetPasswordViewController.m
//  ZZR
//
//  Created by null on 2018/12/24.
//  Copyright © 2018 null. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "UIButton+Code.h"
#import "TabBarController.h"

@interface ForgetPasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *mobileField;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UIButton *secureButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationBarTop;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;


@end

@implementation ForgetPasswordViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBarTop.constant = StatusHight;
    if(self.type == 1){
        self.titleLabel.text = @"忘记密码";
        [self.sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    }else{
        self.titleLabel.text = @"绑定手机号";
        [self.sureBtn setTitle:@"绑定手机号并登录" forState:UIControlStateNormal];
    }
    
    
    [_mobileField addTarget:self action:@selector(textFieldDidChange:)
       forControlEvents:UIControlEventEditingChanged];
    [_codeField addTarget:self action:@selector(textFieldDidChange:)
       forControlEvents:UIControlEventEditingChanged];
    [_passwordField addTarget:self action:@selector(textFieldDidChange:)
       forControlEvents:UIControlEventEditingChanged];
}

#pragma mark -- xib Action
//获取验证码
- (IBAction)codeButtonAction:(UIButton *)sender {
    [self.view endEditing:YES];
    if (self.mobileField.text.length == 0) {
        [LJTools showText:@"请输入手机号" delay:1];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.mobileField.text;
    if (self.type==1) {
        params[@"event"] = @"forget";
    }else {
        params[@"event"] = @"bindThird";
    }
    [NetworkingTool postWithUrl:kGetCodeURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            [sender setCountdown:60 WithStartString:@"" WithEndString:@"获取验证码"];
        }
        [LJTools showText:responseObject[@"msg"] delay:1];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showText:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - 💓💓 Function ------
/** 输入框观察者事件 */
- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == _mobileField && _mobileField.text.length > 11) {
        _mobileField.text = [_mobileField.text substringToIndex:11];
    }
    if (textField == _codeField && _codeField.text.length > 4) {
        _codeField.text = [_codeField.text substringToIndex:4];
    }
    if (textField == _passwordField && _passwordField.text.length > 12) {
        _passwordField.text = [_passwordField.text substringToIndex:12];
    }
}
//确定提交
- (IBAction)commitButtonAction:(id)sender {
    [self.view endEditing:YES];
    if (self.mobileField.text.length == 0) {
        [LJTools showText:@"请输入手机号" delay:1];
        return;
    }
    if (![self.mobileField.text isTelephone]) {
        [LJTools showText:@"请输入正确的手机号" delay:1];
        return;
    }
    if ([self.codeField.text isBlankString]) {
        [LJTools showText:@"请输入验证码" delay:1];
        return;
    }
    if ([self.passwordField.text isBlankString]) {
        [LJTools showText:@"请输入密码" delay:1];
        return;
    }
//    if (![self.passwordField.text isPassword]) {
//        [LJTools showNOHud:@"密码仅限6-12位大小写字母数字" delay:1];
//        return;
//    }
    NSString *url = self.type == 1 ? kForgetPwdURL : kThirdBindURL;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"mobile"] = self.mobileField.text;
    params[@"password"] = self.passwordField.text;
    params[@"captcha"] = self.codeField.text;
    if (self.type==1) {
        params[@"event"] = @"forget";
    }else {
        params[@"event"] = @"bindThird";
        params[@"unionID"] = self.openId;
        ///'0':QQ,'1':微信
        params[@"platform"] = @"1";
//
    }
    [NetworkingTool postWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *userDic = responseObject[@"data"][@"user"];
            User *user = [User mj_objectWithKeyValues:userDic];
            [User saveUser:user];
//            if (self.type == 1) {
//                self.block(self.mobileField.text, self.passwordField.text);
//            }
            [UIApplication sharedApplication].keyWindow.rootViewController = [[TabBarController alloc] init];
        } else {
            [LJTools showText:responseObject[@"msg"] delay:1];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showText:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

- (IBAction)secureButtonAction:(UIButton*)sender {
    self.passwordField.secureTextEntry = sender.selected;
    sender.selected = !sender.selected;
}
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
