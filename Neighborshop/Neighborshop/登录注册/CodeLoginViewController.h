//
//  CodeLoginViewController.h
//  ZZR
//
//  Created by null on 2019/5/16.
//  Copyright © 2019 null. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

/** CodeLoginViewController 手机号登录 */
@interface CodeLoginViewController : BaseViewController

@property (nonatomic, strong) NSString *mobile;

@end

NS_ASSUME_NONNULL_END
