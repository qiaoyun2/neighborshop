//
//  TakeoutFoodSpecListModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/12/2.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TakeoutFoodSpecListModel : BaseModel
///规格id
@property (nonatomic, strong) NSString *specId;///
///规格名称
@property (nonatomic, strong) NSString *specName;
///规格名称
@property (nonatomic, strong) NSString *specValueName;
///规格名称
@property (nonatomic, strong) NSString *specValueId;
///排序
@property (nonatomic, strong) NSString *sort;
///是否可视：0-可见;1-不可见
@property (nonatomic, strong) NSString *isVisible;
///是否可视：0-可见;1-不可见
@property (nonatomic, strong) NSString *status;
///规格说明
@property (nonatomic, strong) NSString *specDes;
@property (nonatomic, assign) NSInteger number;
@end

NS_ASSUME_NONNULL_END
