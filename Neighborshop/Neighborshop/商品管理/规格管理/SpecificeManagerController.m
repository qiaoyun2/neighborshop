//
//  SpecificeManagerController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/14.
//

#import "SpecificeManagerController.h"
#import "SpeciticeManagerCell.h"
#import "SpecSetTableViewCell.h"
#import "EditSpecificeViewController.h"

#import "TakeoutFoodSpecListModel.h"

@interface SpecificeManagerController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *specSetHeaderView;
@property (weak, nonatomic) IBOutlet UIView *specHeaderView;

@end

@implementation SpecificeManagerController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"规格管理";
    self.specHeaderView.hidden = NO;
    self.specSetHeaderView.hidden = YES;
    if(self.type == 2){
        self.navigationItem.title = @"规格值配置";
        self.specHeaderView.hidden = YES;
        self.specSetHeaderView.hidden = NO;
    }
   
    [self setNavigationRightBarButtonWithTitle:@"新增" color:UIColorFromRGB(0x333333)];
    [self loadBasicData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addSpecSuccess:) name:AddSpecSuccess object:nil];
    
}
#pragma mark - 通知更新数据
- (void) addSpecSuccess:(NSNotification *)noti{
    [self loadBasicData];
}

#pragma mark - 获取分类接口
-(void)loadBasicData{
    WeakSelf
    NSString *url = kTakeoutFoodSpecListURL;
    NSDictionary *dict = @{};
    if(self.type == 2){
        url  = kTakeoutFoodSpecValueListURL;
        dict  = @{
            @"specId":self.specId
        };
    }
    [NetworkingTool getWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [weakSelf.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                TakeoutFoodSpecListModel *model = [[TakeoutFoodSpecListModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }  else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    return 10;
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.type == 2){
        SpecSetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SpecSetTableViewCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"SpecSetTableViewCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        TakeoutFoodSpecListModel *model = self.dataArray[indexPath.row];
        [cell setItemObject:model];
        WeakSelf
        cell.btnClickBlock = ^{
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"您确定要删除该规格值吗?" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForDeleteOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
            
        };
        return cell;
    }else{
        SpeciticeManagerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SpeciticeManagerCell"];
        if (!cell) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"SpeciticeManagerCell" owner:nil options:nil] firstObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        TakeoutFoodSpecListModel *model = self.dataArray[indexPath.row];
        model.number = indexPath.row+1;
        [cell setItemObject:model];
        cell.setttingBtn.hidden =  self.type == 2;
       
        WeakSelf
        cell.btnClickBlock = ^{
            [UIAlertController alertViewNormalWithTitle:@"提示" message:@"您确定要删除该规格吗?" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
                [weakSelf requestForDeleteOrder:model];
            } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
            
        };
        return cell;
    }
   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TakeoutFoodSpecListModel *model = self.dataArray[indexPath.row];
    EditSpecificeViewController *vc = [EditSpecificeViewController new];
    vc.specId = model.specId;
    vc.specValueId = model.specValueId;
    vc.type = self.type;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 删除
-(void)requestForDeleteOrder:(TakeoutFoodSpecListModel *)model{
    NSString *url  = @"";
    NSDictionary *dict = @{ };
    if(self.type == 2){
        url = kTakeoutFoodDeleteBySpecValueIdURL;
        dict = @{
            @"specValueId":model.specValueId
        };
    }else{
        dict = @{
            @"specId":model.specId
        };
        url  = kTakeoutFoodDeleteBySpecIdURL;
    }
    WeakSelf
    [NetworkingTool postWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"删除成功！" delay:1.5];
            [weakSelf.dataArray removeObject:model];
            [weakSelf.tableView reloadData];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
#pragma mark - 右边按钮
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender{
    EditSpecificeViewController *vc = [EditSpecificeViewController new];
    vc.isNew = YES;
    vc.type = self.type;
    vc.specId = self.specId;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
