//
//  EditSpecificeViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/14.
//

#import "EditSpecificeViewController.h"
#import "TakeoutFoodSpecListModel.h"
#import "SpecificeManagerController.h"

@interface EditSpecificeViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UIButton *yesAndNoBtn;
@property (weak, nonatomic) IBOutlet UIButton *sortBtn;
@property (weak, nonatomic) IBOutlet UIView *explainView;
@property (weak, nonatomic) IBOutlet UIView *specSetView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specSetViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specSetViewBottom;
@property (weak, nonatomic) IBOutlet UITextField *specSetText;

@property (weak, nonatomic) IBOutlet UITextField *explainText;
@property (nonatomic, strong) TakeoutFoodSpecListModel *model;

@end

@implementation EditSpecificeViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(self.specId ){
        [self loadSpecSetBasicData];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"编辑";
    if(self.isNew){
        self.navigationItem.title = @"新增规格";
    }
    [self.nameText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.explainText addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self setNavigationRightBarButtonWithTitle:@"完成" color:UIColorFromRGB(0x00A2EA)];
    if(self.type == 2){
        self.specSetView.hidden = self.explainView.hidden = YES;
        self.specSetViewBottom.constant =  self.specSetViewHeight.constant = 0;
        if(self.specValueId ){
            [self loadBasicData];
        }
    }else{
        self.specSetView.hidden = self.explainView.hidden = NO;
        self.specSetViewBottom.constant = 10;
        self.specSetViewHeight.constant = 44;
        if(self.specId ){
            [self loadBasicData];
        }
    }
    
}
#pragma mark - 获取分类接口
-(void)loadSpecSetBasicData{
    WeakSelf
    NSString *url  = kTakeoutFoodSpecValueListURL;
    NSDictionary *dict  = @{
        @"specId":self.specId
    };
    
    [NetworkingTool getWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [weakSelf.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                TakeoutFoodSpecListModel *model = [[TakeoutFoodSpecListModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model.specValueName];
            }
            weakSelf.specSetText.text = [weakSelf.dataArray componentsJoinedByString:@","];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        // [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark -- 获取网络详情
-(void)loadBasicData{
    WeakSelf
    NSDictionary *dict = @{};
    NSString *url = @"";
    
    if(self.type == 2){
        url =  kTakeoutFoodGetBySpecValueIdURL;
        dict = @{
            @"specValueId":self.specValueId
        };
    }else{
        url = kTakeoutFoodGetBySpecIdURL;
        dict = @{
            @"specId":self.specId
        };
    }
    [NetworkingTool getWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [weakSelf.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary  *dict = responseObject[@"data"];
            TakeoutFoodSpecListModel *model = [[TakeoutFoodSpecListModel alloc] initWithDictionary:dict];
            weakSelf.model = model;
            [weakSelf updateUI];
        }  else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark -- 更新UI
-(void)updateUI{
    self.nameText.text = [self.model.specValueName length] > 0 ?self.model.specValueName:self.model.specName;
    self.explainText.text = self.model.specDes;
    [self.sortBtn setTitle:[NSString stringWithFormat:@"%@",self.model.sort] forState:UIControlStateNormal];
    /////是否可视：0-可见;1-不可见
    [self.yesAndNoBtn setSelected:!self.model.isVisible.intValue];
}
#pragma mark -- UITextFieldDeletefate
- (void)textFieldDidChange:(UITextField *)textField{
    NSInteger kMaxLength = 5;
    if(textField  == self.explainText){
        kMaxLength = 20;
    }
    NSString *toBeString = textField.text;
    NSString *lang = [[UIApplication sharedApplication]textInputMode].primaryLanguage; //ios7之前使用[UITextInputMode currentInputMode].primaryLanguage
    if ([lang isEqualToString:@"zh-Hans"]) { //中文输入
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        if (!position) {// 没有高亮选择的字，则对已输入的文字进行字数统计和限制
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
                [LJTools showText:[NSString stringWithFormat:@"最多输入%ld字", kMaxLength] delay:1.0f];
            }
        }
    }else{//中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
            [LJTools showText:[NSString stringWithFormat:@"最多输入%ld字", kMaxLength] delay:1.0f];
        }
    }
}
- (IBAction)selectSortBtnClick:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    [UIAlertController actionSheetWithTitle:@"请选择商品排序" message:nil titlesArry:@[@"1",@"2",@"3",@"4",@"5",@"6"] indexBlock:^(NSInteger index, id obj) {
        NSLog(@"%ld",index);
        [sender setTitle:obj forState:UIControlStateNormal];
    }];
    
}

#pragma mark - 是否可视
- (IBAction)yesAndNoBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}
#pragma mark - 规格值配置
- (IBAction)selectBtnClick:(id)sender {
    [self.view endEditing:YES];
    if(self.isNew && self.type == 1){
        if([self.nameText.text length] == 0){
            [LJTools showText:@"请输入规格名称" delay:1.0f];
            return;
        }
        if([self.sortBtn.titleLabel.text isEqualToString:@"请选择"]){
            [LJTools showText:@"请输入规格排序" delay:1.0f];
            return;
        }
        if([self.explainText.text length] == 0){
            [LJTools showText:@"请输入规格说明" delay:1.0f];
            return;
        }
        if([self.explainText.text length] == 0){
            [LJTools showText:@"请输入规格说明" delay:1.0f];
            return;
        }
        
        
        NSString *url = @"";
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[@"specName"] = self.nameText.text;
        ///否可视：0-可见;1-不可见
        dict[@"isVisible"] = self.yesAndNoBtn.selected?@"0":@"1";
        dict[@"sort"] = self.sortBtn.titleLabel.text;
        dict[@"specDes"] = self.explainText.text;
        
        url = kTakeoutFoodSpecAddURL;
        WeakSelf
        [NetworkingTool postWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
            [LJTools hideHud];
            if ([responseObject[@"code"] integerValue]==1) {
                NSString *idStr = responseObject[@"data"];
                weakSelf.specId   =  idStr;
                weakSelf.isNew  = NO;
                SpecificeManagerController *vc = [SpecificeManagerController new];
                vc.type = 2;
                vc.specId = idStr;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else {
                [LJTools showText:responseObject[@"msg"] delay:1.5];
            }
        } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
            [LJTools showNOHud:RequestServerError delay:1.0];
        } IsNeedHub:YES];
    }else{
        SpecificeManagerController *vc = [SpecificeManagerController new];
        vc.type = 2;
        vc.specId = self.specId;
        [self.navigationController pushViewController:vc animated:YES];
    }
   
}
#pragma mark - 右边按钮
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender{
    [self.view endEditing:YES];
    if([self.nameText.text length] == 0){
        [LJTools showText:@"请输入规格名称" delay:1.0f];
        return;
    }
    if(self.type == 1){
        if([self.specSetText.text length] == 0){
            [LJTools showText:@"请配置规格值属性" delay:1.0f];
            return;
        }
    }
    if([self.sortBtn.titleLabel.text isEqualToString:@"请选择"]){
        [LJTools showText:@"请输入规格排序" delay:1.0f];
        return;
    }
    
    NSString *url = @"";
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if(self.type ==  2){
        url = kTakeoutFoodSpecValueAddURL;
        dict[@"specValueName"] = self.nameText.text;
        ///是否显视: 0-显示;1-不显示
        dict[@"status"] = self.yesAndNoBtn.selected?@"0":@"1";
        dict[@"sort"] = self.sortBtn.titleLabel.text;
        //        dict[@"specDes"] = self.explainText.text;
        dict[@"specId"] = self.specId;
        if(self.specValueId.length > 0){
            dict[@"specValueId"] = self.specValueId;
            url = kTakeoutFoodSpecValueEditURL;
        }
    }else{
        if([self.explainText.text length] == 0){
            [LJTools showText:@"请输入规格说明" delay:1.0f];
            return;
        }
        
        dict[@"specName"] = self.nameText.text;
        ///否可视：0-可见;1-不可见
        dict[@"isVisible"] = self.yesAndNoBtn.selected?@"0":@"1";
        dict[@"sort"] = self.sortBtn.titleLabel.text;
        dict[@"specDes"] = self.explainText.text;
        if(self.specId.length > 0){
            dict[@"specId"] = self.specId;
        }
        url = kTakeoutFoodSpecAddURL;
        if(self.specId.length > 0){
            url = kTakeoutFoodSpecEditURL;
        }
    }
    
    WeakSelf
    [NetworkingTool postWithUrl:url params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:self.isNew?@"添加成功！" :@"编辑成功！" delay:1.5];
            [weakSelf.navigationController popViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:AddSpecSuccess object:nil userInfo:nil];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}
@end
