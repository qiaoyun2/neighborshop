//
//  SpeciticeManagerCell.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpeciticeManagerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *setttingBtn;
-(void)setItemObject:(id)object;
@property (nonatomic, copy) void(^btnClickBlock)(void);
@end

NS_ASSUME_NONNULL_END
