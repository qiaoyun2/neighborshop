//
//  SpecSetTableViewCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/12/18.
//

#import "SpecSetTableViewCell.h"
#import "EditSpecificeViewController.h"
#import "SpecificeManagerController.h"
#import "TakeoutFoodSpecListModel.h"
@interface SpecSetTableViewCell()
@property(nonatomic, strong)TakeoutFoodSpecListModel *model;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UILabel *isVisibleLabel;
@end
@implementation SpecSetTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setItemObject:(TakeoutFoodSpecListModel *)object{
    self.model = object;
    self.nameLabel.text = object.specValueName;
    self.orderLabel.text = [NSString stringWithFormat:@"%@",object.sort];
    self.isVisibleLabel.text = object.status.intValue == 0?@"是":@"否";
}
- (IBAction)btnClick:(id)sender {
    EditSpecificeViewController *vc = [EditSpecificeViewController new];
    vc.specId = self.model.specId;
    vc.specValueId = self.model.specValueId;
    vc.type  =  2;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES ];
}


- (IBAction)deleteBtnClick:(id)sender {
    if(self.btnClickBlock){
        self.btnClickBlock();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
