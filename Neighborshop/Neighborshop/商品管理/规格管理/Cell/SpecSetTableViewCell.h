//
//  SpecSetTableViewCell.h
//  CookMaster
//
//  Created by qiaoyun on 2022/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SpecSetTableViewCell : UITableViewCell
-(void)setItemObject:(id)object;
@property (nonatomic, copy) void(^btnClickBlock)(void);
@end

NS_ASSUME_NONNULL_END
