//
//  SpeciticeManagerCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/14.
//

#import "SpeciticeManagerCell.h"
#import "EditSpecificeViewController.h"
#import "SpecificeManagerController.h"
#import "TakeoutFoodSpecListModel.h"
@interface SpeciticeManagerCell()
@property(nonatomic, strong)TakeoutFoodSpecListModel *model;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@end
@implementation SpeciticeManagerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setItemObject:(TakeoutFoodSpecListModel *)object{
    self.model = object;
    self.nameLabel.text = object.specValueName.length > 0?object.specValueName:object.specName;
    self.numberLabel.text = [NSString stringWithFormat:@"%ld",(long)object.number];
    self.orderLabel.text = [NSString stringWithFormat:@"%@",object.sort];
}
- (IBAction)btnClick:(id)sender {
    EditSpecificeViewController *vc = [EditSpecificeViewController new];
    vc.specId = self.model.specId;
    vc.specValueId = self.model.specValueId;
    vc.type  =  self.model.specValueName.length > 0?2:1;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES ];
}

- (IBAction)settingBtnClick:(UIButton *)sender {
    SpecificeManagerController *vc = [SpecificeManagerController new];
//    vc.isSetting = YES;
    vc.specId = self.model.specId;
    vc.type  =  2;
    [[LJTools topViewController].navigationController pushViewController:vc animated:YES ];
}

- (IBAction)deleteBtnClick:(id)sender {
    if(self.btnClickBlock){
        self.btnClickBlock();
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
