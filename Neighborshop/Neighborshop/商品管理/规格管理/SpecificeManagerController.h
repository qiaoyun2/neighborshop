//
//  SpecificeManagerController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/14.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SpecificeManagerController : BaseViewController
//@property (nonatomic, assign) BOOL isSetting;
//@property (nonatomic, strong) NSString *goodsId;
@property (nonatomic, strong) NSString *specId;
///1，规格 2规格属性
@property (nonatomic, assign) NSInteger type;
@end

NS_ASSUME_NONNULL_END
