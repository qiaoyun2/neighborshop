//
//  GoodsManagerListController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/13.
//

#import "GoodsManagerListController.h"
#import "GoodsManagerListCell.h"
#import "WaterFallLayout.h"
#import "GoodsManagerListModel.h"
#import "AddGoodsViewController.h"

@interface GoodsManagerListController ()<WaterFallLayoutDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) WaterFallLayout * layout;

@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL is_refresh; // 是否刷新

@end

@implementation GoodsManagerListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addSpecSuccess:) name:AddGoods object:nil];
    
    [self initUI];
}
#pragma mark - 通知更新数据
- (void) addSpecSuccess:(NSNotification *)noti{
    [self refresh];
}

#pragma mark – UI
- (void)initUI{
    self.layout = [[WaterFallLayout alloc]init];
    self.layout.delegate = self;
   
    self.collectionView.backgroundColor  = UIColorFromRGB(0xF7F8FC);
    self.collectionView.collectionViewLayout = self.layout;
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([GoodsManagerListCell class]) bundle:nil] forCellWithReuseIdentifier:@"GoodsManagerListCell"];
  
    WeakSelf
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            [weakSelf loadMore];
        }];
    });
    [self refresh];
}
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    self.page = self.page + 1;
    [self getDataArrayFromServerIsRefresh:NO];
}
- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"status"] = @(self.index) ;//-1-全部;1-下架;2-上架
    
    WeakSelf
    [NetworkingTool getWithUrl:kTakeoutFoodListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSArray *array = [responseObject objectForKey:@"data"][@"records"];
            if (array.count > 0) {
                for (NSDictionary *dic in array) {
                    GoodsManagerListModel *model = [[GoodsManagerListModel alloc] initWithDictionary:dic];
                    [weakSelf.dataArray addObject:model];
                }
            }
        }else{
            [LJTools showNOHud:responseObject[@"msg"] delay:1.0];
        }
        [weakSelf addBlankOnView:weakSelf.collectionView];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        if(weakSelf.dataArray.count <= 10){
            [weakSelf.collectionView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [weakSelf.collectionView.mj_footer endRefreshing];
        }
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView reloadData];

    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
        [weakSelf.collectionView.mj_header endRefreshing];
        [weakSelf.collectionView.mj_footer endRefreshing];
        [weakSelf.collectionView reloadData];
    } IsNeedHub:YES];
}
#pragma mark - Delegate
#pragma mark - CollectionDelegate
- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return 10;
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GoodsManagerListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsManagerListCell" forIndexPath:indexPath];
    GoodsManagerListModel *model = self.dataArray[indexPath.item];
    [cell setItemObject:model];
    WeakSelf
    cell.btnClickBlock = ^(NSInteger type) {
        [weakSelf takeoutFoodUpdateStatus:type foodId:model.foodId];
    };
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    GoodsManagerListModel *model = self.dataArray[indexPath.item];
    AddGoodsViewController *vc = [AddGoodsViewController new];
    vc.goodsId = model.foodId;
    [self.navigationController pushViewController:vc animated:YES];
}

//区头高度
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

//区尾高度
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

#pragma mark -- WaterFallLayoutDelegate
// 根据宽度获取高度
- (CGFloat)waterfallLayout:(WaterFallLayout *)layout indexPath:(NSIndexPath *)indexPath itemWidth:(CGFloat)itemWidth{
   
    return itemWidth + 92;
}
// 列数
- (NSInteger)waterfallColumnWithLayout:(WaterFallLayout *)layout{
    return 2;
}
// 组偏移量
- (UIEdgeInsets)waterfallInsetWithLayout:(WaterFallLayout *)layout{
    return UIEdgeInsetsMake(0, 12, 20, 12);
}
//最小行间隔
- (CGFloat)waterfallMinimumLineSpacingWithLayout:(WaterFallLayout *)layout{
    return 10;
}
// 最小列间隔
- (CGFloat)waterfallMinimumInteritemSpacingWithLayout:(WaterFallLayout *)layout{
    return 5;
}
#pragma mark -- 按钮点击
-(void)takeoutFoodUpdateStatus:(NSInteger )type foodId:(NSString *)foodId{
    WeakSelf
    NSDictionary *dict = @{
        @"foodId":foodId,
        @"status":@(type)//状态: 0-下架;1-正常
    };
    [NetworkingTool postWithUrl:kTakeoutFoodUpdateStatusURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:type == 1?@"上架成功！":@"下架成功！" delay:1.5];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf refresh];
            });
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}

@end
