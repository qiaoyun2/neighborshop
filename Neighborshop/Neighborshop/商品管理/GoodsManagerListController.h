//
//  GoodsManagerListController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/13.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsManagerListController : BaseViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, assign) NSInteger index;

@end

NS_ASSUME_NONNULL_END
