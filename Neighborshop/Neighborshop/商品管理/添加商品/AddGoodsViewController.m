//
//  AddGoodsViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/13.
//

#import "AddGoodsViewController.h"
#import "LeePlaceholderTextView.h"
// 图片选择器
#import "HXPhotoView.h"
#import "HXAssetManager.h"
#import "LJImagePicker.h"
#import "SelectSpecModel.h"
#import "UploadManager.h"
#import "AddClassTableView.h"
#import "GoodsManagerListModel.h"
#import "TakeoutFoodCategoryModel.h"

#import "SelectSpecViewController.h"
#import "MyClassViewController.h"

@interface AddGoodsViewController ()<HXPhotoViewDelegate, UITextViewDelegate>
///商品名称
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UIButton *mainImgBtn;
///商品标题
@property (weak, nonatomic) IBOutlet LeePlaceholderTextView *titleTextView;
///数量
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
///(0/1)
@property (weak, nonatomic) IBOutlet UILabel *mainImgNumLabel;
///商品轮播(1/5)
@property (weak, nonatomic) IBOutlet UILabel *bannerNumLabel;
///商品轮播 图
@property (weak, nonatomic) IBOutlet HXPhotoView *bannerImgAddView;
///招牌品类
@property (weak, nonatomic) IBOutlet UIButton *classBtn;
///商品分类
@property (weak, nonatomic) IBOutlet UIButton *selectClassBtn;
///商品规格
@property (weak, nonatomic) IBOutlet UIButton *selectSpeBtn;
///是否上架
@property (weak, nonatomic) IBOutlet UIButton *yesAndnoBtn;
///包装费
@property (weak, nonatomic) IBOutlet UITextField *freeText;
///现价
@property (weak, nonatomic) IBOutlet UITextField *sellPriceText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerViewHeight;
///原价
@property (weak, nonatomic) IBOutlet UITextField *originPriceText;
///限购数量
@property (weak, nonatomic) IBOutlet UITextField *limitNumText;
///分类
@property (weak, nonatomic) IBOutlet UIButton *sortBtn;
// 图片上传
@property (nonatomic, strong) HXPhotoManager *manager;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray<UIImage *> *detailPhotos;
@property (nonatomic, strong) NSMutableArray *imgArray;
///图片路径
@property (nonatomic, strong) NSMutableArray *imagePathArray;
///主图
@property (nonatomic, strong) NSString *mainImgStr;
///分类ID
@property (nonatomic, strong) NSString *clasId;
@property (nonatomic, strong) NSArray *goodsSpecFormatArray;
@property (nonatomic, strong) NSArray *selectSpecArray;
@property (nonatomic, assign) NSInteger index; // 上传下标
@property (nonatomic, strong) GoodsManagerListModel *model;
@end

@implementation AddGoodsViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadBasicData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"添加商品";
    
    // 文字输入
    [self.titleTextView setPlaceholder:@"请输入商品描述"];
    [self.titleTextView setPlaceholderColor:UIColorFromRGB(0xBFBFBF)];
    [self.titleTextView setDelegate:self];
    
    [self initUI];
   
    if(self.goodsId.length > 0){
        [self takeoutFoodGetByFoodId];
    }
}

#pragma mark - IU
-(void)initUI{
    // 图片上传
    self.bannerImgAddView.spacing = 10.f;
    self.bannerImgAddView.delegate = self;
    self.bannerImgAddView.deleteCellShowAlert = YES;
    self.bannerImgAddView.outerCamera = YES;
    self.bannerImgAddView.previewShowDeleteButton = YES;
    self.bannerImgAddView.addImageName = @"组 51102";
    self.bannerImgAddView.lineCount = 3;
    self.bannerImgAddView.manager = self.manager;
}
#pragma mark - 获取商品详情
- (void)takeoutFoodGetByFoodId {
    WeakSelf
    NSDictionary *dict = @{
        @"foodId":self.goodsId
    };
    [NetworkingTool getWithUrl:kTakeoutFoodGetByFoodIdURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSDictionary *dict = responseObject[@"data"];
            GoodsManagerListModel *model = [[GoodsManagerListModel alloc] initWithDictionary:dict];
            weakSelf.model = model;
            [weakSelf updateUI];
        }  else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 获取分类接口
-(void)loadBasicData{
    WeakSelf
    [NetworkingTool getWithUrl:kTakeoutFoodCategoryListURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [self.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *dataArray = responseObject[@"data"];
            for (NSDictionary *obj in dataArray) {
                TakeoutFoodCategoryModel *model = [[TakeoutFoodCategoryModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }  else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 更新Ui
- (void) updateUI{
    self.nameText.text = self.model.title;
    self.titleTextView.text = self.model.foodDesc;
    self.numberLabel.text = [NSString stringWithFormat:@"%ld/300", self.titleTextView.text.length];
    self.mainImgStr = self.model.picture;
    [self.mainImgBtn sd_setImageWithURL:[NSURL URLWithString:kImageUrl(self.mainImgStr)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
    if(self.mainImgStr.length > 0){
        self.mainImgNumLabel.text = @"(1/1)";
    }
    self.goodsSpecFormatArray = [self.model.goodsSpecFormat mj_JSONObject];
    self.clasId = self.model.foodCategoryId;
    [self.selectClassBtn setTitle:[NSString stringWithFormat:@"%@",self.model.foodCategoryName] forState:UIControlStateNormal];
    self.yesAndnoBtn.selected = self.model.status.intValue;
    self.freeText.text = self.model.boxCost;
    self.selectSpecArray = self.model.skuList;
    if(self.selectSpecArray.count > 0){
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dict in self.goodsSpecFormatArray) {
            if([dict.allKeys containsObject:@"specName"]){
                [array addObject:dict[@"specName"]];
            }
            if([dict.allKeys containsObject:@"spec_name"]){
                [array addObject:dict[@"spec_name"]];
            }
            
        }
        [self.selectSpeBtn setTitle:[array componentsJoinedByString:@","] forState:UIControlStateNormal];
    }
    if(self.model.imgIdArray.length > 0){
        self.imagePathArray = [NSMutableArray arrayWithArray:[self.model.imgIdArray componentsSeparatedByString:@","]];
        //imageUrlArray是已知图片的网络地址数组
        NSMutableArray *assets = @[].mutableCopy;
        for(NSString *url in self.imagePathArray)  {
            //selected设为YES 表示图片处于已选中状态
            HXCustomAssetModel *asset = [HXCustomAssetModel assetWithNetworkImageURL:[NSURL URLWithString:kImageUrl(url)] selected:YES];
            [assets addObject:asset];
        }
        [self.manager addCustomAssetModel:assets];
        [self.bannerImgAddView refreshView];
    }
    self.originPriceText.text = self.model.originPrice;
    self.sellPriceText.text =  self.model.sellPrice;
    self.limitNumText.text = self.model.limitNum;
    
    if(self.model.sort.intValue > 0){
        [self.sortBtn setTitle:[NSString stringWithFormat:@"%@",self.model.sort] forState:UIControlStateNormal];
    }
}
#pragma mark - HXPhotoViewDelegate
- (void)photoView:(HXPhotoView *)photoView changeComplete:(NSArray<HXPhotoModel *> *)allList photos:(NSArray<HXPhotoModel *> *)photos videos:(NSArray<HXPhotoModel *> *)videos original:(BOOL)isOriginal {
    self.bannerNumLabel.text = [NSString stringWithFormat:@"(%lu/5)",photos.count];
    //        [self.photos removeAllObjects];
    self.photos = [NSMutableArray arrayWithArray:allList];
}
#pragma mark -  拿到view变化的高度
- (void)photoView:(HXPhotoView *)photoView updateFrame:(CGRect)frame {
    self.bannerViewHeight.constant = frame.size.height;
}
#pragma mark -- UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView{
    NSInteger kMaxLength = 300;
    NSString *toBeString = textView.text;
    NSString *lang = [[UIApplication sharedApplication] textInputMode].primaryLanguage; //ios7之前使用[UITextInputMode currentInputMode].primaryLanguage
    if ([lang isEqualToString:@"zh-Hans"]) { //中文输入
        UITextRange *selectedRange = [textView markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        if (!position) {// 没有高亮选择的字，则对已输入的文字进行字数统计和限制
            if (toBeString.length > kMaxLength) {
                textView.text = [toBeString substringToIndex:kMaxLength];
                [LJTools showText:[NSString stringWithFormat:@"最多输入%ld字", kMaxLength] delay:1.0f];
            }
        }
    }else{//中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
        if (toBeString.length > kMaxLength) {
            textView.text = [toBeString substringToIndex:kMaxLength];
            [LJTools showText:[NSString stringWithFormat:@"最多输入%ld字", kMaxLength] delay:1.0f];
        }
    }
    self.numberLabel.text = [NSString stringWithFormat:@"%ld/%ld", textView.text.length, kMaxLength];
}
#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // 判断是否已经存在小数点
    if ([string isEqualToString:@"."] && [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        textField.text = @"";
        return NO;
    } else if ([string isEqualToString:@"."] && [textField.text containsString:@"."]) {
        return NO;
    }
    NSMutableString * futureString = [NSMutableString stringWithString:textField.text];
    [futureString  insertString:string atIndex:range.location];
    NSInteger flag=0;
    const NSInteger limited = 2;  // 小数点  限制输入两位
    for (NSInteger i = futureString.length-1; i>=0; i--) {
        if ([futureString characterAtIndex:i] == '.') {
            if (flag > limited)   return NO;
            break;
        }
        flag++;
    }
    return YES;
}
#pragma mark - 商品主图
- (IBAction)mainImageBtnClick:(UIButton *)sender {
    [self.view endEditing:YES];
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:self allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                weakSelf.mainImgStr = imageUrl;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
                if(weakSelf.mainImgStr.length > 0){
                    weakSelf.mainImgNumLabel.text = @"(1/1)";
                }
            }];
        }
    }];
}
#pragma mark - 招牌品类
- (IBAction)classBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}
#pragma mark - 是否上架
- (IBAction)yesAndNoBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
}
#pragma mark - 选择排序
- (IBAction)sortBtnClick:(UIButton *)sender {
    WeakSelf
    [UIAlertController actionSheetWithTitle:@"请选择商品排序" message:nil titlesArry:@[@"1",@"2",@"3",@"4",@"5",@"6"] indexBlock:^(NSInteger index, id obj) {
        NSLog(@"%ld",index);
        [sender setTitle:obj forState:UIControlStateNormal];
    }];
}
#pragma mark - 选择规格
- (IBAction)selectSpecBtnClick:(UIButton *)sender {
    WeakSelf
    SelectSpecViewController *vc = [SelectSpecViewController new];
    [vc.dataArray removeAllObjects];
    [vc.dataArray addObjectsFromArray:self.selectSpecArray];
    vc.goodsSpecFormatArray = [self.model.goodsSpecFormat mj_JSONObject];
    
    vc.sureClickBlock = ^(NSArray * _Nonnull goodsSpecFormatArray, NSArray * _Nonnull selectSpec ,NSString * _Nonnull selectSpecName) {
        sender.selected = YES;
        [sender setTitle:selectSpecName forState:UIControlStateNormal];
        weakSelf.goodsSpecFormatArray = goodsSpecFormatArray;
        weakSelf.selectSpecArray = selectSpec;
    };
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - 选择分类
- (IBAction)selectClassBtnClick:(UIButton *)sender {
    WeakSelf
    if(self.dataArray.count == 0){
        MyClassViewController *vc = [MyClassViewController new];
        vc.isAdd  = YES;
        vc.sureClickBlock = ^(TakeoutFoodCategoryModel * _Nonnull model) {
            weakSelf.clasId = model.categoryId;
            sender.selected = YES;
            [sender setTitle:model.name forState:UIControlStateNormal];
        };
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
   
    [AddClassTableView titleArray:self.dataArray show:^(NSString * _Nonnull idStr, NSString * _Nonnull name) {
        weakSelf.clasId = idStr;
        sender.selected = YES;
        [sender setTitle:name forState:UIControlStateNormal];
    }];
}

#pragma mark - 保存
- (IBAction)saveBtnClick:(id)sender {
    if(self.nameText.text.length == 0){
        [LJTools showText:self.nameText.placeholder delay:1.0f];
        return;
    }
    if(self.titleTextView.text.length == 0){
        [LJTools showText:self.titleTextView.placeholder delay:1.0f];
        return;
    }
    if(self.mainImgStr.length == 0){
        [LJTools showText:@"请上传商品主图" delay:1.0f];
        return;
    }
    if(self.photos.count == 0){
        [LJTools showText:@"请上传轮播图" delay:1.0f];
        return;
    }
    if(self.clasId.length == 0){
        [LJTools showText:@"请选择商品分类" delay:1.0f];
        return;
    }
    if(self.goodsSpecFormatArray.count == 0){
        [LJTools showText:@"请选择商品规格" delay:1.0f];
        return;
    }
    if(self.selectSpecArray.count == 0){
        [LJTools showText:@"请选择商品规格" delay:1.0f];
        return;
    }
    if(self.sellPriceText.text.length == 0){
        [LJTools showText:self.sellPriceText.placeholder delay:1.0f];
        return;
    }
    if(self.originPriceText.text.length == 0){
        [LJTools showText:self.originPriceText.placeholder delay:1.0f];
        return;
    }
    if(self.limitNumText.text.length == 0){
        [LJTools showText:self.limitNumText.placeholder delay:1.0f];
        return;
    }
    
    if(self.imagePathArray.count > 0){
       
        [self requestForSubmit];
    }else{
        [self requestForUploadImages];
    }
}
#pragma mark - 上传图片
- (void)requestForUploadImages {
    // 开始上传图片
    [LJTools showHud];
    dispatch_group_t getImageGroup = dispatch_group_create();
    // 处理图片数据
    dispatch_group_enter(getImageGroup);
    self.imgArray = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i < self.photos.count; i++) {
        HXPhotoModel *model = self.photos[i];
        if (model.networkPhotoUrl) {
            [self.imgArray addObject:model.networkPhotoUrl];
            if (self.imgArray.count == self.photos.count) {
                // 离开线程
                dispatch_group_leave(getImageGroup);
            }
        }else{
            [model requestPreviewImageWithSize:model.imageSize startRequestICloud:^(PHImageRequestID iCloudRequestId, HXPhotoModel * _Nullable model) {
            } progressHandler:^(double progress, HXPhotoModel * _Nullable model) {
                
            } success:^(UIImage * _Nullable image, HXPhotoModel * _Nullable model, NSDictionary * _Nullable info) {
                [self.imgArray addObject:image];
                if (self.imgArray.count == self.photos.count) {
                    // 离开线程
                    dispatch_group_leave(getImageGroup);
                }
            } failed:^(NSDictionary * _Nullable info, HXPhotoModel * _Nullable model) {
                NSLog(@"获取图片失败");
            }];
        }
    }
    // 运行完了 回主线程开始上传
    dispatch_group_notify(getImageGroup, dispatch_get_main_queue(), ^{
        WeakSelf
        if ([self isContainNetImg]) {
            // 开始循环上传
            weakSelf.index = 0;
            weakSelf.imagePathArray = [NSMutableArray arrayWithCapacity:0];
            [weakSelf uploadIageOrAddUrl];
        } else {
            // 整体上传
            [UploadManager uploadImageArray:self.imgArray block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                NSArray *array = [imageUrl componentsSeparatedByString:@","];
                weakSelf.imagePathArray = [NSMutableArray arrayWithArray:array];
                [weakSelf requestForSubmit];
            }];
        }
        
    });
}
- (void)uploadIageOrAddUrl{
    id obj = self.imgArray[self.index];
    if ([obj isKindOfClass:[UIImage class]]) {
        UIImage *image = (UIImage *)obj;
        WeakSelf
        [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
            [weakSelf.imagePathArray addObject:imageUrl];
            if (weakSelf.imagePathArray.count == weakSelf.imgArray.count) {
                // 提交商品
                [weakSelf requestForSubmit];
            }else{
                weakSelf.index++;
                [weakSelf uploadIageOrAddUrl];
            }
        }];
    } else {
        NSString *imgUrl = (NSString *)obj;
        [self.imagePathArray addObject:imgUrl];
        if (self.imagePathArray.count == self.imgArray.count) {
            // 提交商品
            [self requestForSubmit];
        }else{
            self.index++;
            [self uploadIageOrAddUrl];
        }
    }
}
#pragma mark -  判断是否包含网络图片
- (BOOL)isContainNetImg{
    BOOL isContain = NO;
    for (HXPhotoModel *model in self.photos) {
        if (model.networkPhotoUrl) {
            isContain = YES;
            break;
        }
    }
    return isContain;
}

#pragma mark - 提交网络请求
- (void) requestForSubmit {
    
    NSMutableArray *array = [NSMutableArray array];
    for (SelectSpecModel *model in self.selectSpecArray) {
        NSDictionary *dict = @{
            @"picture":model.picture,
            @"sellPrice":model.sellPrice,
            @"stock":model.stock,
            @"skuName":model.skuName,
            @"attrValueItems":model.attrValueItems,
            @"attrValueItemsFormat":model.attrValueItemsFormat,
        };
        [array addObject:dict];
    }
    self.selectSpecArray = array;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    params[@"title"] = self.nameText.text;
    params[@"foodDesc"] = self.titleTextView.text;
    params[@"picture"] = self.mainImgStr;
    params[@"goodsSpecFormat"] = [self.goodsSpecFormatArray mj_JSONString];
    params[@"foodCategoryId"] = self.clasId;
    params[@"status"] = self.yesAndnoBtn.selected?@"1":@"0";
    params[@"boxCost"] = [self.freeText.text length] == 0?@"0":self.freeText.text;
    params[@"skuList"] = [self.selectSpecArray mj_JSONString];
    params[@"imgIdArray"] = [self.imagePathArray componentsJoinedByString:@","];
    params[@"originPrice"] = self.originPriceText.text;
    params[@"sellPrice"] = self.sellPriceText.text;
    params[@"limitNum"] = self.limitNumText.text;
    
    if(![self.sortBtn.titleLabel.text isEqualToString:@"请选择"]){
        params[@"sort"] = self.sortBtn.titleLabel.text;
    }
    NSString *url = kTakeoutFoodAddURL;
    if(self.goodsId.length >  0){
        params[@"foodId"] = self.goodsId;
        url  = kTakeoutFoodEditURL;
    }
   
    [NetworkingTool postWithUrl:url params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
            [[NSNotificationCenter defaultCenter] postNotificationName:AddGoods object:nil userInfo:nil];
            
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
    } IsNeedHub:YES];
}
#pragma mark - Lazy Loads
- (HXPhotoManager *)manager {
    if (!_manager) {
        _manager = [[HXPhotoManager alloc] initWithType:HXPhotoManagerSelectedTypePhoto];
        _manager.configuration.type = HXConfigurationTypeWXChat;
        _manager.configuration.singleSelected = NO;
        _manager.configuration.lookGifPhoto = NO;
        _manager.configuration.useWxPhotoEdit = YES;
        _manager.configuration.selectTogether=NO;
        _manager.configuration.photoEditConfigur.onlyCliping = YES;
        _manager.configuration.showOriginalBytes = NO;
        _manager.configuration.videoMaxNum = 0;
        _manager.configuration.photoMaxNum = 5;
        _manager.configuration.requestImageAfterFinishingSelection = YES;
        _manager.configuration.photoEditConfigur.aspectRatio = HXPhotoEditAspectRatioType_None;
        _manager.configuration.selectPhotoLimitSize = YES; // 限制图片大小
        /// 限制照片的大小 单位：b 字节
        /// 默认 0字节 不限制
        /// 网络图片不限制
        _manager.configuration.limitPhotoSize = 10 * 1024 * 1024;
    }
    return _manager;
}

#pragma mark - 懒加载
-(NSMutableArray *)photos{
    if (!_photos) {
        _photos = [NSMutableArray array];
    }
    return _photos;
}
-(NSMutableArray *)detailPhotos{
    if (!_detailPhotos) {
        _detailPhotos = [NSMutableArray array];
    }
    return _detailPhotos;
}
@end
