//
//  AddGoodsViewController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/13.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddGoodsViewController : BaseViewController
@property (nonatomic, strong) NSString *goodsId;
@end

NS_ASSUME_NONNULL_END
