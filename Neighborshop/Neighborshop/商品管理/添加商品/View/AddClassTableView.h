//
//  AddClassTableView.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddClassTableView : UITableViewCell
+(AddClassTableView *)titleArray:(NSArray *)array show:(void(^)(NSString *idStr, NSString *name))block;

@end

NS_ASSUME_NONNULL_END
