//
//  AddClassTableView.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/15.
//

#import "AddClassTableView.h"
#import "TakeoutFoodCategoryModel.h"

@interface AddClassTableView()
@property (weak, nonatomic) IBOutlet UIPickerView *pickView;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (nonatomic, copy) void(^Block)(NSString *idStr, NSString *name);
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) NSString *selectItem;
@property (nonatomic, strong) NSString *selectId;
@end
@implementation AddClassTableView
static AddClassTableView *manager = nil;
#pragma mark - 单例
+(instancetype)shareManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager= [[[NSBundle mainBundle] loadNibNamed:@"AddClassTableView" owner:self options:nil] lastObject];
    });
    return manager;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    
}
#pragma mark - 展示
-(void)show
{
   self.hidden = NO;

    CGAffineTransform transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0,1.0);
    self.bgView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.2,0.2);
    self.bgView.alpha = 0;
    self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 delay:0.1 usingSpringWithDamping:0.5 initialSpringVelocity:10 options:UIViewAnimationOptionCurveLinear animations:^{
        self.bgView.transform = transform;
        self.bgView.alpha = 1;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3f];
        
    } completion:^(BOOL finished) {
    }];
}
#pragma mark - 隐藏
-(void)dismiss
{
    [UIView animateWithDuration:0.3 animations:^{
        self.bgView.transform=CGAffineTransformMakeScale(0.02, 0.02);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {

    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {

    return self.array.count;
}

#pragma mark - UIPickerViewDelegate
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {

    return ScreenWidth;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component __TVOS_PROHIBITED {

    return 40;
}


- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {

    return self.array[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{

    UILabel *label = (UILabel *)[pickerView viewForRow:row forComponent:component];
    [label setTextColor:MainColor];
//    NSDictionary *dict  = self.array[row];
    TakeoutFoodCategoryModel *model = self.array[row];
    self.selectItem = model.name;
    self.selectId = model.categoryId;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view  {

    UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, 40)];
    label.textAlignment = NSTextAlignmentCenter;
    TakeoutFoodCategoryModel *model = self.array[row];
    label.text = model.name;
    
    return label;
}

#pragma mark - view创建
+(AddClassTableView *)titleArray:(NSArray *)array show:(void(^)(NSString *idStr, NSString *name))block{
    AddClassTableView *view = [AddClassTableView shareManager];
    view.Block = block;
    view.array = array;
    [view.pickView reloadAllComponents];
    [view show];
    return view;
}
- (IBAction)sureBtnClick:(UIButton *)sender {
    [self  dismiss];
    if(!self.selectId){
        TakeoutFoodCategoryModel *model = self.array[0];
        self.selectItem = model.name;
        self.selectId = model.categoryId;
    }
    if(self.Block){
        self.Block(self.selectId, self.selectItem);
    }
}

- (IBAction)cancelBtnClick:(UIButton *)sender {
    [self dismiss];
}
@end
