//
//  HeaderSelctTableView.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HeaderSelctTableView : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *errorBtn;
@property (weak, nonatomic) IBOutlet UIButton *titleBtn;

@end

NS_ASSUME_NONNULL_END
