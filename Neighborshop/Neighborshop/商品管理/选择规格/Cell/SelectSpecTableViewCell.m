//
//  SelectSpecTableViewCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import "SelectSpecTableViewCell.h"
#import "LJImagePicker.h"
#import "UploadManager.h"

@implementation SelectSpecTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imageBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
//    [_stockText addTarget:self action:@selector(textFieldDidChange:)
//        forControlEvents:UIControlEventEditingChanged];
}
-(void)setModel:(SelectSpecModel *)model{
    _model = model;
    self.nameLabel.text = model.skuName;
    self.priceText.text = [model.sellPrice doubleValue] > 0?[NSString stringWithFormat:@"%@",model.sellPrice]:@"";
    self.stockText.text = [model.stock doubleValue] > 0?[NSString stringWithFormat:@"%@",model.stock]:@"";
    if(model.picture.length > 0){
        [self.imageBtn sd_setImageWithURL:[NSURL URLWithString:kImageUrl(model.picture)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51572"]];
    }else{
        [self.imageBtn setImage:[UIImage imageNamed:@"组 51572"] forState:UIControlStateNormal];
    }
    self.imageBtnHeight.constant = model.height;
}
#pragma mark - 💓💓 Function ------
/** 输入框观察者事件 */
- (void)textFieldDidChange:(UITextField *)textField {
    NSInteger kMaxLength = 7;
    
    NSString *toBeString = textField.text;
    NSString *lang = [[UIApplication sharedApplication]textInputMode].primaryLanguage; //ios7之前使用[UITextInputMode currentInputMode].primaryLanguage
    if ([lang isEqualToString:@"zh-Hans"]) { //中文输入
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        if (!position) {// 没有高亮选择的字，则对已输入的文字进行字数统计和限制
            if (toBeString.length > kMaxLength) {
                textField.text = [toBeString substringToIndex:kMaxLength];
                
                [LJTools showText:[NSString stringWithFormat:@"最多输入%ld字", kMaxLength] delay:1.0f];
            }
        }
    }else{//中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
        if (toBeString.length > kMaxLength) {
            textField.text = [toBeString substringToIndex:kMaxLength];
            [LJTools showText:[NSString stringWithFormat:@"最多输入%ld字", kMaxLength] delay:1.0f];
        }
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == self.stockText){
        self.model.stock = textField.text;
    }else{
        self.model.sellPrice = textField.text;
    }

}
#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.stockText){
        return YES;
    }
    // 判断是否已经存在小数点
    if ([string isEqualToString:@"."] && [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length == 0) {
        textField.text = @"";
        return NO;
    } else if ([string isEqualToString:@"."] && [textField.text containsString:@"."]) {
        return NO;
    }
    NSMutableString * futureString = [NSMutableString stringWithString:textField.text];
    [futureString  insertString:string atIndex:range.location];
    NSInteger flag=0;
    const NSInteger limited = 2;  // 小数点  限制输入两位
    for (NSInteger i = futureString.length-1; i>=0; i--) {
        if ([futureString characterAtIndex:i] == '.') {
            if (flag > limited)   return NO;
            break;
        }
        flag++;
    }
    return YES;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)imageCllick:(UIButton *)sender {
    [self endEditing:YES];
    WeakSelf
    [LJImagePicker showImagePickerFromViewController:[LJTools topViewController] allowsEditing:YES finishAction:^(UIImage *image) {
        if (image) {
            [UploadManager uploadImageArray:@[image] block:^(NSString * _Nonnull ids, NSString * _Nonnull imageUrl) {
                //                NSLog(@"")
                weakSelf.model.picture = imageUrl;
                
                weakSelf.model.height = 80;
                [sender sd_setImageWithURL:[NSURL URLWithString:kImageUrl(imageUrl)] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"组 51102"]];
                weakSelf.imageBtnHeight.constant = 80;
                
                if(weakSelf.btnClickBlock){
                    weakSelf.btnClickBlock();
                }
            }];
        }
    }];
}

@end
