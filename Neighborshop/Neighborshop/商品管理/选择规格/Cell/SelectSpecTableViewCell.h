//
//  SelectSpecTableViewCell.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import <UIKit/UIKit.h>
#import "SelectSpecModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectSpecTableViewCell : UITableViewCell
///图片
@property (weak, nonatomic) IBOutlet UIButton *imageBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageBtnHeight;
///价格
@property (weak, nonatomic) IBOutlet UITextField *priceText;
///库存
@property (weak, nonatomic) IBOutlet UITextField *stockText;
///名称
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, copy) void(^btnClickBlock)(void);

@property (nonatomic,  strong)SelectSpecModel *model;

@end

NS_ASSUME_NONNULL_END
