//
//  SelectSpecViewController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import "SelectSpecViewController.h"
#import "SelectSpecTableViewCell.h"
#import "SelectSpecModel.h"
#import "TakeoutFoodSpecModel.h"

#import "SpecificeManagerController.h"
#import "HeaderSelctTableView.h"

@interface SelectSpecViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headVieew;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headViewHeight;
@property (weak, nonatomic) IBOutlet UIView *selectView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (strong, nonatomic) IBOutlet UIView *tableHeaderView;
@property (weak, nonatomic) IBOutlet UIView *noSpecView;
@property (weak, nonatomic) IBOutlet UIView *specView;
@property (weak, nonatomic) IBOutlet UIView *headBgView;
@property (weak, nonatomic) IBOutlet UIView *lineBgView;

@property (nonatomic, strong) NSString *selectSpecName;

@end

@implementation SelectSpecViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"添加商品规格";
    self.tableHeaderView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 40);
    self.tableView.tableHeaderView = self.tableHeaderView;
    self.lineBgView.hidden = self.tableView.hidden = YES;
    
    WeakSelf
    [self.noSpecView jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer){
        SpecificeManagerController *vc = [SpecificeManagerController new];
        vc.type = 1;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addSpecSuccess:) name:AddSpecSuccess object:nil];
    [self loadBasicData];
    
    if(self.dataArray.count > 0){
        for (SelectSpecModel *model in self.dataArray) {
            if(model.picture.length > 0){
                model.height = 80;
            }
        }
        weakSelf.tableView.hidden = weakSelf.lineBgView.hidden = NO;
        weakSelf.tableViewHeight.constant = (weakSelf.dataArray.count+1)*80+40;
    }
    [weakSelf.tableView reloadData];

}

#pragma mark - 通知更新数据
- (void) addSpecSuccess:(NSNotification *)noti{
    [self loadBasicData];
}
#pragma mark - 查询规格数据
-(void)loadBasicData{
    WeakSelf
    [NetworkingTool getWithUrl:kTakeoutFoodSpecListURL params:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        [weakSelf.specArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *array = responseObject[@"data"];
            if(array.count > 0){
                for (NSDictionary *dict in array) {
                    TakeoutFoodSpecModel *model = [[TakeoutFoodSpecModel alloc]initWithDictionary:dict ];
                    [weakSelf.specArray addObject:model];
                }
            }
            [weakSelf initTagView:weakSelf.specArray view:weakSelf.selectView ];
        } else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - 查询规格数据
-(void)loadBasicTakeoutFoodSpecValue{
    NSMutableArray *array = [NSMutableArray array];
    NSMutableArray *nameArray = [NSMutableArray array];
    for (TakeoutFoodSpecModel *model in self.specArray) {
        if(model.isSelect){
            [array addObject:model.specId];
            [nameArray addObject:model.specName];
        }
    }
    if(array.count == 0){
        [self.dataArray removeAllObjects];
        self.tableViewHeight.constant = 0;
        [self.tableView reloadData];
        return;
    }
    self.selectSpecName = [nameArray componentsJoinedByString:@","];
    NSDictionary *dict = @{
        @"specIds":[array componentsJoinedByString:@","]
    };
    
    WeakSelf
    [NetworkingTool getWithUrl:kTakeoutOrderGetBySpecIdsURL params:dict success:^(NSURLSessionDataTask *task, id responseObject) {
        [weakSelf.dataArray removeAllObjects];
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            NSArray *array = responseObject[@"data"][@"skuList"];
            NSArray *array1 = responseObject[@"data"][@"goodsSpecFormat"];
            weakSelf.goodsSpecFormatArray = array1;
            if(array.count > 0){
                for (NSDictionary *dict in array) {
                    SelectSpecModel *model = [[SelectSpecModel alloc]  initWithDictionary:dict];
                    model.height = 32.5;
                    [weakSelf.dataArray addObject:model];
                }
            }
            if(weakSelf.dataArray.count > 0){
                weakSelf.tableView.hidden = weakSelf.lineBgView.hidden = NO;
                weakSelf.tableViewHeight.constant = weakSelf.dataArray.count*50+40;
            }
            [weakSelf.tableView reloadData];
        } else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:NO];
}
#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectSpecTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SelectSpecTableViewCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SelectSpecTableViewCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    SelectSpecModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    WeakSelf
    cell.btnClickBlock = ^{
        weakSelf.tableViewHeight.constant = 0;
        for (SelectSpecModel *model in weakSelf.dataArray) {
            CGFloat  testHeight = 0;
            if(model.height > 40){
                testHeight = 90;
            }else{
                testHeight = 50;
            }
            weakSelf.tableViewHeight.constant = testHeight +  weakSelf.tableViewHeight.constant ;
        }
        weakSelf.tableViewHeight.constant =   weakSelf.tableViewHeight.constant + 40;
        [weakSelf.tableView reloadData];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
#pragma mark - 创建按钮
-(void)initTagView:(NSArray *)array view:(UIView *)view  {
    [view removeAllSubviews];
    if (array.count > 0){
        CGFloat nowWidth = 0;
        CGFloat nowHeight = 0;
        CGFloat tagsWidth = 0;
        CGFloat tagsHeight = 32;
        for (int i = 0; i < array.count; i++) {
            TakeoutFoodSpecModel * model = array[i];
            NSString *name = model.specName;
            CGSize stringSize = [LJTools sizeForString:[NSString stringWithFormat:@"%@",name] withSize:CGSizeMake(CGFLOAT_MAX, 10) withFontSize:12];
            tagsWidth = stringSize.width + 20*2;
            if (name.length == 2) {
                tagsWidth = stringSize.width + 25*2;
            }
            UIButton *btn = [[UIButton alloc] init];
            [btn setTitle:[NSString stringWithFormat:@" %@ ", name] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(tagsClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.layer.borderWidth = 0;
            btn.tag = i + 100;
            btn.titleLabel.font = [UIFont systemFontOfSize:12 weight:(UIFontWeightBold)];
            btn.layer.cornerRadius = 4;
            btn.layer.borderWidth = 0.5;
            if(model.isSelect){
               
                btn.layer.borderColor = UIColorFromRGB(0x00A2EA).CGColor;
                [btn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
                btn.backgroundColor = UIColorFromRGB(0x00A2EA);
            }else{

                btn.layer.borderColor = UIColorFromRGB(0xBFBFBF).CGColor;
                [btn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
                btn.backgroundColor = UIColorFromRGB(0xF5F6F9);
            }
            [view addSubview:btn];
            
            CGFloat willWidth = 0;
            willWidth = nowWidth + tagsWidth + 10;
            if (willWidth - 10 > SCREEN_WIDTH - 16 - 16) {
                nowWidth = 0;
                nowHeight = nowHeight + tagsHeight + 15;
            }
            btn.frame = CGRectMake(nowWidth, nowHeight, tagsWidth, tagsHeight);
            nowWidth = nowWidth + tagsWidth + 10;
        }
        self.selectViewHeight.constant = nowHeight+24;
        self.noSpecView.hidden = YES;
        self.specView.hidden = NO;
    }else{
        self.specView.hidden = YES;
        self.selectViewHeight.constant = 0;
        self.noSpecView.hidden = NO;
    }
    [self.view layoutIfNeeded];
    
}
#pragma mark - 规格的选择
- (void)tagsClick:(UIButton *)btn {
    [self.view endEditing:YES];
    TakeoutFoodSpecModel  *model  = self.specArray[btn.tag-100];
    NSMutableArray *array = [NSMutableArray array];
    for (TakeoutFoodSpecModel  *model in self.specArray) {
        if(model.isSelect){
            [array addObject:model];
        }
    }
    btn.selected = !btn.selected;
    if (btn.selected ) {
        if(array.count > 2 ){
            [LJTools showText:@"最多可以选择3个" delay:1.5];
            return;
        }
        model.isSelect = YES;
        btn.layer.borderColor = UIColorFromRGB(0x00A2EA).CGColor;
        [btn setTitleColor:UIColorFromRGB(0xFFFFFF) forState:UIControlStateNormal];
        btn.backgroundColor = UIColorFromRGB(0x00A2EA);
    }else{
        btn.layer.borderColor = UIColorFromRGB(0xBFBFBF).CGColor;
        model.isSelect = NO;
        [btn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
        btn.backgroundColor = UIColorFromRGB(0xF5F6F9);
    }
    [self updateCateView];
    [self loadBasicTakeoutFoodSpecValue];
}
#pragma mark - CateView
- (void)updateCateView {
    [self.headVieew removeAllSubviews];
    CGFloat nowWidth = 0;
    CGFloat nowHeight = 0;
    CGFloat tagsWidth = 0;
    CGFloat tagsHeight = 32;
    NSMutableArray *array = [NSMutableArray array];
    for (TakeoutFoodSpecModel *model in self.specArray) {
        if(model.isSelect){
            [array addObject:model];
        }
    }
    if(array.count > 0){
        for (int i = 0; i < array.count; i++) {
            TakeoutFoodSpecModel * model = array[i];
            NSString *name = model.specName;
            CGSize stringSize = [LJTools sizeForString:[NSString stringWithFormat:@"%@",name] withSize:CGSizeMake(CGFLOAT_MAX, 10) withFontSize:12];
            HeaderSelctTableView *button = [[[NSBundle mainBundle] loadNibNamed:@"HeaderSelctTableView" owner:nil options:nil] firstObject];
            [self.headVieew addSubview:button];
            button.tag = i + 1000;
            tagsWidth = stringSize.width + 30*2;
            if (name.length == 2) {
                tagsWidth = stringSize.width + 35*2;
            }

            [button.titleBtn setTitle:[NSString stringWithFormat:@" %@ ", name] forState:UIControlStateNormal];
            CGFloat willWidth = 0;
            willWidth = nowWidth + tagsWidth + 10;
            if (willWidth - 10 > SCREEN_WIDTH - 16 - 16) {
                nowWidth = 0;
                nowHeight = nowHeight + tagsHeight + 15;
            }
            button.frame = CGRectMake(nowWidth, nowHeight, tagsWidth, tagsHeight);
            nowWidth = nowWidth + tagsWidth + 10;
            WeakSelf
            [button.errorBtn jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
                TakeoutFoodSpecModel  *model  = array[i];
                for (TakeoutFoodSpecModel *list in weakSelf.specArray) {
                    if([model.specId isEqualToString:list.specId]){
                        list.isSelect = NO;
                    }
                }
                [weakSelf initTagView:weakSelf.specArray view:weakSelf.selectView ];
                [weakSelf updateCateView];
                [weakSelf loadBasicTakeoutFoodSpecValue];
            }];
        }
        self.headViewHeight.constant = nowHeight+24;
        self.headBgView.hidden = NO;
    }else{
        self.headViewHeight.constant = 0;
        self.headBgView.hidden = YES;
    }
}

#pragma mark - 保存
- (IBAction)saveBtnClick:(id)sender {
    for ( SelectSpecModel *model in self.dataArray) {
        if(model.picture.length == 0){
            [LJTools showText:@"请上传图片" delay:1.0f];
            return;
        }
        if(model.sellPrice.doubleValue == 0){
            [LJTools showText:@"请输入现格" delay:1.0f];
            return;
        }
        if(model.stock.doubleValue == 0){
            [LJTools showText:@"请输入库存" delay:1.0f];
            return;
        }
    }

//    NSMutableArray *array = [NSMutableArray array];
//    for (SelectSpecModel *model in self.dataArray) {
//        NSDictionary *dict = @{
//            @"picture":model.picture,
//            @"sellPrice":model.sellPrice,
//            @"stock":model.stock,
//            @"skuName":model.skuName,
//            @"attrValueItems":model.attrValueItems,
//            @"attrValueItemsFormat":model.attrValueItemsFormat,
//        };
//        [array addObject:dict];
//    }
    [self.navigationController popViewControllerAnimated:YES];
    if(self.sureClickBlock){
        self.sureClickBlock(self.goodsSpecFormatArray, self.dataArray,self.self.selectSpecName);
    }
    
}
#pragma mark - 懒加载
-(NSMutableArray *)specArray{
    if (!_specArray) {
        _specArray = [NSMutableArray array];
    }
    return _specArray;
}

@end
