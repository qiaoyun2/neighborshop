//
//  SelectSpecViewController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectSpecViewController : BaseViewController
@property (nonatomic, strong) NSMutableArray *specArray;
@property (nonatomic, strong) NSArray *goodsSpecFormatArray;

@property (nonatomic, copy) void(^sureClickBlock)(NSArray *goodsSpecFormatArray,NSArray *selectSpec,NSString *selectSpecName);
@end

NS_ASSUME_NONNULL_END
