//
//  SelectSpecModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SelectSpecModel : BaseModel
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *sellPrice;
@property (nonatomic, strong) NSString *originPrice;
@property (nonatomic, strong) NSString *stock;
@property (nonatomic, strong) NSString *skuName;
@property (nonatomic, strong) NSString *attrValueItems;
@property (nonatomic, strong) NSString *attrValueItemsFormat;
@property (nonatomic, assign) CGFloat height;
@end

NS_ASSUME_NONNULL_END
