//
//  TakeoutFoodSpecModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TakeoutFoodSpecModel : BaseModel
///规格id
@property (nonatomic, strong) NSString *specId;
///规格名称
@property (nonatomic, strong) NSString *specName;
///排序
@property (nonatomic, strong) NSString *sort;
///是否可视：0-可见;1-不可见
@property (nonatomic, strong) NSString *isVisible;
///规格说明
@property (nonatomic, strong) NSString *specDes;

@property (nonatomic , assign) BOOL isSelect;

@end

NS_ASSUME_NONNULL_END
