//
//  GoodsManagerListModel.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import "GoodsManagerListModel.h"
#import "SelectSpecModel.h"
@implementation GoodsManagerListModel
- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqual:@"skuList"]) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dic in value) {
            SelectSpecModel *model = [[SelectSpecModel alloc] initWithDictionary:dic];
            [array addObject:model];
        }
        self.skuList = [NSArray arrayWithArray:array];
    }
}
@end
