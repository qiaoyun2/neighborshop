//
//  GoodsManagerListModel.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/29.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GoodsManagerListModel : BaseModel
@property (nonatomic, strong) NSString *boxCost; // 打包费/餐盒费
@property (nonatomic, strong) NSString *foodCategoryId; //
@property (nonatomic, strong) NSString *foodDesc; //
@property (nonatomic, strong) NSString *foodId;
///
@property (nonatomic, strong) NSString *goodsSpecFormat;
///
@property (nonatomic, strong) NSString *imgIdArray;///
@property (nonatomic, strong) NSString *limitNum;///
@property (nonatomic, strong) NSString *monthSales;
@property (nonatomic, strong) NSString *originPrice;

@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *sellPrice;
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSArray *skuList;
@property (nonatomic, strong) NSString *sort;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *totalSales;
@property (nonatomic, strong) NSString *foodCategoryName;


@end

NS_ASSUME_NONNULL_END
