//
//  GoodsManagerListCell.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/13.
//

#import "GoodsManagerListCell.h"
#import "GoodsCommentViewController.h"
#import "GoodsManagerListModel.h"
@interface GoodsManagerListCell()
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *saleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *downBtn;
@property (weak, nonatomic) IBOutlet UIButton *upBtn;

@property (nonatomic, strong) GoodsManagerListModel *model;
@end
@implementation GoodsManagerListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.saleLabel jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        GoodsCommentViewController *vc = [GoodsCommentViewController new];
        [[LJTools topViewController].navigationController pushViewController:vc animated:YES];
    }];
}
-(void)setItemObject:(GoodsManagerListModel *)object{
    self.model = object;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:kImageUrl(object.picture)] placeholderImage:DefaultImgWidth];
    self.titleLabel.text = object.title;
    self.saleLabel.text = [NSString stringWithFormat:@"销量%@",object.totalSales];
    
    self.priceLabel.attributedText = [LJTools attributedString:[NSString stringWithFormat:@"%.2lf",[object.sellPrice doubleValue]] color:UIColorFromRGB(0xFA2033) oneHeight:10 andTColor:UIColorFromRGB(0xFA2033) twoHeight:16 andThreeTColor:UIColorFromRGB(0xFA2033) threeHeight:11];
    ///状态: 0-下架;1-正常
//    if(object.status.intValue == 2 ){//状态是2是啥呢？
//        self.downBtn.hidden = self.upBtn.hidden = YES;
//    }else
    if(object.status.intValue == 1 ){//状态是1是正常
        self.upBtn.hidden = YES;
        self.downBtn.hidden = NO;
    }else{//状态是0是下架 2是啥呢？
        self.downBtn.hidden = YES;
        self.upBtn.hidden = NO;
    }
   
}
- (IBAction)downBtnClick:(id)sender {
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"您确定要下架该产品吗?" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
        if(weakSelf.btnClickBlock){
            weakSelf.btnClickBlock(0);
        }
    } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
}
- (IBAction)upBtnClick:(id)sender {
    WeakSelf
    [UIAlertController alertViewNormalWithTitle:@"提示" message:@"您确定要上架该产品吗?" titlesArry:@[@"确认"] indexBlock:^(NSInteger index, id obj) {
        if(weakSelf.btnClickBlock){//状态: 0-下架;1-正常
            weakSelf.btnClickBlock(1);
        }
    } okColor:MainColor cancleColor:UIColorFromRGB(0x999999) isHaveCancel:YES];
   
}

@end
