//
//  GoodsManagerListCell.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/13.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoodsManagerListCell : UICollectionViewCell

@property (nonatomic, copy) void(^btnClickBlock)(NSInteger type);
-(void)setItemObject:(id)object;

@end

NS_ASSUME_NONNULL_END
