//
//  GoodsManagerController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/13.
//

#import "GoodsManagerController.h"
#import "YNPageViewController.h"
#import "UIView+YNPageExtend.h"
#import "ReLayoutButton.h"
#import "GoodsManagerListController.h"
#import "AddGoodsViewController.h"
#import "SpecificeManagerController.h"

@interface GoodsManagerController ()<YNPageViewControllerDataSource, YNPageViewControllerDelegate>

@property (nonatomic, strong) NSArray *titles;

@end

@implementation GoodsManagerController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"商品管理";
    self.titles = @[@"全部商品",@"出售中",@"已下架"];
    
    [self setNavigationRightBarButtonWithTitle:@"添加商品" color:UIColorFromRGB(0x333333)];
    
    [self setupPageVC];
    
    
    
}

- (void)setupPageVC {
    YNPageConfigration *configration = [YNPageConfigration defaultConfig];
    configration.pageStyle = YNPageStyleTop;
    /// 控制tabbar 和 nav
    configration.showTabbar = YES;
    configration.showNavigation = YES;
    //scrollMenu = NO,aligmentModeCenter = NO 会变成平分
    configration.scrollMenu = NO;
    configration.aligmentModeCenter = NO;
    configration.showBottomLine = NO;
    configration.showScrollLine = NO;
    configration.itemMargin = 0;
    
    configration.menuHeight = 50;
    configration.normalItemColor = RGBA(51, 51, 51, 1);
    configration.selectedItemColor = RGBA(51, 51, 51, 1);
    configration.itemFont = [UIFont systemFontOfSize:16];
    configration.selectedItemFont = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    configration.cutOutHeight = 0;
    
    NSMutableArray *buttonArrayM = [NSMutableArray array];
    for (int i = 0; i<self.titles.count; i++) {
        ReLayoutButton *button = [ReLayoutButton buttonWithType:UIButtonTypeCustom];
        button.layoutType = 3;
        button.margin = 3;
        UIImage *selectImage = [UIImage imageNamed:@"矩形 1884"];
        [button setImage:[UIImage imageNamed:@"矩形 1884-1"] forState:UIControlStateNormal];
        [button setImage:selectImage forState:UIControlStateSelected];
        [buttonArrayM addObject:button];
    }
    configration.buttonArray = buttonArrayM;
    
    YNPageViewController *vc = [YNPageViewController pageViewControllerWithControllers:self.getArrayVCs titles:self.titles  config:configration];
    vc.dataSource = self;
    vc.delegate = self;
    /// 指定默认选择index 页面
    vc.pageIndex = 0;
    /// 作为自控制器加入到当前控制器
    [vc addSelfToParentViewController:self];
    
    UIButton *addSpecBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    addSpecBtn.frame = CGRectMake(SCREEN_WIDTH -12-50, SCREEN_HEIGHT -70 - TAB_BAR_HEIGHT - NavAndStatusHight, 50, 50);
    [addSpecBtn setImage:[UIImage imageNamed:@"组 51570"] forState:UIControlStateNormal];
    [addSpecBtn setTitle:@"" forState:UIControlStateNormal];
    WeakSelf
    [addSpecBtn jk_addTapActionWithBlock:^(UIGestureRecognizer *gestureRecoginzer) {
        SpecificeManagerController *vc = [SpecificeManagerController  new];
        vc.type = 1;
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
    [self.view addSubview:addSpecBtn];
    [self.view bringSubviewToFront:addSpecBtn];

}

- (NSArray *)getArrayVCs {
    NSMutableArray *tempArray = [NSMutableArray array];
    for (NSInteger i = 0; i<self.titles.count; i++) {
        GoodsManagerListController *vc = [[GoodsManagerListController alloc] init];
        //@"全部商品",@"出售中",@"已下架"  -1-全部;1-下架;2-上架
        if(i == 0){
            vc.index = -1;
        }else if (i == 1){
            vc.index = 1;
        }else{
            vc.index = 2;
        }
        [tempArray addObject:vc];
    }
    return [tempArray copy];
}

#pragma mark - YNPageViewControllerDataSource
- (UIScrollView *)pageViewController:(YNPageViewController *)pageViewController pageForIndex:(NSInteger)index {
    UIViewController *vc = pageViewController.controllersM[index];
    
    return [(GoodsManagerListController *)vc collectionView];
}

#pragma mark - YNPageViewControllerDelegate
- (void)pageViewController:(YNPageViewController *)pageViewController
            contentOffsetY:(CGFloat)contentOffset
                  progress:(CGFloat)progress {
}

- (void)pageViewController:(YNPageViewController *)pageViewController
                 didScroll:(UIScrollView *)scrollView
                  progress:(CGFloat)progress
                 formIndex:(NSInteger)fromIndex
                   toIndex:(NSInteger)toIndex
{
    
}

#pragma mark - 右边按钮
- (void)rightButtonTouchUpInside:(UIBarButtonItem *)sender{
//    SpecificeManagerController *vc = [SpecificeManagerController  new];
    AddGoodsViewController *vc = [AddGoodsViewController new];
    [self.navigationController pushViewController:vc animated:YES];
    
}

@end
