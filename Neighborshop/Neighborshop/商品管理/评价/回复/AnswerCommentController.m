//
//  AnswerCommentController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/14.
//

#import "AnswerCommentController.h"
#import "LeePlaceholderTextView.h"

@interface AnswerCommentController ()
@property (weak, nonatomic) IBOutlet LeePlaceholderTextView *commnetTextView;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@end

@implementation AnswerCommentController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"回复";
}
#pragma mark - DataSourceAndDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString * toBeString = [textView.text stringByReplacingCharactersInRange:range withString:text];
      
    //得到输入框的内容
    if (toBeString.length > 99) {
        NSString *str = [toBeString substringToIndex:99];
        textView.text = str;
        self.numberLabel.text  = @"99/99";
        return NO;
    }
    self.numberLabel.text  = [NSString stringWithFormat:@"%d/99",toBeString.length];
    return YES;
}

#pragma mark - 确定
- (IBAction)sureBtnClick:(id)sender {
    [self.view endEditing:YES];
    if(self.commnetTextView.text.length == 0){
        [LJTools showText:@"请输入回复内容" delay:1.5];
        return;
    }
    NSDictionary *dicxt  = @{
        @"replyContent":self.commnetTextView.text,
        @"evaluateId":self.evaluateId
    };
    WeakSelf
    [NetworkingTool postWithUrl:kTakeoutEvaluateReplyURL params:dicxt success:^(NSURLSessionDataTask *task, id responseObject) {
        [LJTools hideHud];
        if ([responseObject[@"code"] integerValue]==1) {
            [LJTools showText:@"回复完成！" delay:1.5];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
    } IsNeedHub:YES];
    
}



@end
