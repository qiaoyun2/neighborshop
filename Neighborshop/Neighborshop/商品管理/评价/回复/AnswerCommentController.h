//
//  AnswerCommentController.h
//  CookMaster
//
//  Created by qiaoyun on 2022/11/14.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AnswerCommentController : BaseViewController
@property (nonatomic, strong) NSString *evaluateId;
@end

NS_ASSUME_NONNULL_END
