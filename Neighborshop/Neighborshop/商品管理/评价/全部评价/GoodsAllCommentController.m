//
//  GoodsAllCommentController.m
//  CookMaster
//
//  Created by qiaoyun on 2022/11/14.
//

#import "GoodsAllCommentController.h"
#import "MyCommentCell.h"
#import "MoreButtonView.h"

@interface GoodsAllCommentController ()
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong)  NSArray *titleArray;
@end

@implementation GoodsAllCommentController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"评价详情";
    self.titleArray = @[@"全部 9999+",@"好评 9999+",@"有图 1030",@"差评 208",@"中评 28",@"点赞 111"];
    [self initUI];
}
#pragma mark - UI
-(void)initUI{
    
    MoreButtonView *view = [[MoreButtonView alloc] initWithFrame:CGRectMake(0, 16, ScreenWidth, 100) titleArr:self.titleArray];
    view.normalColor = UIColorFromRGB(0xF6F7F9);
    view.selectColor = UIColorWithRGB(0, 162, 234, 0.1);
    [view setHeight:[view getViewHeight]+12];
    [view resetIndex:0];
    WeakSelf
    view.Block = ^(NSInteger index) {
        NSLog(@"点击=====%ld",index);
        
    };
    [self.headerView addSubview:view];
    
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, [view getViewHeight]+12+16);
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^(void) {
        [weakSelf refresh];
    }];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [weakSelf loadMore];
    }];
    [self refresh];
}
#pragma mark - Network
- (void)refresh {
    _page = 1;
    [self getDataArrayFromServerIsRefresh:YES];
}
- (void)loadMore {
    _page++;
    [self getDataArrayFromServerIsRefresh:NO];
}

- (void)getDataArrayFromServerIsRefresh:(BOOL)isRefresh {
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    params[@"pageNo"] = @(_page);
    params[@"pageSize"] = @(10);
    params[@"type"] = @"3";
    params[@"startDate"] = @"";
    params[@"endDate"] = @"";
    WeakSelf
    [NetworkingTool getWithUrl:kRWCommentListURL params:params success:^(NSURLSessionDataTask *task, id responseObject) {
        if ([responseObject[@"code"] intValue] == SUCCESS) {
            if (isRefresh) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSDictionary *dataDic = responseObject[@"data"];
            NSArray *dataArray = dataDic[@"records"];
            for (NSDictionary *obj in dataArray) {
                RWOrderCommentModel *model = [[RWOrderCommentModel alloc] initWithDictionary:obj];
                [weakSelf.dataArray addObject:model];
            }
        }
        else {
            [LJTools showText:responseObject[@"msg"] delay:1.5];
        }
        [weakSelf addBlankOnView:self.tableView];
        weakSelf.noDataView.hidden = weakSelf.dataArray.count != 0;
        [weakSelf.tableView.mj_header endRefreshing];
        if(weakSelf.dataArray.count < 10){
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        }else{
            [weakSelf.tableView.mj_footer endRefreshing];
        }
        [weakSelf.tableView reloadData];
    } failed:^(NSURLSessionDataTask *task, NSError *error, id responseObject) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        [LJTools showNOHud:RequestServerError delay:1.0];
    } IsNeedHub:YES];
}


#pragma mark - UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCommentCell"];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyCommentCell" owner:nil options:nil] firstObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    RWOrderCommentModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



@end
