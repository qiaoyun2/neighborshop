//
//  AppDelegate+CheckNetworkState.m
//  yuejiu
//
//  Created by    on 2017/11/1.
//  Copyright © 2017年   . All rights reserved.
//

#import "AppDelegate+CheckNetworkState.h"

@implementation AppDelegate (CheckNetworkState)

/**监听网络状态*/
- (void)ml_checkNetworkState{
    //1.创建网络监测者
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        //这里是监测到网络改变的block  可以写成switch方便
        //在里面可以随便写事件
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                NSLog(@"当前的网络状态——未知网络状态");
                break;
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"当前的网络状态——无网络");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"当前的网络状态——蜂窝数据网");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"当前的网络状态——WiFi网络");
                break;
            default:
                break;
        }
    }] ;
    //开始监听
    [manager startMonitoring];
}


@end
