//
//  SOAComponentAppDelegate.m
//  ZZR
//
//  Created by null on 2019/8/17.
//  Copyright © 2019 null. All rights reserved.
//

#import "SOAComponentAppDelegate.h"
#import "WxPayService.h"
#import "AliPaySevice.h"
#import "UMShareService.h"

@implementation SOAComponentAppDelegate
{
    NSMutableArray* allServices;
}

#pragma mark - 服务静态注册

//需要运行程序之前，手工增加根据需要的新服务

-(void)registeServices
{
    [self registeService:[[WxPayService alloc] init]];
    [self registeService:[[AliPaySevice alloc] init]];
    [self registeService:[[UMShareService alloc] init]];

}

#pragma mark - 获取SOAComponent单实例

+ (instancetype)instance {
    
    static SOAComponentAppDelegate *insance = nil;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        insance = [[SOAComponentAppDelegate alloc] init];
    });
    
    
    return insance;
}

#pragma mark - 获取全部服务
-(NSMutableArray *)services
{
    
    if (!allServices) {
        allServices = [[NSMutableArray alloc]init];
        [self registeServices];
    }
    
    return allServices;
}

#pragma mark - 服务动态注册
-(void)registeService:(id)service
{
    if (![allServices containsObject:service])
    {
        [allServices addObject:service];
    }
}
@end
