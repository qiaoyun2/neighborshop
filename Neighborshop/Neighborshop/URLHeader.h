//
//  URLHeader.h
//  HOOLA
//
//  Created by null on 2018/9/18.
//  Copyright © 2018 null. All rights reserved.
//

#ifndef URLHeader_h
#define URLHeader_h

#define SUCCESS  1
#define TokenError  401
#define RequestServerError @"您的网络不稳定，请稍后重试"

#define kDomainUrl @"https://hello.pinpinkan.vip/pinpinkan-merchant/api/"
#define kServerUrl [NSString stringWithFormat:@"%@v1/", kDomainUrl]
#define kServerUrlV2 [NSString stringWithFormat:@"%@v2/", kDomainUrl]

#define kBaseImageUrl @"https://pinpinkans.oss-cn-hangzhou.aliyuncs.com/"

#define kImageUrl(url) [url hasPrefix:@"http"] ? [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] : [NSString stringWithFormat:@"%@%@",kBaseImageUrl,[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]]


#pragma mark ===== 通用 =====
/** 文件上传 **/
#define kUploadFileURL [NSString stringWithFormat:@"%@common/uploadImagesAli", kServerUrl]
/** 发送验证码  注册:register； 登录：login; 忘记密码：forget； 绑定微信qq:bindThird **/
#define kGetCodeURL [NSString stringWithFormat:@"%@sms/aliSend", kServerUrl]
/** 获取配置信息  registerAgreement：注册协议； privacyPolicy：隐私政策l; logo:logo; companyPicture:公司图片; personalPicture:个人图片; officialMobile:官方电话**/
#define kConfigNameURL [NSString stringWithFormat:@"%@config/config/queryValueByConfigName", kServerUrl]
/** 留言建议 **/
#define kFeedbackURL [NSString stringWithFormat:@"%@marketFeedback/add",kServerUrl]

#pragma mark ===== 登录注册 =====
/** 注册 **/
#define kRegisterURL [NSString stringWithFormat:@"%user/register", kServerUrl]
/** 账号密码登录 **/
#define kMobileLoginURL [NSString stringWithFormat:@"%@user/login", kServerUrl]
/** 手机验证码登录 **/
#define kCodeLoginURL [NSString stringWithFormat:@"%@user/loginOrRegister", kServerUrl]
/** 忘记密码 **/
#define kForgetPwdURL [NSString stringWithFormat:@"%@user/forgetPwd", kServerUrl]
/** qq、微信一键登录 **/
#define kThirdLoginURL [NSString stringWithFormat:@"%@user/mobileThirdLogin", kServerUrl]
/** qq、微信绑定手机号 **/
#define kThirdBindURL [NSString stringWithFormat:@"%@user/bindOpenId", kServerUrl]

#pragma mark ===== 首页 =====
/** 首页店铺信息**/
#define kTakeoutShoppMsgURL [NSString stringWithFormat:@"%@takeoutShop/shopMsg", kServerUrl]
/**查看明细**/
#define kTakeoutBusinessListURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/business/list", kServerUrl]
/**订单列表**/
#define kTakeoutOrderListURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/list", kServerUrl]
/**首页订单列表**/
#define kTakeoutOrderFirstListURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/first/list", kServerUrl]



#pragma mark ===== 会员 =====
/** 会员类型**/
#define kMasterVipTypeURL [NSString stringWithFormat:@"%@masterVipType/masterVipType/list", kServerUrl]
/** 购买会员支付**/
#define kMasterBuyVipURL [NSString stringWithFormat:@"%@masterUserVipRecord/pay", kServerUrl]
/** 购买记录**/
#define kMasterPayLogURL [NSString stringWithFormat:@"%@masterUserVipRecord/list", kServerUrl]
/** 内购**/
#define kInAppPurchaseURL [NSString stringWithFormat:@"%@vipRecharge/applePayMaster", kServerUrl]

#pragma mark ===== 我的 =====
/** 获取用户信息 **/
#define kGetUserInfoURL [NSString stringWithFormat:@"%@user/queryUser", kServerUrl]
/** 修改用户信息 **/
#define kEditUserInfoURL [NSString stringWithFormat:@"%@user/modifyUser", kServerUrl]
/** 师傅认证 **/
#define kAuthenticationMasterURL [NSString stringWithFormat:@"%@user/authenticationMaster", kServerUrl]
/** 提现收款账号列表查询 **/
#define kTakeoutCashAccountURL [NSString stringWithFormat:@"%@takeout/takeoutCashAccount/list", kServerUrl]
/** 根据类型获取账号 **/
#define kTakeoutCashGetByTypeURL [NSString stringWithFormat:@"%@takeout/takeoutCashAccount/getByType", kServerUrl]
/** 银行类型 **/
#define kQueryListByCodeURL [NSString stringWithFormat:@"%@dict/queryListByCode", kServerUrl]
/**提现收款账号修改 **/
#define kTakeoutCashAccountEditURL [NSString stringWithFormat:@"%@takeout/takeoutCashAccount/edit", kServerUrl]
/** 提现收款账号添加 **/
#define kTakeoutCashAccountAddURL [NSString stringWithFormat:@"%@takeout/takeoutCashAccount/add", kServerUrl]
/** 分类列表**/
#define kTakeoutFoodCategoryListURL [NSString stringWithFormat:@"%@takeoutFoodCategory/takeoutFoodCategory/list", kServerUrl]
/** 分类添加**/
#define kTakeoutFoodCategoryAddURL [NSString stringWithFormat:@"%@takeoutFoodCategory/takeoutFoodCategory/add", kServerUrl]
/** 修改分类**/
#define kTakeoutFoodCategoryEditURL [NSString stringWithFormat:@"%@takeoutFoodCategory/takeoutFoodCategory/edit", kServerUrl]
/** 删除分类**/
#define kTakeoutFoodCategoryDeleteURL [NSString stringWithFormat:@"%@takeoutFoodCategory/takeoutFoodCategory/deleteById", kServerUrl]
/** 商铺详情**/
#define ktakeoutShopShopInfoURL [NSString stringWithFormat:@"%@takeoutShop/shopInfo", kServerUrl]
/** 营业设置**/
#define ktakeoutShopShopEditURL [NSString stringWithFormat:@"%@takeoutShop/edit", kServerUrl]
/** 营业设置**/
#define kMerchantAnswerQuestionURL [NSString stringWithFormat:@"%@config/merchantAnswerQuestion", kServerUrl]
/** 流水记录 **/
#define kFlowRecordURL [NSString stringWithFormat:@"%@takeoutShopAccount/bill/detailList", kServerUrl]




/** 我的评价列表 **/
#define kRWCommentListURL [NSString stringWithFormat:@"%@recovery/recoveryOrderComments/list", kServerUrl]

/** 师傅端意见反馈类型表 **/
#define kFeedBackTypeListURL [NSString stringWithFormat:@"%@feedback/merchantFeedbackType/list", kServerUrl]
/** 添加意见反馈 **/
#define kAddFeedBackURL [NSString stringWithFormat:@"%@feedback/merchantFeedback/add", kServerUrl]
/** 我的反馈记录 **/
#define kFeedBackLogURL [NSString stringWithFormat:@"%@feedback/merchantFeedback/list", kServerUrl]

/** 旧密码修改密码 **/
#define kChangePwdByOldURL [NSString stringWithFormat:@"%@user/modifyPwd", kServerUrl]
/** 验证码更改密码-验证（下一步） **/
#define kChangePwdByCodeFirstURL [NSString stringWithFormat:@"%@user/verifyChangePwd", kServerUrl]
/** 验证码更改密码-更改  **/
#define kChangePwdByCodeSecondURL [NSString stringWithFormat:@"%@user/changePwd", kServerUrl]
/** 修改手机号第一步（验证手机号）  **/
#define kChangeMobileFirstURL [NSString stringWithFormat:@"%@user/verifyMobile", kServerUrl]
/** 修改手机号第二步  **/
#define kChangeMobileSecondURL [NSString stringWithFormat:@"%@user/changeMobile", kServerUrl]
/** 注销账号 **/
#define kDelUserURL [NSString stringWithFormat:@"%@takeoutShop/logoutShop", kServerUrl]
/** 是否设置支付密码 **/
#define kIsPayPasswordURL [NSString stringWithFormat:@"%@user/account/isPayPassword", kServerUrl]
/** 支付密码是否正确 **/
#define kCheckPayPasswordURL [NSString stringWithFormat:@"%@user/account/checkPayPassword", kServerUrl]
/** 重置支付密码 **/
#define kResetPayPasswordURL [NSString stringWithFormat:@"%@user/account/resetPayPassword", kServerUrl]
/** 验证码-设置支付密码 **/
#define kSetPayPasswordURL [NSString stringWithFormat:@"%@user/account/setPayPassword", kServerUrl]

/** 绑定微信、支付宝 **/
#define kBindReceivablesInfoURL [NSString stringWithFormat:@"%@user/account/bindReceivablesInfo", kServerUrl]
/** 获取绑定账号信息 **/
#define kGetBindInfoURL [NSString stringWithFormat:@"%@takeout/takeoutCashAccount/getBindInfo", kServerUrl]
/** 我的钱包 **/
#define kMyWalletURL [NSString stringWithFormat:@"%@takeoutShopAccount/myWallet", kServerUrl]

/** 账户提现申请 **/
#define kWithdrawApplyURL [NSString stringWithFormat:@"%@takeoutShopAccount/withdrawApply", kServerUrl]
/** 提现记录 **/
#define kWithdrawRecordURL [NSString stringWithFormat:@"%@withdraw/queryWithdraw", kServerUrl]

#pragma mark ===== 商品管理 =====
/** 商品 - 列表 **/
#define kTakeoutFoodListURL [NSString stringWithFormat:@"%@takeoutFood/takeoutFood/list", kServerUrl]
/** 商品 - 上下架 **/
#define kTakeoutFoodUpdateStatusURL [NSString stringWithFormat:@"%@takeoutFood/takeoutFood/updateStatus", kServerUrl]
/** 商品规格 - 列表 **/
#define kTakeoutFoodSpecListURL [NSString stringWithFormat:@"%@takeoutFoodSpec/takeoutFoodSpec/list", kServerUrl]
/**商品规格 - 规格属性列表**/
#define kTakeoutOrderGetBySpecIdsURL [NSString stringWithFormat:@"%@takeoutFoodSpecValue/takeoutFoodSpecValue/getBySpecIds", kServerUrl]
/**商品 - 添加**/
#define kTakeoutFoodAddURL [NSString stringWithFormat:@"%@takeoutFood/takeoutFood/add", kServerUrl]
/**商品 - 编辑**/
#define kTakeoutFoodEditURL [NSString stringWithFormat:@"%@takeoutFood/takeoutFood/edit", kServerUrl]

/**商品规格 - 列表**/
#define kTakeoutFoodSpecListURL [NSString stringWithFormat:@"%@takeoutFoodSpec/takeoutFoodSpec/list", kServerUrl]
/**商品规格 - 删除**/
#define kTakeoutFoodDeleteBySpecIdURL [NSString stringWithFormat:@"%@takeoutFoodSpec/takeoutFoodSpec/deleteBySpecId", kServerUrl]
/**商品规格 - 编辑**/
#define kTakeoutFoodSpecEditURL [NSString stringWithFormat:@"%@takeoutFoodSpec/takeoutFoodSpec/edit", kServerUrl]
/**商品规格 - 新增**/
#define kTakeoutFoodSpecAddURL [NSString stringWithFormat:@"%@takeoutFoodSpec/takeoutFoodSpec/add", kServerUrl]
/**商品规格 - 详情**/
#define kTakeoutFoodGetBySpecIdURL [NSString stringWithFormat:@"%@takeoutFoodSpec/takeoutFoodSpec/getBySpecId", kServerUrl]
/**商品 - 详情**/
#define kTakeoutFoodGetByFoodIdURL [NSString stringWithFormat:@"%@takeoutFood/takeoutFood/getByFoodId", kServerUrl]
/**商品规格属性 - 列表**/
#define kTakeoutFoodSpecValueListURL [NSString stringWithFormat:@"%@takeoutFoodSpecValue/takeoutFoodSpecValue/list", kServerUrl]
/**商品规格属性 - 删除**/
#define kTakeoutFoodDeleteBySpecValueIdURL [NSString stringWithFormat:@"%@takeoutFoodSpecValue/takeoutFoodSpecValue/deleteBySpecValueId", kServerUrl]
/**商品规格属性 - 详情**/
#define kTakeoutFoodGetBySpecValueIdURL [NSString stringWithFormat:@"%@takeoutFoodSpecValue/takeoutFoodSpecValue/getBySpecValueId", kServerUrl]
/**商品规格属性 - 新增**/
#define kTakeoutFoodSpecValueAddURL [NSString stringWithFormat:@"%@takeoutFoodSpecValue/takeoutFoodSpecValue/add", kServerUrl]
/**商品规格属性 - 编辑**/
#define kTakeoutFoodSpecValueEditURL [NSString stringWithFormat:@"%@takeoutFoodSpecValue/takeoutFoodSpecValue/edit", kServerUrl]




#pragma mark ===== 维修 =====
/**抢单大厅分页列表查询*/
#define kMTRepairOrderURL [NSString stringWithFormat:@"%@repairOrder/list", kServerUrl]
/**维修分类 - 一、二级分类*/
#define kMTRepairClassifyURL [NSString stringWithFormat:@"%@repairClassify/list", kServerUrl]
/**我的订单列表*/
#define kMTRepairMyOrderListURL [NSString stringWithFormat:@"%@repairOrder/myOrderList", kServerUrl]
/**首页-待接单详情*/
#define kMTQueryHomeOrderByIdURL [NSString stringWithFormat:@"%@repairOrder/queryHomeOrderById", kServerUrl]
/**维修订单-接单*/
#define kMTQueryHomeReceiveURL [NSString stringWithFormat:@"%@repairOrder/receive", kServerUrl]
/**维修订单-立即出发*/
#define kMTQueryHomeSetOutURL [NSString stringWithFormat:@"%@repairOrder/setOut", kServerUrl]
/**订单列表详情*/
#define kMTQueryMyOrderByIdURL [NSString stringWithFormat:@"%@repairOrder/queryMyOrderById", kServerUrl]
/**订单-师傅取消订单*/
#define kMTRepairOrderCancelURL [NSString stringWithFormat:@"%@repairOrder/cancel", kServerUrl]
/** 删除订单 **/
#define kMTRepairDeleteOrderURL [NSString stringWithFormat:@"%@repairOrder/deleteOrder", kServerUrl]
/**维修订单-师傅上传图片**/
#define kMTRepairUploadPictureURL [NSString stringWithFormat:@"%@repairOrder/uploadPicture", kServerUrl]

#pragma mark ===== 订单 =====
/****订单列表***/
#define kTakeoutOrderListURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/list", kServerUrl]
/****订单详情***/
#define kTakeoutOrderQueryByIdURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/queryById", kServerUrl]
/*****接单***/
#define kTakeoutOrderQueryReceiveURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/receive", kServerUrl]
/****提货验证**/
#define kVerifyPickupCodeURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/verifyPickupCode", kServerUrl]
/****标记配送中/待自提***/
#define kTakeoutOrderDeliveryURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/delivery", kServerUrl]
/****拒绝/同意退款***/
#define kTakeoutOrderRefundURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/refund", kServerUrl]
/****取消订单***/
#define kTakeoutOrderCancelURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/cancel", kServerUrl]
/****删除订单****/
#define kTakeoutOrderDeleteURL [NSString stringWithFormat:@"%@takeoutOrder/takeoutOrder/delete", kServerUrl]
/****评价 - 订单评价****/
#define kTakeoutEvaluateGetByOrderNoURL [NSString stringWithFormat:@"%@takeoutEvaluate/takeoutEvaluate/getByOrderNo", kServerUrl]
/****评价 - 删除回复****/
#define kTakeoutEvaluateDelReplyURL [NSString stringWithFormat:@"%@takeoutEvaluate/takeoutEvaluate/del/reply", kServerUrl]
/****评价 - 回复****/
#define kTakeoutEvaluateReplyURL [NSString stringWithFormat:@"%@takeoutEvaluate/takeoutEvaluate/reply", kServerUrl]

#pragma mark ===== 消息 =====
/**公告 - 消息列表*/
#define kMessageListByTypeURL [NSString stringWithFormat:@"%@message/listByType", kServerUrl]
/**公告 - 删除*/
#define kMessageDeleteByIdURL [NSString stringWithFormat:@"%@message/delete", kServerUrl]
/**公告 - 标记已读*/
#define kMessageReadURL [NSString stringWithFormat:@"%@message/read", kServerUrl]
/**公告 - 详情*/
#define kMessageGetByMessageIdURL [NSString stringWithFormat:@"%@message/getByMessageId", kServerUrl]
/**公告 - 未读条数*/
#define kMessageUnreadCountURL [NSString stringWithFormat:@"%@message/unreadCount", kServerUrl]


#endif /* URLHeader_h */
